# -*- coding: utf-8 -*-
__author__ = 'peterd'

from moneyed import EUR

from django.core.exceptions import ObjectDoesNotExist
from shopper.models.product import BrandProductCategory, ProductUnit, SellerProductCategory, SellerToCommonCategoryMap, \
    CommonProductCategory, BrandProduct, SellerProduct, SellerProductDiscount, Brand
from shopper.crawlers.discount import Discount

PU_PIECE_ID = 4

class HoferDiscount(Discount):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller, description,
                 new_price, currency=EUR, date_format='%Y-%m-%d'):
        super(HoferDiscount, self).__init__(url, image_file, name, valid_from, valid_until, seller, description,
                                                new_price, None, currency, date_format)


    def save(self, create_seller_product=False, logger=None):
        """Saves this discount into database and returns linked seller product if any, None otherwise."""

        ean_pc = 'x' * 13
        brand_product_category = BrandProductCategory.objects.get(name='Unknown')
        product_unit = ProductUnit.get_product_unit_by_id(PU_PIECE_ID)

        try:
            seller_product_category = SellerProductCategory.objects.get(seller_id=self.seller.id,
                                                                        name='Unknown')
        except ObjectDoesNotExist:
            seller_product_category = SellerProductCategory(seller_id=self.seller.id,
                                                            name='Unknown')
            seller_product_category.save()

        try:
            common_product_category_id = SellerToCommonCategoryMap.get_common_product_category_id(
                                                            seller_product_category.id)
        except ObjectDoesNotExist:
            common_product_category = CommonProductCategory.objects.get(name=u'Všetko pre domov')
            seller_to_common_category_map = SellerToCommonCategoryMap(
                                                            seller_id=self.seller.id,
                                                            seller_product_category_id=seller_product_category.id,
                                                            common_product_category_id=common_product_category.id)
            seller_to_common_category_map.save()

            common_product_category_id = common_product_category.id

        brand_product = BrandProduct.create_brand_product(brand=Brand.get_default_brand(),
                                                          brand_product_category=brand_product_category,
                                                          common_product_category_id=common_product_category_id,
                                                          name=self.name,
                                                          ascii_name=self.ascii_name,
                                                          ean_pc=ean_pc,
                                                          ean_pc_int=ean_pc,
                                                          unit_id=product_unit.id,
                                                          amount_input_type='pc',
                                                          amount_input_base=1)

        brand_product.update_image(self.image_link.image_file, self.image_link.file_name)

        if self.description:
            brand_product.description = self.description

        brand_product.save()

        seller_product = SellerProduct.create_seller_product(product_name=self.name,
                                                             product_ascii_name=self.ascii_name,
                                                             ean_pc=ean_pc, ean_pc_int=ean_pc,
                                                             ean_crt=None, ean_crt_int=None,
                                                             product_unit_id=product_unit.id,
                                                             brand_product_id=brand_product.id,
                                                             seller_id=self.seller.id,
                                                             seller_product_category_id=seller_product_category.id,
                                                             discount_valid_from=self.valid_from,
                                                             discount_valid_until=self.valid_until,
                                                             price_pc_with_vat=self.new_price,
                                                             must_recategorize=False)

        seller_product_discount = SellerProductDiscount(image_full=self.image_file, url=self.url,
                                                        name=self.name,
                                                        ascii_name=self.ascii_name,
                                                        discount_valid_from=self.valid_from,
                                                        discount_valid_until=self.valid_until,
                                                        seller_id=self.seller.id,
                                                        description=self.description,
                                                        price_with_vat=self.new_price,
                                                        price_with_vat_old=self.old_price,
                                                        is_expired=False,
                                                        seller_product_id=seller_product.id)
        seller_product_discount.save()

        return seller_product