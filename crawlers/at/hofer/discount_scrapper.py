# -*- coding: utf-8 -*-

#!/usr/bin/env python
from datetime import datetime, timedelta
from decimal import Decimal
import argparse
import re
from bs4 import BeautifulSoup
import logging

import django
django.setup()

from shopper.crawlers.discount import ImageLink
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.models.product import Seller, SellerProductDiscount, SellerProduct, get_product_amount_and_unit, \
    SellerProductDiscountTrash
from shopper.crawlers.at.hofer.discount import HoferDiscount

__author__ = 'peterd'

class HoferScrapper(DiscountScrapper):
    BASE_URL = 'https://www.hofer.at/'

    RE_DISCOUNT_PERIOD = re.compile(r'.*?(\d{8}).*?')

    def __init__(self, logger, seller, sleep_int=None, dont_torify=False):
        super(HoferScrapper, self).__init__(seller, logger, sleep_int, dont_torify)

    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        try:
            response = self.session.get(HoferScrapper.BASE_URL)
            soup = BeautifulSoup(response.content)

            for special_offer_link in soup.find('li', {'class': 'special-offers'}).findAll('a'):
                try:
                    m = re.search(HoferScrapper.RE_DISCOUNT_PERIOD, special_offer_link['href'])
                    if m:
                        discount_valid_from = datetime.strptime(m.group(1), "%d%m%Y").date()
                        if discount_valid_from.weekday() == 0:
                            discount_valid_until = discount_valid_from + timedelta(days=2)
                        elif discount_valid_from.weekday() == 3:
                            discount_valid_until = discount_valid_from + timedelta(days=3)
                        else:
                            raise ReferenceError('Uknown discount span for date %s (expected weekday 3 or 4 got %d' % (
                                discount_valid_from, discount_valid_from.weekday()))

                        response = self.session.get(special_offer_link['href'])
                        soup = BeautifulSoup(response.content)

                        for product_link in soup.findAll('a', {'class': 'box--description--header--link'}):
                            try:
                                response = self.session.get(product_link['href'])
                                if response.status_code != 200:
                                    response.raise_for_status()

                                soup = BeautifulSoup(response.content)

                                try:
                                    product_name = soup.find('h1', {'class': 'detail-box--price-box--title'}).text.strip()
                                except AttributeError:
                                    pass

                                price_box = soup.find('div', {'class': 'detail-box--price-box--price'})

                                amount = price_box.find('span',
                                                {'class': 'detail-box--price-box--price--amount'}).text.strip()

                                if len(price_box.findAll('span', {'class': 'box--value'})) == 1:
                                    new_price = ('%s%s' % (price_box.find('span',
                                                            {'class': 'box--value'}).text.strip(),
                                                       price_box.find('span',
                                                        {'class': 'box--decimal'}).text.strip())).replace(',', '.').\
                                        replace(u'–', '0')
                                    if new_price.count('.') == 2 and new_price.find('.0') != -1:
                                        new_price = new_price.replace('.0', '').replace('.', '')
                                else:
                                    new_price = '0.%s' % price_box.findAll('span',
                                                        {'class': 'box--value'})[1].text.strip()
                                new_price = Decimal(new_price)

                                detailed_amount = soup.find('div', {'class': 'detail-box--price-box--price'}).find('span',
                                                {'class': 'detail-box--price-box--price--detailamount'})
                                if detailed_amount is not None:
                                    amount = '%s, %s' % (amount, detailed_amount.text.strip())

                                for para in soup.find('div', {'class': 'detail-tabcontent'}).findAll('p'):
                                    product_amount, product_unit = get_product_amount_and_unit(para.text.strip())
                                    if product_amount is not None and product_unit is not None:
                                        product_name = '%s %s %s' % (product_name, product_amount, product_unit)
                                        break

                                discount = HoferDiscount(product_link['href'], None, product_name,
                                                         discount_valid_from.strftime('%Y-%m-%d'),
                                                         discount_valid_until.strftime('%Y-%m-%d'),
                                                         self.seller, amount, new_price)

                                # skip the product if trashed
                                if SellerProductDiscountTrash.discount_exists(discount):
                                    logger.info('skipping "%s" as has been trashed' % discount.name)
                                    continue

                                # skip the product if scrapped already
                                if SellerProductDiscount.discount_exists(discount):
                                    logger.info('skipping "%s" as already scrapped' % discount.name)
                                    continue

                                seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                                        discount.name,
                                                                        discount.url,
                                                                        self.seller.id)

                                if seller_product_discount and seller_product_discount.seller_product_id is not None:
                                    seller_product_discount.on_discount_valid(discount)

                                    seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                                    seller_product.on_discount_valid(seller_product_discount)
                                else:
                                    image_link = ImageLink(soup.find('img', {'class': ['media-gallery--image', 'm-first']}))
                                    image_link.fetch(self.session)

                                    discount.image_file = image_link.rel_image_path
                                    discount.image_link = image_link
                                    seller_product = discount.save()

                                    if seller_product is None:
                                        self.add(discount)
                                    else:
                                        if seller_product.must_recategorize:
                                            self.on_must_recategorize(seller_product)
                            except Exception, e:
                                if product_link and product_link.has_attr('href'):
                                    self.logger.exception(u'Failed to process %s content, reason: %s' % (
                                        product_link['href'], e))
                                else:
                                    self.logger.exception(u'Failed to process a product content, reason: %s' % e)
                except Exception, e:
                    if special_offer_link and special_offer_link.has_attr('href'):
                        self.logger.exception(u'Failed to process %s content, reason: %s' % (special_offer_link['href'],
                                                                                             e))
                    else:
                        self.logger.exception(u'Failed to process a product content, reason: %s' % e)
        except Exception, e:
            self.logger.exception(u'Failed to get %s content, reason: %s' % (HoferScrapper.BASE_URL, e))

        return self.discount_map


parser = argparse.ArgumentParser(description='hofer discount scrapper utility')
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--teaser-only', '-t', dest="teaser_only", action='store_true',
                    help='Scraps only few discounts.')
parser.add_argument('--dont-torify', dest="dont_torify", action='store_true',
                    help='Do not use TOR when scrapping. Used by default.')


args = parser.parse_args()

hofer = Seller.objects.get(name='Hofer')

log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)

logger.info('Scrapping discounts...')

discount_scrapper = HoferScrapper(logger=logger, seller=hofer,
                                  sleep_int=None, dont_torify=args.dont_torify)
discount_scrapper.scrap(teaser_only=args.teaser_only)
discount_scrapper.send_mail()