# -*- coding: utf-8 -*-
#!/usr/bin/env python


import argparse
import logging
from bs4 import BeautifulSoup
import requests
import sys

import django
from shopper.models.viral import Prospect, ProspectState, PROSPECT_STATE_NOT_APPROACHED
from shopper.utils.session import TorSession

django.setup()

parser = argparse.ArgumentParser(description='mediatel company scrapper')
parser.add_argument('-l', '--loglevel',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set the logging level')
parser.add_argument('-f', '--logfile',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('-b', '--business-category',
                    help='Category to search for companies.')

args = parser.parse_args()

if args.loglevel:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=args.loglevel)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=args.loglevel)
else:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

if args.business_category is None:
    parser.print_usage()
    sys.exit(1)

PROSPECT_STATE_NOT_APPROACHED_OBJ = ProspectState.objects.get(state=PROSPECT_STATE_NOT_APPROACHED)

try:
    def scrap_companies(page):
        my_ip = requests.get('http://icanhazip.com/').content.strip('\n')
        session = TorSession(my_ip, logger)
        response = session.get('http://www.zlatestranky.sk/hladanie/%s/%d/' % (args.business_category, page))
        soup = BeautifulSoup(response.content)
        for company in soup.findAll('li', {'class': 'results-list__item'}):
            try:
                name = company.find('h1').text.replace('\n', '').strip()
                address = company.find('span', {'class': 'address'}).text
                email = company.find('li', {'class': 'mail'}).text

                if email and not Prospect.objects.filter(nick=name).exists():
                    prospect = Prospect(nick=name, is_business=True, address=address, email=email,
                                        state=PROSPECT_STATE_NOT_APPROACHED_OBJ)
                    prospect.save()

                logger.info('scrapped "%s"' % name)
            except Exception, e:
                logger.exception('failed to scrap company, reason: %s' % e)

        last_page = soup.find('ul', {'class': ['paginate', 'ptb30', 'divider']}).find('li', {'class': 'disabled'})
        last_page = False if last_page is None else last_page.text == u'Ďalšie'
        if not last_page:
            scrap_companies(page + 1)
    scrap_companies(1)
except Exception, e:
    logger.exception('failed to scrap companies, reason: %s' % e)

