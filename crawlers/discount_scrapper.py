__author__ = 'peterd'

import requests

from django.core.mail import send_mail
from django.utils.translation import ugettext as _

from shopper.utils.session import TorSession
from shopper.models.household import HouseholdMemberRole


class DiscountScrapper(object):
    def __init__(self, seller=None, logger=None, sleep_int=None, dont_torify=False):
        if seller is not None:
            self.seller = seller
            self.seller_name = seller.name.lower()
        self.logger = logger
        self.sleep_int = sleep_int
        self.dont_torify = dont_torify
        self.discount_map = dict()
        self.my_ip = requests.get('http://icanhazip.com/').content.strip('\n')
        self.recategorize_seller_products = []
        self.err_msgs = []


    @property
    def session(self):
        return TorSession(my_ip=self.my_ip, logger=self.logger, sleep_int=self.sleep_int) if not self.dont_torify \
            else requests.Session()


    def scrap(self, teaser_only=False):
        """Returns list of scrapped discount objects. If teaser_only is True scraps only first page."""

        raise NotImplementedError('This method must be implemented by a derive class.')


    def add(self, discount):
        """Adds supplied discount to the discount map."""

        if not self.discount_map.has_key(discount.seller.name):
            self.discount_map[discount.seller.name] = dict(discounts=[], num_discounts_with_candidates=0)
        self.discount_map[discount.seller.name]['discounts'].append(discount)
        if discount.has_candidates:
            self.discount_map[discount.seller.name]['num_discounts_with_candidates'] += 1


    def get_num_scrapped_discounts(self):
        return len(self.discount_map.keys())


    def on_must_recategorize(self, seller_product):
        """Adds supplied seller product to the list of seller products requiring recategorization."""

        self.recategorize_seller_products.append(seller_product)


    def send_mail(self):
        """Mails scrapping result to all discount editors."""

        junior_discount_editor_emails = []
        for discount_editor in HouseholdMemberRole.get_junior_discount_editors():
            if not discount_editor.is_locust:
                junior_discount_editor_emails.append(discount_editor.user.email)

        senior_discount_editor_emails = []
        for discount_editor in HouseholdMemberRole.get_senior_discount_editors():
            if not discount_editor.is_locust:
                senior_discount_editor_emails.append(discount_editor.user.email)

        for seller_name, discount_ctx in self.discount_map.iteritems():
            subject = None
            if discount_ctx['num_discounts_with_candidates'] == 1:
                subject = _('1 %(seller)s discount is to be linked.') % ({'seller': seller_name})
            elif discount_ctx['num_discounts_with_candidates'] > 1:
                subject = _('%(num_discounts)d %(seller)s discounts are to be linked.') \
                          % ({'num_discounts': discount_ctx['num_discounts_with_candidates'],
                              'seller': seller_name})
            if subject:
                send_mail(subject, None, 'discount.scrapper@20deka.com', junior_discount_editor_emails)

            subject = None
            if len(discount_ctx['discounts']) == 1:
                if len(self.recategorize_seller_products) == 1:
                    subject = _('1 %(seller)s discount is to be linked. 1 seller product is to be recategorized.') % (
                                    {'seller': seller_name})
                else:
                    subject = _('1 %(seller)s discount is to be linked.') % ({'seller': seller_name})
            else:
                if len(self.recategorize_seller_products) == 0:
                    subject = _('%(num_discounts)d %(seller)s discounts are to be linked.') % (
                                        {'num_discounts': len(discount_ctx['discounts']),
                                         'seller': seller_name})
                elif len(self.recategorize_seller_products) == 1:
                    subject = _('%(num_discounts)d %(seller)s discounts are to be linked. 1 seller product '
                                'is to be recategorized.') % (
                                        {'num_discounts': len(discount_ctx['discounts']),
                                         'seller': seller_name})
                elif len(self.recategorize_seller_products) > 1:
                    subject = _('%(num_discounts)d %(seller)s discounts are to be linked. %(num_seller_products)d '
                                'seller products are to be recategorized.') % (
                                        {'num_discounts': len(discount_ctx['discounts']),
                                         'seller': seller_name,
                                         'num_seller_products': len(self.recategorize_seller_products)})
            if subject:
                send_mail(subject, None, 'discount.scrapper@20deka.com', senior_discount_editor_emails)