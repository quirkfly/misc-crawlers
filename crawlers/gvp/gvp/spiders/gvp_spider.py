import logging

from scrapy import Selector
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shopper.crawlers.gvp.gvp.items import ProductItem

class GVPSpider(CrawlSpider):
    name = 'gvp'
    base_url = 'http://www.gvphe.sk'
    allowed_domains = ['www.gvphe.sk']
    start_urls = [
        'http://www.gvphe.sk/-privatna-znacka'
    ]

    rules = (Rule(LinkExtractor(allow=(r'--\d+-\d+-.*'),), callback='parse_product'),
             Rule(LinkExtractor(allow=(r'-.*'),), follow=True))


    def parse_product(self, response):
        """Returns ProductItem."""

        hxs = Selector(response)

        try:
            name_candidates = hxs.xpath('//div[@class="col-md-12 vnutro"]/h2/text()').extract()
            if len(name_candidates) > 0:
                name = name_candidates[0]
                amount = hxs.xpath('//table[@class="table01"]/tbody/tr')[1].xpath('td/text()')[1].extract().replace('\r\n','').replace('\t', '')
                id = hxs.xpath('//table[@class="table01"]/tbody/tr')[2].xpath('td/text()')[1].extract().replace('\r\n','').replace('\t', '')
                producer = hxs.xpath('//table[@class="table01"]/tbody/tr')[3].xpath('td/text()')[1].extract().replace('\r\n','').replace('\t', '')
                image = '%s/%s' % (GVPSpider.base_url , hxs.xpath('//img[@alt="%s"]/@src' % name)[0].extract())

                product_item = ProductItem()
                product_item['link'] = response.url
                product_item['name'] = name
                product_item['amount'] = amount
                product_item['id'] = id
                product_item['producer'] = producer
                product_item['image_urls'] = [image]

                return product_item
            else:
                logging.info('skipping product at %s as being most likely an over counter product' % response.url)

        except Exception, e:
            logging.exception('failed to parse product item from url %s, reason: %s' % (response.url, e))

            return None
