# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class ProductItem(Item):
    link = Field()
    name = Field()
    amount = Field()
    id = Field()
    producer = Field()
    image_urls = Field()
    images = Field()