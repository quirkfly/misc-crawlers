#!/bin/bash -x

# main script that orchestrates the crawling process
#

SELLER="gvp"

USE_HTTP_PROXY=1
LOG_DIR=/var/log/itrash
DATA_DIR=/var/data/itrash

CRAWL_INTERVAL=7

MAX_NUM_LOG_FILES=6
MAX_NUM_WRITER_FILES=3
MAX_NUM_READER_FILES=3
MAX_NUM_DATA_FILES=3
MIN_NUM_SCRAPED_ITEMS=400

function usage {
	echo "gvp seller crawler and db writer"
	echo "`basename $0` -c <days> [-l <log dir>] [-d <data dir>]"
	echo "-c - how often to crawl seller, defaults to 7 days"
	echo "log dir - defaults to $LOG_DIR"
	echo "data dir - defaults to $DATA_DIR"
	exit 1
}

function rotate_files {
	FILE_DIR=$1
	FILE_PREFIX=$2
	MAX_NUM_FILES=$3

	NUM_FILES=`ls $FILE_DIR/$FILE_PREFIX* | wc -l`
	if [ $NUM_FILES -gt $MAX_NUM_FILES ]
	then 
		NUM_FILES=`expr $NUM_FILES - 1`
		rm $FILE_DIR/$FILE_PREFIX.$NUM_FILES
	fi

	while [ $NUM_FILES -gt 0 ]
	do
		CUR_FILE_SUFFIX=`expr $NUM_FILES - 1`
		NEXT_FILE_SUFFIX=`expr $CUR_FILE_SUFFIX + 1`
		if [ $CUR_FILE_SUFFIX -gt 0 ]
		then
			mv $FILE_DIR/$FILE_PREFIX.$CUR_FILE_SUFFIX $FILE_DIR/$FILE_PREFIX.$NEXT_FILE_SUFFIX
		else
			mv $FILE_DIR/$FILE_PREFIX $FILE_DIR/$FILE_PREFIX.1
		fi
		NUM_FILES=`expr $NUM_FILES - 1`
	done
}

while getopts c:l:d: FLAG; do
	case $FLAG in
	c)
		CRAWL_INTERVAL=$OPTARG;
		;;
    l)
		LOG_DIR=$OPTARG;
     	;; 
    d)
		DATA_DIR=$OPTARG;
      	;;
    ?)
     	usage;
      	;;
  esac
done

#
# rotate files
#

# rotate crawler files
test -f $LOG_DIR/${SELLER}_product_crawler.log && rotate_files $LOG_DIR ${SELLER}_product_crawler.log $MAX_NUM_LOG_FILES

# rotate writer files
test -f $LOG_DIR/${SELLER}_product_writer.log && rotate_files $LOG_DIR ${SELLER}_product_writer.log $MAX_NUM_WRITER_FILES

# rotate reader files
test -f $LOG_DIR/${SELLER}_price_list_reader.log && rotate_files $LOG_DIR ${SELLER}_price_list_reader.log $MAX_NUM_READER_FILES

if [ -f $DATA_DIR/${SELLER}_products.jsonlines ]
then
    CUR_DATE=`date +%Y-%m-%d`
    CUR_DATE_SECS=`date -d "$CUR_DATE" +%s`
    DATA_FILE_LAST_MODIFIED_DATE=`stat -c %y $DATA_DIR/${SELLER}_products.jsonlines | cut -f1 -d' '`
    DATA_FILE_LAST_MODIFIED_DATE_SEC=`date -d "$DATA_FILE_LAST_MODIFIED_DATE" +%s`
    LAST_DATE_FILE_CRAWL_DAY_DIFF=`echo "( $CUR_DATE_SECS - $DATA_FILE_LAST_MODIFIED_DATE_SEC) / (24*3600)" | bc -l | cut -f1 -d.`
else
    LAST_DATE_FILE_CRAWL_DAY_DIFF=-1
fi

if [ $LAST_DATE_FILE_CRAWL_DAY_DIFF -eq -1 -o $LAST_DATE_FILE_CRAWL_DAY_DIFF -gt $CRAWL_INTERVAL ]
then
    # rotate data files
    test -f $DATA_DIR/${SELLER}_products.jsonlines && rotate_files $DATA_DIR ${SELLER}_products.jsonlines $MAX_NUM_DATA_FILES

    cd $ITRASH_HOME_DIR/shopper/crawlers/$SELLER

    #
    # launch product crawler
    #

    echo "[`date`] about to start $SELLER product crawler..."

    PYTHONPATH=$PYTHONPATH:$ITRASH_HOME_DIR scrapy crawl $SELLER -o $DATA_DIR/${SELLER}_products.jsonlines -t jsonlines --logfile $LOG_DIR/${SELLER}_product_crawler.log 2>$LOG_DIR/${SELLER}_product_crawler.log

    echo "[`date`] $SELLER product crawler finished"

	NUM_SCRAPED_ITEMS=`grep item_scraped_count $LOG_DIR/${SELLER}_product_crawler.log | cut -f2 -d: | tr -d ' ,'`
	test "x$NUM_SCRAPED_ITEMS" == "x" && NUM_SCRAPED_ITEMS=0

	if [ $NUM_SCRAPED_ITEMS -lt $MIN_NUM_SCRAPED_ITEMS ]
	then
        cd $ITRASH_HOME_DIR
        grep -i -A100 "dumping scrapy stats" $LOG_DIR/${SELLER}_product_crawler.log > $LOG_DIR/${SELLER}_product_crawler.stats
        python utils/mailer.py --from_addr ${SELLER}_crawler@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to crawl $SELLER products." $LOG_DIR/${SELLER}_product_crawler.stats
        rm $LOG_DIR/${SELLER}_product_crawler.stats
        exit 1
    fi

    #
    # write scrapped products to db
    #

    echo "[`date`] about to start $SELLER product writer..."

    cd $ITRASH_HOME_DIR

    DJANGO_SETTINGS_MODULE=itrash.settings PYTHONPATH=`pwd`:.. python shopper/writers/$SELLER/${SELLER}_writer.py --logfile $LOG_DIR/${SELLER}_writer.log $DATA_DIR/${SELLER}_products.jsonlines 2>$LOG_DIR/${SELLER}_writer.log
    RV=$?

    echo "[`date`] $SELLER product writer finished"

    if [ ! $RV -eq 0 ]
    then
        tail -100 $LOG_DIR/${SELLER}_writer.log > $LOG_DIR/${SELLER}_writer.err
        python utils/mailer.py --from_addr ${SELLER}_writer@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to write $SELLER (seller_branch_id=$SELLER_BRANCH_ID) products to database." $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        rm $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        exit 2
    fi

    #
    # check writer log for errors
    #

    NUM_WRITE_ERRORS=`grep ERROR $LOG_DIR/${SELLER}_writer.log | wc -l`
    if [ $NUM_WRITE_ERRORS -gt 0 ]
    then
        grep ERROR $LOG_DIR/${SELLER}_writer.log > $LOG_DIR/${SELLER}_writer.err
        python utils/mailer.py --from_addr ${SELLER}_writer@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to write $NUM_WRITE_ERRORS $SELLER products to database." $LOG_DIR/${SELLER}_writer.err
        rm $LOG_DIR/${SELLER}_writer.err
        exit 2
    fi

fi

#
# launch price list reader
#

echo "[`date`] about to start $SELLER price list reader..."

cd $ITRASH_HOME_DIR

DJANGO_SETTINGS_MODULE=itrash.settings PYTHONPATH=`pwd`:.. python shopper/crawlers/$SELLER/price_list_reader.py --logfile $LOG_DIR/${SELLER}_price_list_reader.log --loglevel DEBUG 2>$LOG_DIR/${SELLER}_price_list_reader.log
RV=$?

echo "[`date`] $SELLER price list reader finished"

#
# check price reader for errors
#

NUM_EXCEPTIONS=`grep EXCEPTION $LOG_DIR/_price_list_reader.log | wc -l`
if [ $NUM_EXCEPTIONS -gt 0 ]
then
	grep EXCEPTION $LOG_DIR/${SELLER}_price_list_reader.log > $LOG_DIR/${SELLER}_price_list_reader.err
	python utils/mailer.py --from_addr ${SELLER}_price_list_reader@20deka.com --to_addr peter.dermek@20deka.com --subject "Got $NUM_EXCEPTIONS while running $SELLER price list reader." $LOG_DIR/${SELLER}_price_list_reader.err
	rm $LOG_DIR/${SELLER}_price_list_reader.err
	exit 2
fi