# -*- coding: utf-8 -*-
#!/usr/bin/env python

import argparse
import logging
import os
import re
from moneyed import EUR, Money
import requests
from bs4 import BeautifulSoup
import simplejson
import sys
from xlrd import open_workbook

import django
from django.core.exceptions import ObjectDoesNotExist
from shopper.models.location import State, City, Street, Location, District
from shopper.utils.psql import to_tsquery_str

django.setup()

from infra.lexical import CharMapper
from itrash.settings import MEDIA_ROOT
from shopper.utils.conv import money_to_string
from itrash.itrash_settings import ITRASH_DATA_ROOT
from shopper.models.product import SellerProduct, Seller, BrandProduct, SellerBrand, UnknownProduct, \
    get_product_amount_and_unit, SellerBranch, Brand

BASE_URL = 'http://www.gvphe.sk'

RE_ORDER_LINK = re.compile(r'objednavka_internet(\d+).xls')

FIRST_DATA_LN = 7
PRODUCT_ID_COL = 3

active_seller_ids = Seller.get_active_sellers()[2]

seller = Seller.objects.get(name='GVP')

# create or get seller branch
slovakia = State.objects.get(name='Slovensko')
humenne = City.objects.get(name=u'Humenné')
try:
    tolsteho = Street.objects.get(name=u'Tolstého')
except ObjectDoesNotExist:
    tolsteho = Street(name=u'Tolstého', ascii_name='tolsteho')
    tolsteho.save()

try:
    gvp_warehouse_location = Location.objects.get(street=tolsteho, street_num='1',
                                                  district=District.get_or_create_default_district(),
                                                  city=humenne,
                                                  state=slovakia)
except ObjectDoesNotExist:
    gvp_warehouse_location = Location(street=tolsteho, street_num='1',
                                      district=District.get_or_create_default_district(),
                                      city=humenne,
                                      state=slovakia)
    gvp_warehouse_location.save()

try:
    gvp_warehouse_branch = SellerBranch.objects.get(seller_id=seller.id, location=gvp_warehouse_location)
except ObjectDoesNotExist:
    gvp_warehouse_branch = SellerBranch(seller_id=seller.id, location=gvp_warehouse_location)
    gvp_warehouse_branch.save()


class Product(object):
    ACRONYM_MAP = {'ZP': 'zubna pasta',
                   'NH': 'na holenie',
                   'SAMP.': 'sampon',
                   'SG': 'sprchovy gel',
                   'TEK.M.': 'tekute mlieko',
                   'TEL.M.': 'telove mlieko',
                   'TUZID.': 'tuzidlo',
                   'USTNA V.': 'ustna voda',
                   'LISTE.': 'listerine',
                   'UTIER.VL.': 'utierky vlhcene',
                   'VAT.TYCIN.': 'vatove tycinky',
                   'PH': 'po holeni',
                   'ZILET.NAHR.HL.': 'ziletky nahradna hlava',
                   'DET.': 'detsky',
                   'HOLIA.STROJ': 'holiaci strojcek',
                   'HYG.VR.': 'hygienicke vreckovky',
                   'PLIEN.PAMP.': 'plienky pampers',
                   'VAT.ODL.TAMPON.': 'vatove odlicovacie tampony',
                   'VLOZ.': 'vlozky',
                   'TOAL.P.': 'toaletny papier',
                   'TOAL.': 'toaletny papier',
                   'UTIER.KUCH.': 'utierky kuchynske',
                   'OSV.': 'osviezovac',
                   'VL.': 'vlhkosti',
                   'KRM.': 'krmivo'
    }

    def __init__(self, new_stuff, discount, category, id, name, pkg_amount, unit, price_with_vat,
                 order_amount, order_price):
        errors = []

        self.new_stuff = new_stuff
        self.discount = discount

        if category is None:
            errors.append(u'missing category')
        else:
            self.category = category

        if id is None:
            errors.append(u'missing id')
        elif not id.isdigit():
            errors.append(u'id must be number, got "%s" instead' % id)
        else:
            self.id = int(id)

        if name is None:
            errors.append(u'missing name')
        else:
            self.name = name

        if pkg_amount is None:
            errors.append(u'missing pkg_amount')
        elif type(pkg_amount) not in (int, float):
            errors.append(u'pkg_amount must be number, got "%s" instead' % pkg_amount)
        else:
            self.pkg_amount = pkg_amount

        if unit is None:
            errors.append(u'missing unit')
        else:
            self.unit = unit

        if price_with_vat is None:
            errors.append(u'missing price')
        elif type(price_with_vat) is not float:
            errors.append(u'price_with_vat must be float number, got "%s"  instead' % price_with_vat)
        elif price_with_vat <= 0:
            errors.append(u'price_with_vat must be greater than 0, got "%d"  instead' % price_with_vat)
        else:
            self.price_with_vat = price_with_vat

        self.order_amount = order_amount
        self.order_price = order_price

        self.seller_product = None
        self.num_brand_product_candidates = 0

        if len(errors) > 0:
            raise RuntimeError('.'.join(errors))


    def link_with_seller_product(self, seller_id):
        """Attempts to link this instance with a seller product. Returns (linked seller product, 0) if successful,
        (None, num_brand_product_candidates) otherwise."""

        try:
            seller_product = SellerProduct.objects.get(seller_id=seller_id, seller_product_code=str(self.id))
            seller_product.price_pkg_with_vat = Money(self.price_with_vat, EUR)
            seller_product.save(update_fields=['price_pkg_with_vat'])

            self.seller_product = seller_product

        except ObjectDoesNotExist:
            try:
                unknown_product = UnknownProduct.objects.get(seller_branch_id=gvp_warehouse_branch.id,
                                                             name=self.name,
                                                             seller_product_code=str(self.id))
                self.seller_product = unknown_product.seller_product

                unknown_product_candidates_file = os.path.join(MEDIA_ROOT, 'product', 'seller', '%d' % seller_id,
                                                               'unknown_product', '%d.json' % unknown_product.id)
                if os.path.exists(unknown_product_candidates_file):
                    with open(unknown_product_candidates_file) as f:
                        self.num_brand_product_candidates = len(f.readlines())
            except ObjectDoesNotExist:
                logger.debug('looking for product "%s" candidates...' % self.name)

                product_ascii_name = CharMapper.cp1250_to_ascii(self.name).lower()

                product_amount, product_unit_sym = get_product_amount_and_unit(product_ascii_name)

                if product_amount is None:
                    logger.warning('product "%s" seems to be missing amount' % self.name)

                    return (self.seller_product, self.num_brand_product_candidates)

                for acronym, meaning in Product.ACRONYM_MAP.iteritems():
                    product_ascii_name = product_ascii_name.replace(acronym.lower(), ' %s ' % meaning)

                product_ascii_name_words = filter(lambda w: len(w) > 1 and not w[0].isdigit(), product_ascii_name.split(' '))

                unwound_product_ascii_name_words = []

                for word in product_ascii_name_words:
                    if word.find('.') != -1:
                        subwords = []
                        for subword in word.split('.'):
                            if not subword[0].isdigit():
                                subwords.append(subword)
                        unwound_product_ascii_name_words.extend(subwords)
                    else:
                        unwound_product_ascii_name_words.append(word)

                brand_product_candidates = dict()

                brand_to_num_products_map = dict()

                for term in unwound_product_ascii_name_words:
                    # skip amount indicators
                    if len(term) == 0 or term.replace(',', '').replace('.', '').isdigit():
                        continue

                    try:
                        for brand_product in BrandProduct.objects.search(to_tsquery_str(term,
                                                                                        allow_partial_match=True),
                                                                         raw=True):

                            #logger.debug('trying "%s" as candidate...' % brand_product.name)

                            if not brand_product.is_sold_in_sellers(active_seller_ids):
                                continue

                            brand_specific_sellers = SellerBrand.get_sellers_for_brand(brand_product.brand)
                            if len(brand_specific_sellers) and seller not in brand_specific_sellers:
                                continue

                            bp_product_amount, bp_product_unit_sym = get_product_amount_and_unit(brand_product.ascii_name)

                            try:
                                if not brand_product_candidates.has_key(brand_product.id) and \
                                                brand_product.detailed_common_product_category is not None and \
                                        bp_product_amount == product_amount and bp_product_unit_sym == product_unit_sym:
                                    num_matched_word = 0

                                    for l_word in brand_product.ascii_name.split(' '):
                                        for r_word in unwound_product_ascii_name_words:
                                            if l_word.find(r_word) != -1:
                                                num_matched_word += 1

                                    if num_matched_word < 2:
                                        continue

                                    brand_product_candidates[brand_product.id] = dict(
                                                                              bp_id=brand_product.id,
                                                                              ean_pc=brand_product.ean_pc,
                                                                              ean_pc_int=brand_product.ean_pc_int,
                                                                              brand_name=brand_product.brand.name,
                                                                              cpc_name=brand_product.detailed_common_product_category.name,
                                                                              ascii_name=brand_product.ascii_name,
                                                                              name=brand_product.name,
                                                                              image=brand_product.image_thumb_big_url,
                                                                              price_per=brand_product.price_per,
                                                                              product_unit_id=brand_product.unit.id)

                                    brand = Brand.get_brand_by_id(brand_product.brand_id)
                                    if brand_to_num_products_map.has_key(brand.name):
                                        brand_to_num_products_map[brand.name] += 1
                                    else:
                                        brand_to_num_products_map[brand.name] = 1

                                    logger.debug('added "%s" to the list of candidates' % brand_product.name)

                            except ObjectDoesNotExist:
                                pass
                            except Exception, e:
                                logger.exception('Failed to process brand product "%s", reason: %s' % (brand_product.name,
                                                                                                       e))

                    # can happen when term contains %
                    except IndexError:
                        pass

                self.num_brand_product_candidates = len(brand_product_candidates)

                if self.num_brand_product_candidates > 0:
                    unknown_product = UnknownProduct(seller_branch_id=gvp_warehouse_branch.id,
                                                     name=self.name,
                                                     ascii_name=product_ascii_name,
                                                     seller_product_code=str(self.id),
                                                     price_with_vat=Money(self.price_with_vat, EUR))
                    unknown_product.save()

                    unknown_product_dir = os.path.join(MEDIA_ROOT, 'product', 'seller', '%d' % seller.id, 'unknown_product')
                    if not os.path.exists(unknown_product_dir):
                        os.makedirs(unknown_product_dir)

                    with open(os.path.join(unknown_product_dir, '%d.json' % unknown_product.id),
                              'w') as output_file:
                        simplejson.dump(brand_product_candidates, output_file)

                    brands = sorted(brand_to_num_products_map, key=brand_to_num_products_map.get)
                    brands.reverse()

                    if len(brands) > 0:
                        with open(os.path.join(unknown_product_dir, '%d_brand.json' % unknown_product.id),
                                  'w') as output_file:
                            output_file.write(brands[0].encode('utf-8'))
                else:
                    unknown_product = UnknownProduct(seller_branch_id=gvp_warehouse_branch.id,
                                                     name=self.name,
                                                     ascii_name=product_ascii_name,
                                                     seller_product_code=str(self.id),
                                                     price_with_vat=Money(self.price_with_vat, EUR),
                                                     is_linkable=False)
                    unknown_product.save()


        return (self.seller_product, self.num_brand_product_candidates)


    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return u'name: %s, id: %d, price with vat: %s' % (self.name, self.id, self.price_with_vat)

    __repr__ = __str__


parser = argparse.ArgumentParser(description='GVP price reader')
parser.add_argument('-l', '--loglevel',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set the logging level')
parser.add_argument('-f', '--logfile',
                    help='File where the logging will be written. If not specified logging will go to console.')

args = parser.parse_args()

if args.loglevel:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=args.loglevel)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=args.loglevel)
else:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

# fetch price list from web
try:
    response = requests.get('%s/-objednavky' % BASE_URL)
    soup = BeautifulSoup(response.content)

    price_list_url = None
    for a_div in soup.findAll('div', {'class': 'files_popis_nazov'}):
        a_href = a_div.find('a')['href']
        if re.search(RE_ORDER_LINK, a_href):
            price_list_url = a_href
            break

    if price_list_url is None:
        raise RuntimeError('Failed to get price list link')

    response = requests.get('%s/%s' % (BASE_URL, price_list_url), stream=True)

    if not os.path.exists(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'gvp_gastro_cash', 'prices')):
        os.makedirs(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'gvp_gastro_cash', 'prices'))
    price_list_file = os.path.join(ITRASH_DATA_ROOT, 'sellers', 'gvp_gastro_cash', 'prices', os.path.basename(price_list_url))

    with open(price_list_file, 'wb') as f:
        for chunk in response:
            f.write(chunk)

    num_unlinked_products_with_candidates = 0
    num_unlinked_products_without_candidates = 0

    wb = open_workbook(price_list_file)
    for sheet in wb.sheets():
        num_rows = sheet.nrows
        for row in range(FIRST_DATA_LN, sheet.nrows):
            values = []
            for col in range(sheet.ncols):
                value = sheet.cell(row,col).value
                if type(value) in (str, unicode):
                    value = value.strip()
                values.append(value)

            if values[PRODUCT_ID_COL].isdigit():
                try:
                    product = Product(*values)

                    logger.debug('[%d / %d] created product "%s"' % (row, num_rows, product))

                    logger.debug('linking "%s" product' % product)

                    seller_product, num_brand_product_candidates = product.link_with_seller_product(seller.id)

                    if seller_product:
                        logger.debug('product "%s" is linked with seller product "%s", price_pkg_with_vat: %s' %
                                     (product, product.seller_product.name,
                                      money_to_string(product.seller_product.price_pkg_with_vat)))
                    elif num_brand_product_candidates > 0:
                        logger.debug('product "%s" is not linked yet but has got %d candidates' % (product,
                                                                                                   num_brand_product_candidates))
                        num_unlinked_products_with_candidates += num_brand_product_candidates
                    else:
                        logger.debug('product "%s" will not be linked as has not got any candidates' % product)
                        num_unlinked_products_without_candidates += 1

                except Exception, e:
                    logger.exception('Failed to create product at row %d, reason: %s' % (row, e))

except Exception, e:
    logger.exception('Failed to process price list, reason: %s' % e)
    sys.exit(1)

logger.info('number of non-zero price products: %d' % SellerProduct.objects.filter(seller_id=seller.id,
                                                                                   price_pkg_with_vat__gt=0).count())
logger.info('number of zero price products: %d' % SellerProduct.objects.filter(seller_id=seller.id,
                                                                               price_pkg_with_vat=0).count())
logger.info('number of unlinked products with candidates: %d' % num_unlinked_products_with_candidates)
logger.info('number of unlinked products without candidates: %d' % num_unlinked_products_without_candidates)