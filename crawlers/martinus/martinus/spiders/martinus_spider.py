import re
import sys

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

from shopper.crawlers.martinus.martinus.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item

class MartinusSpider(CrawlSpider):
    name = "martinus"
    allowed_domains = ["martinus.sk"]
    start_urls = [
        "http://www.martinus.sk/knihy"
    ]

    rules = (Rule(SgmlLinkExtractor(allow=['\?uItem=\d+']), 'parse_item', follow=True),)

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)

        if extract_data_item(hxs.select('//a[@class="selected"]/@href')).find('knihy') == -1:
            return None

        product_item = ProductItem()
        product_item['name'] = extract_data_item(hxs.select('//h1[@itemprop="name"]/text()'))

        return product_item
