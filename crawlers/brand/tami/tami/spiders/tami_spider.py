import re

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

from shopper.crawlers.brand.tami.tami.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

class TamiSpider(CrawlSpider):
	name = "tami"
	allowed_domains = ["www.tami.sk"]
	start_urls = [
        "http://www.tami.sk"        
    ]	

	rules = (Rule(SgmlLinkExtractor(deny=['page/.*', '\?export=pdf'], allow=['stranka/.*']), callback='parse_product_item', follow=True),)			
	
	product_categories = {}
	product_category_mapping = {}
	
		
	def parse_product_item(self, response):
		hxs = HtmlXPathSelector(response)
		
		for product in hxs.select('//div[@class="catalogue"]'):
			product_item = ProductItem()
			product_item['image_urls'] = extract_data_list(product.select('div[@class="catalogue_img"]/a/img/@src'))
			product_item['name'] =  extract_data_item(product.select('div[@class="catalogue_obsah"]/div[@class="catalogue_nadpis"]/text()'))
			product_ids = product.select('div[@class="catalogue_obsah"]/div[@class="tabulka"]/div[@class="catalogue_cislo_produktu"]/div/text()')
			try:
				product_item['id'] = re.findall('\d+', product_ids[0].extract())[0]
			except IndexError:
				continue
			try:
				product_item['bc'] = re.findall('\d+', product_ids[2].extract())[0]
			except IndexError:
				continue
			
			yield product_item 