# -*- coding: utf-8 -*-

import logging
from bs4 import BeautifulSoup
import re

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shopper.crawlers.delia.delia.items import ProductItem

DELIA_URL = 'https://www.edelia.sk'

product_map = dict()

class DeliaSpider(CrawlSpider):
    RE_DISCOUNT_VALID_UNTIL = re.compile('(\d+.\d+)')

    name = 'delia'
    allowed_domains = ['www.edelia.sk']
    start_urls = [DELIA_URL]

    rules = (Rule(LinkExtractor(deny=['vseobecne-obchodne-podmienky', 'casto-kladene-otazky/',
                                      'kontakt', 'nase-predajne'],
                                allow=['\d+-.*?']), callback='parse_items', follow=True),)


    def parse_items(self, response):
        soup = BeautifulSoup(response.body)

        for article in soup.findAll('article', {'class': 'product'}):
            product_url = article.find('a', {'class': 'showproduct'})['href']

            if product_map.has_key(product_url):
                continue

            product_name = article.find('span', {'class': 'title'}).text

            product_image = article.find('img')['src']

            try:
                discount = article.find('span', {'class': 'akcia'}).text.replace('-', '').replace('%', '')
            except AttributeError:
                discount = None

            try:
                discount_valid_until = DeliaSpider.RE_DISCOUNT_VALID_UNTIL.search(article.find('span',
                                                                                   {'class': 'akcia_platnost'}).text).group(1)
            except AttributeError:
                discount_valid_until = None

            try:
                price_old = article.find('span', {'class': 'price_before'}).text
            except AttributeError:
                price_old = None
            price_new = article.find('span', {'class': 'discount_price'}).text

            product_item = ProductItem()
            product_item['name'] = product_name
            product_item['image_urls'] = ['%s%s' % (DELIA_URL, product_image)]
            product_item['url'] = product_url
            product_item['price_old'] = price_old
            product_item['price_new'] = price_new
            product_item['discount'] = discount
            product_item['discount_valid_until'] = discount_valid_until

            product_map[product_url] = product_item

            yield product_item