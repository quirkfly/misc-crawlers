# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class ProductItem(Item):
    name = Field()
    image_urls = Field()
    images = Field()
    url = Field()
    price_old = Field()
    price_new = Field()
    discount = Field()
    discount_valid_until = Field()