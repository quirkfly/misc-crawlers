__author__ = 'peterd'

from moneyed import EUR, Money

from shopper.crawlers.discount import Discount

class MalinaDiscount(Discount):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller, description,
                 new_price, currency=EUR, date_format='%Y-%m-%d'):
        super(MalinaDiscount, self).__init__(url, image_file, name, valid_from, valid_until, seller, description,
                                                new_price, None, currency, date_format)