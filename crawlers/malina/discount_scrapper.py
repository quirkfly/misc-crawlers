# -*- coding: utf-8 -*-

#!/usr/bin/env python
from datetime import datetime, timedelta
from decimal import Decimal
from shopper.crawlers.at.hofer.discount import HoferDiscount

__author__ = 'peterd'

import sys
import argparse
import re
from bs4 import BeautifulSoup
import logging

from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.models import Seller


class MalinaScrapper(DiscountScrapper):
    BASE_URL = 'http://malinastore.com/akciov%C3%A9-let%C3%A1ky.html'

    RE_DISCOUNT_PERIOD = re.compile(r'.*?(\d{8}).*?')

    def __init__(self, logger, seller, sleep_int=None):
        super(MalinaScrapper, self).__init__(seller, logger, sleep_int)

    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        try:
            response = self.session.get(MalinaScrapper.BASE_URL)
            soup = BeautifulSoup(response.content)

            # TODO: completed once striked a deal with the retailer

        except Exception, e:
            self.logger.exception(u'Failed to get %s content, reason: %s' % (MalinaScrapper.BASE_URL, e))

        return self.discount_map


parser = argparse.ArgumentParser(description='malina discount scrapper utility')
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--teaser-only', '-t', dest="teaser_only", action='store_true',
                    help='Scraps only few discounts.')

args = parser.parse_args()

malina = Seller.objects.get(name='Malina')

log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)

logger.info('Scrapping discounts...')

discount_scrapper = MalinaScrapper(logger=logger, seller=malina,
                                  sleep_int=0)
discount_scrapper.scrap(teaser_only=args.teaser_only)
discount_scrapper.send_mail()