# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field


class ProductItem(Item):
	category = Field()
	id = Field()
	bc = Field()
	name = Field()
	desc = Field()
	brand = Field()
	image_urls = Field()
	images = Field()
	brand = Field()
	details = Field()