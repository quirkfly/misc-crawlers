import re

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

from shopper.crawlers.bioland.bioland.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

class BiolandSpider(CrawlSpider):
    name = "bioland"
    allowed_domains = ["www.bioland.sk"]
    start_urls = [
        "http://www.bioland.sk/produkty.aspx"        
    ]	

    rules = (Rule(SgmlLinkExtractor(allow=['kategoria.*']), follow=True),
             Rule(SgmlLinkExtractor(allow=['\d+-.*']), callback='parse_product_item', follow=True),)

    product_categories = {}
    product_category_mapping = {}


    def parse_product_item(self, response):
        hxs = HtmlXPathSelector(response)

        product_item = ProductItem()

        product_categories = []
        for product_category in hxs.select('//*[@id="ctl00_CtrlObsah1_PanDetailKategoriaMapa"]/a/text()'):
            product_categories.append(product_category.extract())
        product_item['category'] = '/'.join(product_categories)

        product_item['name'] = extract_data_item(hxs.select('//div[@class="nadpis"]/span/text()'))

        image_big_url = extract_data_item(hxs.select('//div[@class="fotka"]/a/img/@src'))
        if image_big_url:
            image_big_url = 'http://www.bioland.sk/%s' % image_big_url
            image_small_url = image_big_url.replace('.jpg', '_small.jpg')
            product_item['image_urls'] = [image_big_url, image_small_url]
        else:
            product_item['image_urls'] = []

        product_item['desc'] = extract_data_item(hxs.select('//div[@class="popis"]/text()'))
        product_item['id'] = extract_data_item(hxs.select('//*[@id="ctl00_CtrlObsah1_PanDetailKod"]/div/div/h3/text()'))
        product_item['brand'] = extract_data_item(hxs.select('//*[@id="ctl00_CtrlObsah1_PanVyrobca"]/div/div/h3/text()'))
        product_item['bc'] = extract_data_item(hxs.select('//*[@id="ctl00_CtrlObsah1_PanDetailEan"]/div/div/h3/text()'))
        product_item['details'] = hxs.select('//*[@id="ctl00_CtrlObsah1_PanDetailPopis"]/*/text()').extract()

        return product_item