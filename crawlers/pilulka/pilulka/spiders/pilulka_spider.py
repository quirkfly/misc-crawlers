# -*- coding: utf-8 -*-

import logging
import requests
from bs4 import BeautifulSoup

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shopper.crawlers.pilulka.pilulka.items import ProductItem

PILULKA_URL = 'https://www.pilulka.sk'

class PilulkaSpider(CrawlSpider):
    name = 'pilulka'
    allowed_domains = ['www.pilulka.sk']
    start_urls = [PILULKA_URL]

    rules = (Rule(LinkExtractor(allow=['.*?']), callback='parse_items', follow=True),)


    def parse_items(self, response):
        soup = BeautifulSoup(response.body)

        product_li_list = soup.find('div', {'class': 'product-list'})

        if product_li_list is not None:
            for product_li in product_li_list.findAll('li'):
                try:
                    product = Product(product_li.find('span', {'class': 'name'}).text,
                                      product_li.find('img')['src'],
                                      product_li.find('a')['href'])
                    product_item = ProductItem()
                    product.to_item(product_item)

                    yield product_item

                except Exception, e:
                    logging.exception('Failed to parse product li "%s", reason: %s' % (product_li, e))



class Product(object):
    def __init__(self, name, image, url):
        self.name = name
        self.image_urls = ['%s%s' % (PILULKA_URL, image)]
        self.url = url

        response = requests.get('%s%s' % (PILULKA_URL, self.url))
        soup = BeautifulSoup(response.content)

        self.path = soup.find('p', {'id': 'breadcrumb'}).text

        self.price_old = None
        try:
            self.price_old = soup.find('del', {'id': 'priceOld'}).text
        except Exception, e:
            logging.warning('Failed to parse old price, reason: %s', e)

        self.price_new = None
        try:
            self.price_new = soup.find('strong', {'id': 'priceNew'}).text
        except Exception, e:
            logging.warning('Failed to parse new price, reason: %s', e)

        self.availability = None
        try:
            self.availability = soup.find('span', {'id': 'availability'}).text.strip('\n\t')
        except Exception, e:
            logging.warning('Failed to parse availability, reason: %s', e)

        self.ean = None
        got_ean = False
        for i, product_param_th in enumerate(soup.find('div', {'id': 'product-params'}).findAll('th')):
            if product_param_th.text.find('EAN') != -1:
                got_ean = True
                break

        if got_ean:
            try:
                self.ean = soup.find('div', {'id': 'product-params'}).findAll('td')[i].text.strip('\n\t')
            except Exception, e:
                logging.warning('Failed to parse EAN, reason: %s', e)

        self.brand = None
        got_brand = False
        for i, product_param_th in enumerate(soup.find('div', {'id': 'product-params'}).findAll('th')):
            if product_param_th.text.find(u'Značka') != -1:
                got_brand = True

        if got_brand:
            try:
                self.brand = soup.find('div', {'id': 'product-params'}).findAll('td')[i].text.strip('\n\t')
            except Exception, e:
                logging.warning('Failed to parse brand, reason: %s', e)

        self.supplier = None
        got_supplier = False
        for i, product_param_th in enumerate(soup.find('div', {'id': 'product-params'}).findAll('th')):
            if product_param_th.text.find(u'Dodávateľ na trh') != -1:
                got_supplier = True

        if got_supplier:
            try:
                self.supplier = soup.find('div', {'id': 'product-params'}).findAll('td')[i].text.strip('\n\t')
            except Exception, e:
                logging.warning('Failed to parse supplier, reason: %s', e)

        self.description = None
        try:
            self.description = soup.find('div', {'id': 'tab-info'}).text
        except Exception, e:
            logging.warning('Failed to parse description, reason: %s', e)

        self.usage_doc = None
        try:
            self.usage_doc = soup.find('div', {'id': 'att-files'}).find('a')['href']
        except Exception, e:
            logging.warning('Failed to parse usage doc, reason: %s', e)



    def to_item(self, product_item):
        product_item['name'] = self.name
        product_item['image_urls'] = self.image_urls
        product_item['url'] = self.url
        product_item['path'] = self.path
        product_item['price_old'] = self.price_old
        product_item['price_new'] = self.price_new
        product_item['availability'] = self.availability
        product_item['ean'] = self.ean
        product_item['brand'] = self.brand
        product_item['supplier'] = self.supplier
        product_item['description'] = self.description
        product_item['usage_doc'] = self.usage_doc

        return product_item


    def __unicode__(self):
        return self.name