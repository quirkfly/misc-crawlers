# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class ProductItem(Item):
    name = Field()
    image_urls = Field()
    images = Field()
    url = Field()
    path = Field()
    price_old = Field()
    price_new = Field()
    availability = Field()
    ean = Field()
    brand = Field()
    supplier = Field()
    description = Field()
    usage_doc = Field()