#!/bin/bash

function usage {
	echo "usage: $SELF <seller>"
	exit 1
}


while getopts nl:d: FLAG; do
        case $FLAG in
        n)
                USE_HTTP_PROXY=0;
                ;;
    l)
                LOG_DIR=$OPTARG;
        ;; 
    d)
                DATA_DIR=$OPTARG;
        ;;
    ?)
        usage;
        ;;
  esac
done

shift $(( OPTIND - 1 ));

test $# -lt 1 && usage 

SELF=`basename $0`
SELLER=$1
SELLER_BRANCH_ID=$2

kill $(ps aux | grep $SELLER | grep -v grep | grep -v $SELF | awk '{print $2}')
if [ $? -ne 0 ]
then
	echo "Failed to kill crawler."
	exit 1
fi

#
# write scrapped items do db
#

cd $ITRASH_HOME_DIR

DJANGO_SETTINGS_MODULE=settings PYTHONPATH=`pwd`:.. python2.7 shopper/writers/$SELLER/${SELLER}_writer.py --logfile $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log $DATA_DIR/${SELLER}${SELLER_BRANCH_ID}_products.jsonlines 2>$LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log
RV=$?

if [ ! $RV -eq 0 ]
then
        tail -100 $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log > $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        python2.7 utils/mailer.py --from_addr ${SELLER}_writer@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to write $SELLER (seller_branch_id=$SELLER_BRANCH_ID) products to database." $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        rm $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        exit 2
fi

#
# check writer log for errors
#

NUM_WRITE_ERRORS=`grep -i error $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log | wc -l`
if [ $NUM_WRITE_ERRORS -gt 0 ]
then
        grep -i error $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log > $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        python2.7 utils/mailer.py --from_addr ${SELLER}_writer@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to write $NUM_WRITE_ERRORS $SELLER (seller_branch_id=$SELLER_BRANCH_ID)  products to database." $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        rm $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
        exit 2
fi
