"""
Created on Mar 3, 2013

@author: peterd
"""

from urlparse import urljoin 


def extract_data_item(selector):
    data = selector.extract()
    if type(data) == list and len(data) > 0:
        data = data[0]
        data = data.strip(' \r\n\t')
        return data
    else:
        return None
    

def extract_data_list(selector, prefix=None):
    data = []
    
    for item in selector.extract():
        item = item.strip(' \r\n\t')
        if prefix is None:
            data.append(item)
        else:
            data.append(urljoin(prefix, item))    
    
    return data

