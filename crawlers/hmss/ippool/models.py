from django.db import models
from django.utils.timezone import now

# Create your models here.

class Host(models.Model):
    ipaddress = models.CharField(max_length=128)
    port = models.IntegerField()
    type = models.CharField(max_length=16, null=True, blank=True)
    is_alive = models.BooleanField(default=True)
    added_on = models.DateTimeField(default=now)
    used_on = models.DateTimeField(null=True, blank=True)

    def __eq__(self, other):
        return self.ip == other.ip and self.port == other.port

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return '%s:%s, is_alive = %s, added_on = %s, used_on = %s' % (self.ipaddress, self.port, self.is_alive, self.added_on, self.used_on)

    __repr__ = __str__
