#!/usr/bin/env python

# mark supplied host as dead

import sys

from ippool.ippool_manager import set_is_alive

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print 'usage: %s <host>' % sys.argv[0]
		sys.exit(-1)
		
	host = sys.argv[1].split(':')[0]
	port = sys.argv[1].split(':')[1]
	
	set_is_alive(host, port, False)