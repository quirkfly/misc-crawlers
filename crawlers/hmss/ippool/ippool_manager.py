#!/usr/bin/env python

import argparse

import django
django.setup()
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from ippool.models import Host

AVERAGE_RESPONSE_TIME_SECS = 5      # the higher the number the higher unique host pickup rate


def get_host(ns=None):
    """Prints a host address in format <ip>:<port>:<type> to stdout if available, "no host available" otherwise."""

    try:
        got_host = False
        for host in Host.objects.filter(is_alive=True).order_by('-used_on'):
            now = timezone.now()
            # ensure different host is picked up if between two calls to get_host() elapses more than
            # AVERAGE_RESPONSE_TIME_SECS
            if not host.used_on or (now - host.used_on).seconds > AVERAGE_RESPONSE_TIME_SECS:
                got_host = True

                print '%s:%s:%s' % (host.ipaddress, host.port, host.type)

                host.used_on = now
                host.save(update_fields=['used_on'])

                break

        if not got_host:
            print 'no host available, reason: all hosts busy or dead'
    except Exception, e:
        print 'no host available, reason: %s' % e


def on_host_unreachable(ns=None, ip=None, port=None):
    if ns is None:
        ip = ip
        port = port
    else:
        args = vars(ns)
        ip = args['ip']
        port = args['port']

    try:
        host = Host.objects.get(ipaddress=ip, port=port)
        host.is_alive = False
        host.save()
    except ObjectDoesNotExist:
        pass


parser = argparse.ArgumentParser('IP Pool Manager Utility')
subparsers = parser.add_subparsers(help='commands')

# get_ cmd parser
get_host_parser = subparsers.add_parser('get_host', help='Returns a host address in format <ip>:<port>:<type>')
get_host_parser.set_defaults(func=get_host)

# host_unreachable cmd parser
host_unreachable_parser = subparsers.add_parser('host_unreachable', help='Reports an unreachable host')
host_unreachable_parser.add_argument('ip', action='store')
host_unreachable_parser.add_argument('port', action='store')
host_unreachable_parser.set_defaults(func=on_host_unreachable)

args = parser.parse_args()
args.func(args)

