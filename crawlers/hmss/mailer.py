import argparse
import datetime
from smtplib import SMTP

from mailer_conf import * 

parser = argparse.ArgumentParser(description='mail client')
parser.add_argument('msg_body_file', help='File containing message body.')
parser.add_argument('--from_addr', '-f', help='message sender')
parser.add_argument('--to_addr', '-t', help='message recepient')
parser.add_argument('--subject', '-s', help='message subject')
args = parser.parse_args()

fp = open(args.msg_body_file, 'rb')
msg_body = fp.read()
fp.close()

smtp = SMTP(SMTP_SERVER, SMTP_PORT)
smtp.starttls()
debuglevel = 0
smtp.set_debuglevel(debuglevel)
smtp.login(USER, PASS)

date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
msg = "From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" % ( args.from_addr, args.to_addr, args.subject, date, msg_body)

smtp.sendmail(args.from_addr, args.to_addr, msg)
smtp.quit()
