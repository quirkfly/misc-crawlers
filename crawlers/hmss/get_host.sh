#!/bin/bash

test ! $# -eq 1 && echo "usage: get_host.sh <HMSS_HOME_DIR>" && exit 1

cd $1

DJANGO_SETTINGS_MODULE=hmss_settings PYTHONPATH=`pwd`:$PYTHONPATH ippool/ippool_manager.py get_host 
