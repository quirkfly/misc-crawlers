import logging
import re

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector

from bs4 import BeautifulSoup

from crawler.hmss.items import IPItem

RE_STYLE = r'\n*(?P<name>\.[-\w]+)\{(?P<val>.*?)\}'
RE_CLASS_ELEM = r'<(?:span|div) class="([-\w]+)">([\.\w]+)</(?:span|div)>'
RE_SUB_DISP_NONE = r'<(span|div) style="display:none".*?</(span|div)>'
RE_IP_FRAG = r'>([\d.]+)<'
RE_IP = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
RE_PORT = r'\d{2,4}'
RE_SPEED = r'width: (\d+).*'

MIN_ACCEPTED_SPEED = 70

class HideMyAssSpider(CrawlSpider):
    name = 'hidemyass'
    # start_urls = ['http://proxylist.hidemyass.com/']
    # allowed_domains = ['proxylist.hidemyass.com']
    # start_urls = ['https://www.sslproxies.org/']
    # allowed_domains = ['www.sslproxies.org']
    start_urls = ['https://list.proxylistplus.com/SSL-List-1']
    allowed_domains = ['list.proxylistplus.com']

    rules = (Rule(LinkExtractor(allow=('SSL.*'),), callback='parse_ip_item', follow=True),)

    def parse_ip_item(self, response):
        logging.debug('url: %s' % response.url)
        try:
            items = []

            soup = BeautifulSoup(response.body)

            for row in soup.find('table', {'class': 'bg'}).findChildren('tr', {'class': 'cells'}):
                tokens = row.findChildren('td')
                item = IPItem()
                item['ipaddress'] = tokens[1].text
                item['port'] = tokens[2].text
                item['type'] = 'HTTPS'

                items.append(item)

            return items
        except AttributeError:
            return None

    def parse_ip_item_hmss(self, response):
        hxs = Selector(response)

        links = hxs.xpath('//tr[@rel]')
        items = []
        for link in links:
            # skip other than fast ones
            speed_indicator = re.search(RE_SPEED, link.xpath('td[6]/div/div').extract()[0])
            if int(speed_indicator.group(1)) < MIN_ACCEPTED_SPEED:
                continue

            type = link.xpath('td[7]/text()').extract()[0].strip('\n ')

            # skip other than higly anonymous
            if link.xpath('td[8]/text()').extract()[0].find('High') == -1:
                continue

            data = link.extract()
            styles = dict(re.findall(RE_STYLE, data))

            for class_elem in re.findall(RE_CLASS_ELEM, data):
                style = '.%s' % class_elem[0]
                if styles.has_key(style):
                    data = re.sub(r'class="%s"' % class_elem[0], 'style="%s"' % styles[style], data)

            data = re.sub(RE_SUB_DISP_NONE, '', data)

            ip_frag = re.findall(RE_IP_FRAG, data)
            ipaddress = ''.join(ip_frag)
            port = link.xpath('td[3]/text()').extract()[0].strip('\n ')
            if re.match(RE_IP, ipaddress) and re.match(RE_PORT, port):
                item = IPItem()
                item['ipaddress'] = ipaddress
                item['port'] = port
                item['type'] = type
                items.append(item)
        return items
