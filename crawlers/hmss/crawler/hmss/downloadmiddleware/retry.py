"""
Created on Mar 10, 2013

@author: peterd

Retry handler integrated with ippool proxy manager
"""

import logging

import os
import re

from scrapy.downloadermiddlewares.retry import RetryMiddleware

from ippool import host_mgr


class ProxyRetryMiddleware(RetryMiddleware):
    def __init__(self, settings):
        RetryMiddleware.__init__(self, settings)
        
    def _retry(self, request, reason, spider):
        retries = request.meta.get('retry_times', 0) + 1

        if retries <= self.max_retry_times:
            proxy = request.meta['proxy']
            
            if proxy:
                m = re.search('http://(.*?):(\d+)', proxy)
                if m:
                    is_proxy_alive = host_mgr.is_alive(m.group(1), m.group(2))
                    
                    if is_proxy_alive:
                        logging.info('Retrying request %s, failed %d times, reason: %s' % (request, retries, reason))

                        retryreq = request.copy()
                        retryreq.meta['retry_times'] = retries
                        retryreq.dont_filter = True
                        retryreq.priority = request.priority + self.priority_adjust
                        return retryreq
                    else:
                        # get new proxy
                        proxy = host_mgr.get_host()
                        if proxy is not None:                                    
                            logging.info('Retrying %s (failed %d times): %s Setting http proxy to %s' % (
                                                                                                         request,
                                                                                                         retries,
                                                                                                         reason,
                                                                                                         proxy))
                            retryreq = request.copy()
                            retryreq.meta['proxy'] = 'http://%s:%s' % (proxy['ipaddress'], proxy['port'])
                            retryreq.meta['retry_times'] = retries
                            retryreq.dont_filter = True
                            retryreq.priority = request.priority + self.priority_adjust
                            os.environ['live_http_proxy'] = retryreq.meta['proxy'] 
                            return retryreq
                        else:
                            logging.info('Gave up retrying %s (failed %d times): %s Failed to obtain a '
                                         'proxy from ippool.' % (spider, request, retries, reason))
            else:
                logging.info('Gave up retrying %s (failed %d times): %s Proxy not set.' % (
                                                                                           request,
                                                                                           retries,
                                                                                           reason))
        else:
            # mark current proxy as dead
            proxy = request.meta['proxy'] if request.meta.has_key('proxy') else None
            if proxy:
                m = re.search('http://(.*?):(\d+)', proxy)
                if m:
                    host_mgr.set_is_alive(m.group(1), m.group(2), False)
            # get new proxy
            proxy = host_mgr.get_host()
            if proxy is not None:                                    
                logging.info('Gave up retrying %s (failed %d times): %s Setting '
                             'http proxy to %s and retrying again.' % (
                                                                       request,
                                                                       retries,
                                                                       reason,
                                                                       proxy))
                retryreq = request.copy()
                retryreq.meta['proxy'] = 'http://%s:%s' % (proxy['ipaddress'], proxy['port'])
                retryreq.meta['retry_times'] = 0
                retryreq.dont_filter = True
                retryreq.priority = request.priority + self.priority_adjust
                os.environ['live_http_proxy'] = retryreq.meta['proxy']
                return retryreq
            else:
                logging.info('Gave up retrying %s (failed %d times): %s Failed to '
                             'obtain a proxy from ippool.' % (
                                                              request,
                                                              retries,
                                                              reason))
