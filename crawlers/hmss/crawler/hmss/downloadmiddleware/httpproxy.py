"""
Created on Mar 10, 2013

@author: peterd

"""

import os

from scrapy.downloadermiddlewares.httpproxy import HttpProxyMiddleware

class LiveHttpProxyMiddleware(HttpProxyMiddleware):
    def __init__(self, auth_encoding):
        HttpProxyMiddleware.__init__(self, auth_encoding)
        
    def _set_proxy(self, request, scheme):
        if os.environ.has_key('live_http_proxy'):            
            request.meta['proxy'] = os.environ['live_http_proxy']
        else:
            super(LiveHttpProxyMiddleware, self)._set_proxy(request, scheme) 