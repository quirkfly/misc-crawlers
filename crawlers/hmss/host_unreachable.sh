#!/bin/bash

test ! $# -eq 3 && echo "usage: host_unreachable.sh <HMSS_HOME_DIR> <ip> <port>" && exit 1

cd $1

DJANGO_SETTINGS_MODULE=hmss_settings PYTHONPATH=`pwd`:$PYTHONPATH ippool/ippool_manager.py host_unreachable $2 $3
