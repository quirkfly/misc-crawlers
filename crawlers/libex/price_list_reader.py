# -*- coding: utf-8 -*-
#!/usr/bin/env python

import argparse
from decimal import Decimal
import logging
import os
import re
import subprocess
from datetime import datetime
from django.core.mail import send_mail
from moneyed import EUR
from moneyed import Money
import requests
from bs4 import BeautifulSoup

import django
django.setup()

from shopper.models.household import HouseholdMemberRole
from shopper.crawlers.libex.discount_scrapper import LibexScrapper
from itrash.itrash_settings import ITRASH_DATA_ROOT
from shopper.models.product import Seller

BASE_URL = 'http://www.libex.sk/'

RE_DISCOUNT_PERIOD = re.compile(r'(\d+\.\d+\.).*?(\d+\.\d+\.)')
RE_ORDER_NUM = re.compile(r'^(\d{7})$')
RE_PRICE = re.compile(r'^(\d+,\d+)$')

parser = argparse.ArgumentParser(description='GVP price reader')
parser.add_argument('-l', '--loglevel',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set the logging level')
parser.add_argument('-f', '--logfile',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--send-email', choices=('y', 'n'), default='y',
                    help='Send scrapping results by email. (default is y)')

args = parser.parse_args()

if args.loglevel:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=args.loglevel)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=args.loglevel)
else:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

seller = Seller.objects.get(name='Libex')

num_scrapped_products = 0
num_must_recategorize_products = 0
num_errors = 0

try:

    if not os.path.exists(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'libex', 'prices')):
        os.makedirs(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'libex', 'prices'))
    price_list_dir = os.path.join(ITRASH_DATA_ROOT, 'sellers', 'libex', 'prices')

    price_lists = []
    response = requests.get('%s' % BASE_URL)
    soup = BeautifulSoup(response.content)

    for action_info in soup.findAll('div', {'class': 'action-info'}):
        m = RE_DISCOUNT_PERIOD.search(action_info.find('h2').text)
        if m:
            discount_valid_from = datetime.strptime('%s2017' % m.group(1), '%d.%m.%Y').date()
            discount_valid_until = datetime.strptime('%s2017' % m.group(2), '%d.%m.%Y').date()

            for a_link in action_info.findAll('a', {'class': 'link-w-icon'}):
                if a_link['href'].find('obj_formular') != -1 and \
                    not os.path.exists(os.path.join(price_list_dir, os.path.basename(a_link['href']))):
                    price_lists.append(dict(discount_valid_from=discount_valid_from,
                                            discount_valid_until=discount_valid_until,
                                            url=a_link['href']))

    for price_list in price_lists:
        try:
            logger.info('processing price list %s...' % price_list['url'])

            price_list_src_file = os.path.join(price_list_dir, os.path.basename(price_list['url']))

            response = requests.get(price_list['url'], stream=True)

            with open(price_list_src_file, 'wb') as f:
                for chunk in response:
                    f.write(chunk)

            order_numbers = []
            prices = []

            subprocess.call(['pdftotext', price_list_src_file])

            price_list_txt_file = price_list_src_file.replace('pdf', 'txt')

            with open(price_list_txt_file, 'r') as f:
                for ln in f:
                    ln = ln.replace('\n', '').strip()
                    m = RE_ORDER_NUM.search(ln)
                    if m:
                        order_numbers.append(ln)
                    else:
                        m = RE_PRICE.search(ln)
                        if m:
                            prices.append(Money(Decimal(ln.replace(',', '.')), EUR))

            if len(order_numbers) != len(prices):
                raise RuntimeError('Failed to process price list, got %d order numbers but %d prices!' % (len(order_numbers),
                                                                                                          len(prices)))

            discount_scrapper = LibexScrapper(logger, seller, discount_valid_from, discount_valid_until,
                                              order_numbers, prices, sleep_int=0, dont_torify=False)
            discount_map = discount_scrapper.scrap()
            num_scrapped_products += discount_map['num_scrapped_products']
            num_must_recategorize_products += discount_map['num_must_recategorize_products']
            num_errors += discount_map['num_errors']
        except Exception, e:
            num_errors += 1
            logger.exception('Failed to process price list %s, reason: %s' % (price_list['url'], e))
except Exception, e:
    num_errors += 1
    logger.exception('Failed to retrieve price lists, reason: %s' % e)

logger.info('num_scrapped_products: %d' % num_scrapped_products)
logger.info('num_must_recategorize_products: %d' % num_must_recategorize_products)
logger.info('num_errors: %d' % num_errors)

if args.send_email == 'y':
    senior_discount_editor_emails = []
    for discount_editor in HouseholdMemberRole.get_senior_discount_editors():
        if not discount_editor.is_locust:
            senior_discount_editor_emails.append(discount_editor.user.email)

    if num_scrapped_products > 0:
        send_mail(u'%d libex products have been scrapped' % num_scrapped_products,
                  None, 'discount.scrapper@20deka.com', senior_discount_editor_emails)

    if num_must_recategorize_products > 0:
        send_mail(u'%d libex products must be recategorized' % num_must_recategorize_products,
                  None, 'discount.scrapper@20deka.com', senior_discount_editor_emails)

    if num_errors > 0:
        send_mail(u'%d libex products have not been scrapped, see log file for details.' % num_errors,
                  None, 'discount.scrapper@20deka.com', senior_discount_editor_emails)