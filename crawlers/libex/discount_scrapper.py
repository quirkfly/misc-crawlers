# -*- coding: utf-8 -*-
import os

import re
from bs4 import BeautifulSoup
from django.db.models import Q
from infra.lexical import CharMapper
from itrash.settings import MEDIA_ROOT
from shopper.crawlers.discount import ImageLink

from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.exception import ProductUnitDoesNotExist
from shopper.models.product import BrandProduct, get_product_amount_and_unit, ProductUnit, \
    seller_product_discount_get_full_image_path, find_brand, Brand, BrandProductCategory, SellerProductCategory, \
    CommonProductCategory, SellerBranch, SellerProduct


class LibexScrapper(DiscountScrapper):
    BASE_URL = 'http://eshop.libex.sk/'

    RE_PKG_SIZE = re.compile(u'Balenie:(\d+).*?')
    RE_MIN_ORDER_SIZE = re.compile(u'Minimálne vydávané množstvo \(MvM\):(\d+).*?')
    RE_EAN_PC = re.compile(u'EAN:(\d{8,13}).*?')
    TEASER_SIZE = 10

    def __init__(self, logger, seller, discount_valid_from, discount_valid_until,
                 order_numbers, prices, sleep_int=None, dont_torify=False):
        super(LibexScrapper, self).__init__(seller, logger, sleep_int, dont_torify)

        self.discount_valid_from = discount_valid_from
        self.discount_valid_until = discount_valid_until
        self.order_numbers = order_numbers
        self.prices = prices

        self.brand_product_category = BrandProductCategory.objects.get(name='Unknown')
        self.seller_product_category = SellerProductCategory.get_or_create_default_seller_product_category(seller.id)
        self.other_common_product_category = CommonProductCategory.get_common_product_category_by_name(u'Ostatné')
        self.default_seller_branch = SellerBranch.get_or_create_default_seller_branch(seller.id)[0]


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        num_scrapped_products = 0
        num_must_recategorize_products = 0
        num_errors = 0

        for i, order_number in enumerate(self.order_numbers):
            try:
                response = self.session.get('%s/default.aspx?content=SHOP&nparams=typ_t;;search;%s;rozsah;G;kde;ALL;ako;ALL'
                                        % (LibexScrapper.BASE_URL, order_number))
                soup = BeautifulSoup(response.content)
                product_details_link = soup.find('div', {'class': 'popisMain0'}).find('a')['href']
                response = self.session.get('%s%s' % (LibexScrapper.BASE_URL, product_details_link))
                soup = BeautifulSoup(response.content)
                product_table_rows = soup.find('table', {'class': 'TBmainPROD'}).findAll('tr')
                pkg_size = int(LibexScrapper.RE_PKG_SIZE.search(product_table_rows[1].text.replace('\n', '').strip()).group(1))
                min_order_size = int(LibexScrapper.RE_MIN_ORDER_SIZE.search(product_table_rows[2].text.replace('\n', '').strip()).group(1))
                ean_pc = LibexScrapper.RE_EAN_PC.search(product_table_rows[4].text.replace('\n', '').strip()).group(1)
                brand_product_filter = BrandProduct.objects.filter(
                    Q(ean_pc=ean_pc) | Q(ean_crt=ean_pc) | Q(ean_pc_int=ean_pc) | Q(ean_crt_int=ean_pc))
                if brand_product_filter.exists():
                    brand_product = brand_product_filter[0]

                    self.logger.info('brand product "%s" already in db' % brand_product.name)
                else:
                    product_name_toks = soup.find('div', {'class': 'productName'}).text.split('\r\n')
                    product_name = product_name_toks[0].strip()
                    product_ascii_name = CharMapper.cp1250_to_ascii(product_name).lower()
                    product_amount, product_unit_sym = get_product_amount_and_unit(product_ascii_name)

                    try:
                        product_unit = ProductUnit.get_product_unit_by_name(product_unit_sym)
                    except ProductUnitDoesNotExist:
                        self.logger.warning('Failed to get product unit for product "%s" (ean = %s), '
                                       'using product unit sym "%s", will use kg as unit' % (product_name,
                                                                                             ean_pc,
                                                                                             product_unit_sym))
                        product_unit = ProductUnit.get_product_unit_by_name('kg')

                    image_link = ImageLink(soup.find('a', {'rel': 'lightbox-tovar'}).find('img'),
                                           base_url=LibexScrapper.BASE_URL)
                    image_link.fetch()

                    image_full_rel_path = seller_product_discount_get_full_image_path(None, image_link.rel_image_path)
                    image_full_abs_path = os.path.join(MEDIA_ROOT, image_full_rel_path)

                    brand = find_brand(product_name)
                    if brand is None:
                        brand = Brand.get_default_brand()

                    brand_product = BrandProduct.create_brand_product(brand=brand,
                                              brand_product_category=self.brand_product_category,
                                              common_product_category_id=self.other_common_product_category.id,
                                              name=product_name,
                                              ascii_name=product_ascii_name,
                                              ean_pc=ean_pc,
                                              unit_id=product_unit.id,
                                              product_image_file=image_full_abs_path,
                                              amount_input_type='pc',
                                              amount_input_base=1)

                    self.logger.info('Inserted brand product "%s" to db' % brand_product.name)

                if brand_product.description is None:
                    product_description = soup.find('div', {'class': 'productName'}).find('table').text.replace('\n', '')
                    if len(product_description) > 0:
                        brand_product.description = product_description
                        brand_product.save(update_fields=['description'])

                if not SellerProduct.objects.filter(seller_id=self.seller.id, seller_product_code=order_number).exists():
                    seller_product = SellerProduct.create_seller_product(product_name=brand_product.name,
                                         product_ascii_name=brand_product.ascii_name,
                                         seller_product_code=order_number,
                                         seller_branch=self.default_seller_branch,
                                         ean_pc=brand_product.ean_pc,
                                         ean_pc_int=None,
                                         ean_crt=None,
                                         ean_crt_int=None,
                                         product_unit_id=brand_product.unit_id,
                                         brand_product_id=brand_product.id,
                                         seller_id=self.seller.id,
                                         seller_product_category_id=self.seller_product_category.id,
                                         discount_valid_from=self.discount_valid_from,
                                         discount_valid_until=self.discount_valid_until,
                                         price_pkg_with_vat=self.prices[i],
                                         must_recategorize=brand_product.common_product_category == self.other_common_product_category)

                    num_scrapped_products += 1
                    if seller_product.must_recategorize:
                        num_must_recategorize_products += 1

                    self.logger.info('Inserted seller product "%s" to db' % seller_product.name)
                else:
                    self.logger.info('seller product "%s" already in db' % brand_product.name)

            except Exception, e:
                num_errors += 1
                self.logger.exception('Failed to process product details (order number: %s), reason: %s' % (order_number, e))

            if teaser_only and i == LibexScrapper.TEASER_SIZE - 1:
                break

        self.discount_map['num_scrapped_products'] = num_scrapped_products
        self.discount_map['num_must_recategorize_products'] = num_must_recategorize_products
        self.discount_map['num_errors'] = num_errors

        return self.discount_map