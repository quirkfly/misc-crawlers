import json

from bs4 import BeautifulSoup

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shopper.crawlers.t3.t3.items import ProductCategory

class T3Spider(CrawlSpider):
    name = 't3'
    base_url = 'https://potravinydomov.itesco.sk'
    allowed_domains = ['potravinydomov.itesco.sk']
    start_urls = [
        'https://potravinydomov.itesco.sk/groceries/sk-SK/shop'
    ]


    rules = (Rule(LinkExtractor(allow=(r'sk-SK/shop/.*'), deny=('en-GB', '\?', 'all', r'.*/products/\d+', 'tel:.*')),
                                callback='parse_product_category',
                                follow=True),
             )


    def parse_product_category(self, response):
        """Returns ProductCategory instance if url contains products, None otherwise."""

        product_category = None

        soup = BeautifulSoup(response.body)
        data_props = json.loads(soup.find('html')['data-props'])

        try:
            if data_props['resources']['productsByCategory']['data']['results'].has_key('productItems'):
                product_category = ProductCategory(url=response.url)
        except KeyError:
            pass

        return product_category
