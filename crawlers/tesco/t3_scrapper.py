# -*- coding: utf-8 -*-

#
# tesco product scrapper
#

import argparse
from dateutil.parser import parse
import json
import logging
import os
import re
from decimal import Decimal
from datetime import date
from time import sleep
import sys
from bs4 import BeautifulSoup

import django
from django.core.files.base import ContentFile
from django.core.mail import send_mail
from moneyed import EUR, Money
import requests
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist

from infra.lexical import CharMapper
from itrash.settings import DATA_ROOT, THUMB_SMALL_SIZE, DEFAULT_FROM_EMAIL, CEO_EMAIL
from shopper.crawlers.discount import ImageLink
from shopper.crawlers.t3.t3.spiders.t3_spider import T3Spider
from shopper.exception import CommonProductCategoryDoesNotExist
from shopper.models.product import SellerProduct, Seller, SellerBranch, Brand, BrandProductCategory, product_get_full_image_path, \
    product_get_small_thumb_image_path, product_get_big_thumb_image_path, ProductUnit, SellerProductCategory, \
    PriceTag, BrandProduct, find_brand, CommonProductCategoryMapping, CommonProductCategory
from shopper.models.helpers import resize_image

django.setup()

RE_PROMO_TEXT = re.compile(r'(?P<discount>-[,\d\s]+%).*?predt\xfdm\s+(?P<price_before>[,\d]+).*?teraz\s(?P<price_now>[,\d]+)')
RE_PRODUCT_ID = re.compile(r'products/(\d+)')
RE_PAGE = re.compile(r'page=(\d+)')

missing_seller_product_categories = []

parser = argparse.ArgumentParser(description='tesco product scrapper')
parser.add_argument('categories_dump', help='File containing category entries in jsonline format.')
parser.add_argument('--verbose', '-v', action='count', help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f', help='File where the logging will be written. If not specified logging will go to console.')
args = parser.parse_args()

log_level = logging.INFO
if args.verbose > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)


# get seller instance
seller = None
try:
    seller = Seller.objects.get(name='Tesco')
except ObjectDoesNotExist:
    logger.error('Failed to get seller object (did you provision it?)')
    sys.exit(1)

# get seller branch
seller_branch = None
try:
    #seller_branch = SellerBranch.objects.get(location__name='Tesco Potraviny domov')
    seller_branch = SellerBranch.objects.get(seller_id=seller.id, location__name='Unknown')
    seller_branch_currency = seller_branch.location.state.currency
except ObjectDoesNotExist:
    logger.error('Failed to get seller branch  (did you provision it?)')
    sys.exit(1)

# get default brand
brand = None
try:
    brand = Brand.objects.get(name='Unknown')
except ObjectDoesNotExist:
    logger.error('Failed to get default brand  (did you provision it?)')
    sys.exit(1)

# get default brand product category
brand_product_category = None
try:
    brand_product_category = BrandProductCategory.objects.get(name='Unknown')
except ObjectDoesNotExist:
    logger.error('Failed to get default brand product category (did you provision it?)')
    sys.exit(1)


class T3ScrapperStats(object):
    def __init__(self):
        self.num_scrapped_categories = 0
        self.num_scrapped_brands = 0
        self.num_scrapped_products = 0
        self.num_scrapped_discounts = 0
        self.scrapped_brand_names = []

    def __str__(self):
        return 'num scrapped categories = %d, num_scrapped_brands = %d, num scrapped products = %d, ' \
               'num scrapped discounts = %d' % \
               (self.num_scrapped_categories, self.num_scrapped_brands,
                self.num_scrapped_products, self.num_scrapped_discounts)

    def __unicode__(self):
        return u'%s' % self.name

    __repr__ = __str__



class T3Product(object):
    def __init__(self, session, url, path, soup, brand):
        data_props = json.loads(soup.find('html')['data-props'])
        product = data_props['product']

        self.session = session
        self.url = url
        self.brand = brand
        self.id = product['id']
        self.taxonomy_id = product['primaryTaxonomyNodeId']
        self.name = product['title']
        self.ascii_name = CharMapper.cp1250_to_ascii(self.name).lower()
        self.image_full_url = product['defaultImageUrl']
        self.bc = self.image_full_url.split('/')[6]
        if self.bc == 'default':
            self.bc = ''
        self.bulk_buy_limit = product['bulkBuyLimit']
        self.is_for_sale = product['isForSale']
        self.average_weight = product['averageWeight']
        self.price = Decimal(str(product['price']))
        self.unit = ProductUnit.get_product_unit_by_name(product['unitOfMeasure'])
        self.amount_input_type = 'kg' if soup.find('select', {'aria-label': u'Hmotnosť'}) else 'pc'
        self.amount_input_base = 1

        self.unit_price = Decimal(str(product['unitPrice']))
        if len(data_props['promotions']) == 1:
            promotion = data_props['promotions'][0]
            self.promotion = dict(start_date=promotion['startDate'],
                                  end_date=promotion['endDate'],
                                  offer_text=promotion['offerText'])

            m = RE_PROMO_TEXT.search(self.promotion['offer_text'])
            if m:
                promo_text = m.groupdict()
                self.discount = int(promo_text['discount'].replace('-', '').replace('%', ''))
                self.price_before = Decimal(promo_text['price_before'].replace(',', '.'))
                self.discount_valid_from = date.today()
                self.discount_valid_until = parse(self.promotion['end_date']).date()
        else:
            self.promotion = None
            self.discount = None
            self.price_before = None
            self.discount_valid_from = None
            self.discount_valid_until = None

        self.description = data_props['brandBankAttributes']

        self.path = u'%s/%s' % (path, data_props['shelfName'])

        if SellerProductCategory.objects.filter(seller_id=seller.id, name=self.path).exists():
            self.seller_product_category = SellerProductCategory.objects.get(seller_id=seller.id, name=self.path)
        else:
            self.seller_product_category = SellerProductCategory(seller_id=seller.id,
                                                                 name=self.path,
                                                                 ascii_name=CharMapper.cp1250_to_ascii(self.name).lower())
            self.seller_product_category.save()


    @property
    def is_image_empty(self):
        return self.image_full_url.find('no-image') != -1

    @property
    def image_thumb_big_url(self):
        return self.image_full_url.replace('328x328', '135x135')


    def save(self):
        try:
            if SellerProduct.objects.filter(Q(seller_product_id=self.id) | Q(seller_product_code=self.id),
                                            seller_branch=seller_branch).exists():
                seller_product = SellerProduct.objects.get(Q(seller_product_id=self.id) | Q(seller_product_code=self.id),
                                                           seller_branch=seller_branch)
            elif SellerProduct.objects.filter(ean_pc=self.bc, name=self.name, seller_branch=seller_branch).exists():
                seller_product = SellerProduct.objects.get(ean_pc=self.bc, name=self.name, seller_branch=seller_branch)
            else:
                seller_product = SellerProduct.objects.get(brand_product=BrandProduct.objects.get(name=self.name,
                                                                                                  ean_pc=self.bc),
                                                           seller_id=seller.id)

            brand_product = seller_product.brand_product

            self.update_product_description(brand_product)
            self.update_product_brand(brand_product)
            self.update_product_images(brand_product)
            self.update_product_unit(brand_product)
            self.update_product_ean_pc(brand_product, seller_product)
            self.update_product_amount_input(brand_product)
            self.update_product_id(seller_product)
            self.update_product_url(seller_product)
            self.update_taxonomy_id(seller_product)
            self.update_seller_product_category_id(seller_product)
            self.update_common_product_category_id(brand_product, seller_product)
            self.update_product_name(brand_product, seller_product)
            self.update_product_price(seller_product)
        except ObjectDoesNotExist:
            try:
                common_product_category_mapping = CommonProductCategoryMapping.objects.get(
                    seller_product_category_id=self.seller_product_category.id)
            except ObjectDoesNotExist:
                common_product_category = CommonProductCategory.get_common_product_category_by_name(u'Ostatné')

                common_product_category_mapping = CommonProductCategoryMapping(
                    seller_product_category_id=self.seller_product_category.id,
                    common_product_category_id=common_product_category.id
                )
                common_product_category_mapping.save()

                missing_seller_product_categories.append(self.path)

            detailed_common_product_category = CommonProductCategory.get_common_product_category_by_id(
                common_product_category_mapping.common_product_category_id)
            base_common_product_category_name = detailed_common_product_category.name.split('/')[0]

            try:
                common_product_category = CommonProductCategory.get_common_product_category_by_name(
                    base_common_product_category_name)
            except CommonProductCategoryDoesNotExist:
                common_product_category = CommonProductCategory.get_common_product_category_by_name(u'Ostatné')

            # # first get or create brand product
            # if self.bc != '':
            #     brand_products = BrandProduct.objects.filter(Q(ean_pc=self.bc) | Q(ean_pc_int=self.bc))
            # else:
            #     brand_products = BrandProduct.objects.filter(name=self.name)

            brand_products = BrandProduct.objects.filter(name=self.name).filter(Q(ean_pc=self.bc) | Q(ean_pc_int=self.bc))

            brand_product = None
            if len(brand_products) > 0:
                brand_product = brand_products[0]

            if brand_product is None:
                brand_product = BrandProduct(brand=self.brand, brand_product_category=brand_product_category,
                                             common_product_category=common_product_category,
                                             detailed_common_product_category=detailed_common_product_category,
                                             ascii_name=self.ascii_name, ean_pc=self.bc, name=self.name,
                                             unit=self.unit,
                                             amount_input_type=self.amount_input_type,
                                             amount_input_base=self.amount_input_base,
                                             on_discount=self.promotion is not None)

                image = 'no-image.png'
                brand_product.image_full.save(image, ContentFile(''))
                brand_product.image_thumb_small.save(image, ContentFile(''))
                brand_product.image_thumb_big.save(image, ContentFile(''))

                if not self.is_image_empty:
                    self.update_product_images(brand_product)

            # than create seller product
            seller_product = SellerProduct(ascii_name=brand_product.ascii_name, ean_pc=self.bc,
                                           name=brand_product.name, brand_product=brand_product,
                                           seller_product_id=self.id, taxonomy_id=self.taxonomy_id,
                                           seller_product_category=self.seller_product_category,
                                           seller_id=seller.id,
                                           seller_branch=seller_branch, details=self.description)
            seller_product.save()

            # than update product price
            self.update_product_price(seller_product)

            logging.info('Inserted product "%s" (bc: %s) to the database.' % (self.name, self.bc))


    def update_product_description(self, brand_product):
        if self.description and len(self.description) > 0 and brand_product.description != self.description:
            brand_product.description = self.description
            brand_product.save(update_fields=['description'])

            logger.info('Updated brand product "%s" description.' % (brand_product.name))


    def update_product_brand(self, brand_product):
        """Updates product brand if differs."""

        if self.brand.name != 'Unknown' and brand_product.brand != self.brand:
            old_brand = brand_product.brand
            brand_product.brand = self.brand
            brand_product.save(update_fields=['brand'])

            logger.info('Updated brand product "%s" brand (was = "%s", now "%s").' % (brand_product.name,
                                                                                      old_brand.name,
                                                                                      brand_product.brand.name))


    def update_product_images(self, brand_product):
        """Updates product images if not empty and differ."""

        if not self.is_image_empty:
            image_full_link = ImageLink(img=dict(src=self.image_full_url))
            image_full_link.fetch(session=self.session)

            if brand_product.is_image_changed(image_full_link.file_name):
                # fetch thumb big
                image_thumb_big_link = ImageLink(img=dict(src=self.image_thumb_big_url),
                                            dst_path=os.path.join(DATA_ROOT, 'media', 'images', 'thumbs', 'big'))
                image_thumb_big_link.fetch(session=self.session)

                # resize to thumb_small
                image_thumb_small = os.path.join(DATA_ROOT, 'media', 'images', 'thumbs', 'small',
                                                 image_thumb_big_link.file_name)
                resize_image(image_thumb_big_link.image_file, THUMB_SMALL_SIZE, image_thumb_small)

                brand_product.image_full = product_get_full_image_path(brand_product,
                                                                       image_full_link.file_name)
                brand_product.image_thumb_small = product_get_small_thumb_image_path(brand_product,
                                                                                     image_thumb_big_link.file_name)
                brand_product.image_thumb_big = product_get_big_thumb_image_path(brand_product,
                                                    os.path.basename(image_thumb_small))
                brand_product.is_image_empty = False
                brand_product.save(update_fields=['image_full', 'image_thumb_small',
                                                  'image_thumb_big', 'is_image_empty'])

                logger.info('Updated brand product "%s" image to "%s".' % (brand_product.name,
                                                                           brand_product.image_full))
                brand_product.on_image_changed()


    def update_product_unit(self, brand_product):
        """Updates product unit if differs."""

        if self.unit != brand_product.unit:
            brand_product_unit_old = brand_product.unit
            brand_product.unit = self.unit
            brand_product.save(update_fields=['unit'])

            logger.info('Updated brand product "%s" unit (old: %s, new: %s).' % (brand_product.name,
                                                                                 brand_product_unit_old,
                                                                                 brand_product.unit))

    def update_product_ean_pc(self, brand_product, seller_product):
        """Updates ean pc if differs."""

        if len(self.bc) > 0 and not self.bc in (brand_product.ean_pc, brand_product.ean_pc_int):
            brand_product_ean_pc_old = brand_product.ean_pc
            brand_product.ean_pc = self.bc
            brand_product.save(update_fields=['ean_pc'])

            logger.info('Updated brand product "%s" EAN pc (old: %s, new: %s).' % (brand_product.name,
                                                                                   brand_product_ean_pc_old,
                                                                                   brand_product.ean_pc))

        if len(self.bc) > 0 and seller_product.ean_pc != self.bc:
            seller_product_ean_pc_old = seller_product.ean_pc
            seller_product.ean_pc = self.bc
            seller_product.save(update_fields=['ean_pc'])

            logger.info('Updated seller product "%s" EAN pc (old: %s, new: %s).' % (seller_product.name,
                                                                                    seller_product_ean_pc_old,
                                                                                    seller_product.ean_pc))

            if seller_product.brand_product.ean_pc != seller_product.ean_pc:
                seller_product.brand_product.on_ean_pc_changed(seller_product.ean_pc,
                                                               skip_seller_products=[seller_product])



    def update_product_amount_input(self, brand_product):
        """Populates amount_input_type and amount_input_base if None."""

        amount_input_type_old = brand_product.amount_input_type
        if amount_input_type_old is None:
            brand_product.amount_input_type = self.amount_input_type
            brand_product.amount_input_base = self.amount_input_base
            brand_product.save()

            logger.info('Populated brand product "%s" amount input type = %s base = %s' % (brand_product.name,
                         brand_product.amount_input_type, brand_product.amount_input_base))


    def update_product_id(self, seller_product):
        """Updates product id if differs."""

        if self.id != seller_product.seller_product_id:
            seller_product_id_old = seller_product.seller_product_id
            seller_product.seller_product_id = self.id
            seller_product.save(update_fields=['seller_product_id'])

            logger.info('Updated seller product "%s" id (old: %s, new: %s).' % (seller_product.name,
                                                                                 seller_product_id_old,
                                                                                 seller_product.seller_product_id))

    def update_product_url(self, seller_product):
        """Updates product url if differs."""

        if self.url != seller_product.url:
            seller_product_url_old = seller_product.url
            seller_product.url = self.url
            seller_product.save(update_fields=['url'])

            logger.info('Updated seller product "%s" url (old: %s, new: %s).' % (seller_product.name,
                                                                                 seller_product_url_old,
                                                                                 seller_product.url))


    def update_taxonomy_id(self, seller_product):
        """Populates taxonomy id if None."""

        if seller_product.taxonomy_id is None:
            seller_product.taxonomy_id = self.taxonomy_id
            seller_product.save(update_fields=['taxonomy_id'])

            logger.info('Populated seller product "%s" taxonomy_id (%s)' % (seller_product.name,
                                                                             seller_product.taxonomy_id))


    def update_seller_product_category_id(self, seller_product):
        """Updates seller product category if differs."""

        if seller_product.seller_product_category_id != self.seller_product_category.id:
            seller_product_category_old = seller_product.seller_product_category
            seller_product.seller_product_category_id = self.seller_product_category.id
            seller_product.save(update_fields=['seller_product_category_id'])

            logger.info('Updated seller product "%s" seller product category (old: %s, new: %s).' %
                         (seller_product.name, seller_product_category_old.name, self.seller_product_category.name))


    def update_common_product_category_id(self, brand_product, seller_product):
        """Populates brand product common product category if None."""

        try:
            common_product_category_mapping = CommonProductCategoryMapping.objects.get(
                seller_product_category_id=self.seller_product_category.id)
        except ObjectDoesNotExist:
            # handle seller product category change by mapping the new seller product category to the existing
            # detailed common product category to avoid need to modify the mapping manually
            common_product_category_mapping = CommonProductCategoryMapping(
                seller_product_category_id=self.seller_product_category.id,
                common_product_category_id=brand_product.detailed_common_product_category_id
            )
            common_product_category_mapping.save()

        base_common_product_category_name = common_product_category_mapping.common_product_category.name.split('/')[0]
        base_common_product_category = CommonProductCategory.get_common_product_category_by_name(
            base_common_product_category_name)
        detailed_common_product_category = CommonProductCategory.get_common_product_category_by_id(
            common_product_category_mapping.common_product_category_id)

        if brand_product.common_product_category_id is None or brand_product.common_product_category_id != base_common_product_category.id:
            old_brand_product_common_product_category = brand_product.common_product_category
            brand_product.common_product_category = base_common_product_category
            brand_product.save(update_fields=['common_product_category'])

            logger.info('Updated brand product "%s" (id=%d) common product category (was "%s", now "%s")' %
                         (brand_product.name, brand_product.id, old_brand_product_common_product_category.name,
                          brand_product.common_product_category.name))

        if brand_product.detailed_common_product_category_id is None or \
                        brand_product.detailed_common_product_category_id != detailed_common_product_category.id:
            old_brand_product_detailed_common_product_category = brand_product.detailed_common_product_category
            brand_product.detailed_common_product_category = detailed_common_product_category
            brand_product.save(update_fields=['detailed_common_product_category'])

            logger.info('Update brand product "%s" detailed common product category (was "%s", now "%s")' %
                         (brand_product.name, old_brand_product_detailed_common_product_category.name,
                          brand_product.detailed_common_product_category.name))


    def update_product_name(self, brand_product, seller_product):
        """Updates product name if differs."""

        if self.name != brand_product.name:
            brand_product_name_old = brand_product.name
            brand_product.ascii_name = self.ascii_name
            brand_product.name = self.name
            brand_product.save(update_fields=['ascii_name', 'name'])

            seller_product.ascii_name = self.ascii_name
            seller_product.name = self.name
            seller_product.save(update_fields=['ascii_name', 'name'])

            logger.info('Updated brand and seller product names (old: "%s", new: "%s").' % (brand_product_name_old,
                                                                                            brand_product.name))


    def update_product_price(self, seller_product):
        """Update product price if differs."""

        price_pc_with_vat_old = None
        price_pc_with_vat = None
        price_pkg_with_vat_old = None
        price_pkg_with_vat = None
        price_100g_with_vat_old = None
        price_100g_with_vat = None
        price_1kg_with_vat_old = None
        price_1kg_with_vat = None
        price_unit_with_vat_old = None

        price_changed = False

        if self.unit.is_kg:
            if self.amount_input_type == 'dkg':
                if self.average_weight == 0.1:
                    price_changed = seller_product.price_100g_with_vat.amount != self.price
                    price_100g_with_vat_old = seller_product.price_100g_with_vat
                    price_100g_with_vat = Money(self.price, EUR)
                else:
                    price_changed = seller_product.price_1kg_with_vat.amount != self.unit_price

                # weight based product should not have price_pkg_with_vat populated
                if not price_changed and seller_product.price_pkg_with_vat is not None:
                    price_pkg_with_vat = Money(0, EUR)
                    price_pkg_with_vat_old = Money(0, EUR)

                    price_changed = True

                if self.price_before:
                    price_1kg_with_vat_old = Money(self.price_before, EUR)
                else:
                    price_1kg_with_vat_old = seller_product.price_1kg_with_vat

            else:
                price_changed = seller_product.price_pkg_with_vat.amount != self.price
                if self.price_before:
                    price_pkg_with_vat_old = Money(self.price_before, EUR)
                else:
                    price_pkg_with_vat_old = seller_product.price_pkg_with_vat
                price_pkg_with_vat = Money(self.price, EUR)

            price_1kg_with_vat = Money(self.unit_price, EUR)

        elif self.unit.is_litre:
            price_changed = seller_product.price_pkg_with_vat.amount != self.price
            if self.price_before:
                price_pkg_with_vat_old = Money(self.price_before, EUR)
            else:
                price_pkg_with_vat_old = seller_product.price_pkg_with_vat
            price_pkg_with_vat = Money(self.price, EUR)
        elif self.unit.is_piece:
            price_changed = seller_product.price_pc_with_vat.amount != self.price
            if self.price_before:
                price_pc_with_vat_old = Money(self.price_before, EUR)
            else:
                price_pc_with_vat_old = seller_product.price_pc_with_vat
            price_pc_with_vat = Money(self.price, EUR)
        elif self.unit.is_dose:
            price_changed = seller_product.price_pkg_with_vat.amount != self.price
            if self.price_before:
                price_pkg_with_vat_old = Money(self.price_before, EUR)
            else:
                price_pkg_with_vat_old = seller_product.price_pkg_with_vat
            price_pkg_with_vat = Money(self.price, EUR)

        price_unit_with_vat = Money(self.unit_price, EUR)

        # ensure current price 100 g is correct if not set it to 0 and let seller product price to calculate it
        if price_1kg_with_vat and price_100g_with_vat is None and seller_product.price_100g_with_vat and \
            seller_product.price_100g_with_vat != PriceTag.get_price_per_100g(price_1kg_with_vat):
            price_100g_with_vat = Money(0, EUR)
            price_100g_with_vat_old = Money(0, EUR)

            price_changed = True

        if price_changed or self.discount_valid_until is not None \
                and seller_product.discount_valid_until != self.discount_valid_until:
            price_pc_with_vat_before = seller_product.price_pc_with_vat
            price_pkg_with_vat_before = seller_product.price_pkg_with_vat
            price_100g_with_vat_before = seller_product.price_100g_with_vat
            price_1kg_with_vat_before = seller_product.price_1kg_with_vat

            seller_product.update(discount_valid_from=self.discount_valid_from,
                                  discount_valid_until=self.discount_valid_until,
                                  price_pc_with_vat_old=price_pc_with_vat_old,
                                  price_pc_with_vat=price_pc_with_vat,
                                  price_pkg_with_vat_old=price_pkg_with_vat_old,
                                  price_pkg_with_vat=price_pkg_with_vat,
                                  price_100g_with_vat_old=price_100g_with_vat_old,
                                  price_100g_with_vat=price_100g_with_vat,
                                  price_1kg_with_vat_old=price_1kg_with_vat_old,
                                  price_1kg_with_vat=price_1kg_with_vat,
                                  price_unit_with_vat_old=price_unit_with_vat_old,
                                  price_unit_with_vat=price_unit_with_vat,
                                  discount=self.discount)

            try:
                logger.info('Updated seller product "%s" seller product price was: price_pc_with_vat = %s, '
                            'price_pkg_with_vat = %s, price_100g_with_vat = %s, price_1kg_with_vat = %s, '
                            'now: price_pc_with_vat = %s, price_pkg_with_vat = %s, price_100g_with_vat = %s, '
                            'price_1kg_with_vat = %s' % (seller_product.name, price_pc_with_vat_before,
                                                         price_pkg_with_vat_before, price_100g_with_vat_before,
                                                         price_1kg_with_vat_before, seller_product.price_pc_with_vat,
                                                         seller_product.price_pkg_with_vat,
                                                         seller_product.price_100g_with_vat,
                                                         seller_product.price_1kg_with_vat))
            except AttributeError, e:
                pass



    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return u'%s' % self.name

    __repr__ = __str__



class T3ProductCategory(object):
    def __init__(self, session, url, soup):
        self.session = session
        self.url = url
        data_props = json.loads(soup.find('html')['data-props'])
        for face_list in data_props['resources']['productsByCategory']['data']['results']['facetLists']:
            if face_list['category'] == 'Brand':
                self.brand_props = face_list
                break
        else:
            self.brand_props = None
        try:
            self.path = '/'.join(map(lambda b: b['label'], data_props['resources']['productsByCategory']['data']['breadcrumbs'])).strip()
        except KeyError, e:
            logger.warning('Failed to create T3ProductCategory from url %s, reason: %s', e)
            raise e
        self.scrapped_product_ids = []


    @staticmethod
    def get_product_items(soup):
        """Returns product items from soup."""

        product_items = []

        for product_tile in soup.findAll('a', {'class': 'product-tile--title'}):
            product_item = dict(product=dict(id=RE_PRODUCT_ID.search(product_tile['href']).group(1),
                                             title=product_tile.text))
            product_items.append(product_item)

        return product_items


    @staticmethod
    def get_num_pages(soup):
        """Returns num pages in soup."""

        pages = []
        for page_button in soup.findAll('a', {'class': 'pagination--button'}):
            if page_button.attrs.has_key('href'):
                page = int(RE_PAGE.search(page_button['href']).group(1))
                if page not in pages:
                    pages.append(page)

        return max(pages)


    def scrap(self):
        # handle case there are no facets
        if self.brand_props:
            for facet in self.brand_props['facets']:
                self.scrap_brand(facet['facetId'], facet['facetName'], page=1)

                if facet['facetName'] not in t3_scrapper_stats.scrapped_brand_names:
                    t3_scrapper_stats.scrapped_brand_names.append(facet['facetName'])
                    t3_scrapper_stats.num_scrapped_brands += 1

            # as not all branded products are grouped to brands scrap the rest as non-branded
            self.scrap_nobrand(page=1)
        else:
            self.scrap_nobrand(page=1)


    def scrap_brand(self, brand_id, brand_name, page=1):
        logger.info('Scrapping category "%s", brand "%s", page %d' % (self.path, brand_name, page))

        url = '%s?brand=%d&viewAll=brand&page=%d' % (self.url, brand_id, page)
        response = self.session.get(url)
        soup = BeautifulSoup(response.content)

        data_props = json.loads(soup.find('html')['data-props'])
        if data_props.has_key('resources'):
            page_info = data_props['resources']['productsByCategory']['data']['results']['pageInformation']

            self.scrap_product_items(Brand.get_or_create_brand(brand_name),
                                     data_props['resources']['productsByCategory']['data']['results']['productItems'])

            if page_info['pageCount'] > page:
                self.scrap_brand(brand_id, brand_name, page + 1)
        else:
            logger.warning('Did not find resources key in data-props, url = %s' % url)


    def scrap_nobrand(self, page=1):
        logger.info('Scrapping category "%s", page %d' % (self.path, page))

        response = self.session.get('%s?page=%d' % (self.url, page))
        soup = BeautifulSoup(response.content)

        data_props = json.loads(soup.find('html')['data-props'])
        page_info = data_props['resources']['productsByCategory']['data']['results']['pageInformation']

        self.scrap_product_items(Brand.get_default_brand(),
                                 data_props['resources']['productsByCategory']['data']['results']['productItems'])

        if page_info['pageCount'] > page:
            self.scrap_nobrand(page + 1)


    def scrap_product_items(self, brand, product_items):
        for product_item in product_items:
            if product_item['product']['id'] in self.scrapped_product_ids:
                continue

            num_tries = 5
            while num_tries > 0:
                try:
                    product_response = self.session.get('%s/groceries/sk-SK/products/%s' % (T3Spider.base_url,
                                                                                            product_item['product']['id']))
                    product_soup = BeautifulSoup(product_response.content)

                    if brand.name == 'Unknown':
                        a_brand = find_brand(product_item['product']['title'])
                        if a_brand:
                            brand = a_brand

                    t3_product = T3Product(self.session, product_response.url, self.path, product_soup, brand)
                    t3_product.save()

                    self.scrapped_product_ids.append(t3_product.id)

                    t3_scrapper_stats.num_scrapped_products += 1

                    if t3_product.promotion:
                        t3_scrapper_stats.num_scrapped_discounts += 1

                    break

                except Exception, e:
                    num_tries -= 1

                    if num_tries == 0:
                        logger.exception('Failed to scrap product item %s, reason: %s' % (product_item, e))
                    else:
                        sleep(5)



    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return u'%s' % self.path

    __repr__ = __str__


session = requests.Session()
session.headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko)' \
                                ' Chrome/48.0.2564.82 Safari/537.36'

categories_dump_file = open(args.categories_dump)
ln_num = 1

t3_scrapper_stats = T3ScrapperStats()

for category_ln in categories_dump_file.readlines():
    try:
        category = json.loads(category_ln)

        if category['url'].find('promotion') != -1 or category['url'].find('?') != -1 or category['url'].find('%') != -1:
            continue

        logger.info('processing category %s' % category['url'])

        response = session.get(category['url'])
        soup = BeautifulSoup(response.content)

        t3_product_category = T3ProductCategory(session, response.url, soup)
        t3_product_category.scrap()

        t3_scrapper_stats.num_scrapped_categories += 1

        logger.info(t3_scrapper_stats)

        ln_num += 1

    except Exception, e:
        logging.exception('Failed process line %i, reason: %s' % (ln_num, e))
        continue

if len(missing_seller_product_categories) > 0:
    subject = '%d %s category mappings must be modified, currently mapped to "Other".' % (len(missing_seller_product_categories),
                                                                                          seller.name)
    send_mail(subject, '%s' % '\n'.join(missing_seller_product_categories), DEFAULT_FROM_EMAIL, [CEO_EMAIL])