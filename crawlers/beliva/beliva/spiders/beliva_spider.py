# -*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

from shopper.crawlers.beliva.beliva.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

class BelivaSpider(CrawlSpider):
    name = 'beliva'
    allowed_domains = ['www.beliva.sk']
    start_urls = ['http://www.beliva.sk/ponuka/']

    rules = (Rule(SgmlLinkExtractor(allow=['/produkt/.*']), 'parse_product_item', follow=True),
            Rule(SgmlLinkExtractor(allow=['\d+-.*']), follow=True),
             )


    product_categories = {}
    product_category_mapping = {}


    def parse_product_item(self, response):
        hxs = HtmlXPathSelector(response)

        product_item = ProductItem()
        product_item['name'] = extract_data_item(hxs.select('//div[@class="detail"]/h1/text()'))
        product_item['link'] = response.url
        product_item['image_urls'] = extract_data_list(hxs.select('//img[@id="zoom_01"]/@src'), prefix='http://www.beliva.sk')

        detail_labels = ' '.join(extract_data_list(hxs.select('//div[@class="detail"]/ul/li/text()')))
        has_brand = False
        if detail_labels.find(u'Značka') != -1:
            has_brand = True
        has_bc = False
        if detail_labels.find(u'EAN') != -1:
            has_bc = True

        details = extract_data_list(hxs.select('//div[@class="detail"]/ul/li/span/text()'))
        if has_brand:
            product_item['brand'] = details[0]
            if has_bc:
                product_item['bc'] = details[1]
        elif has_bc:
            product_item['bc'] = details[0]

        prices = extract_data_list(hxs.select('//div[@class="price"]/h2/text()'))
        product_item['price'] = prices[0]
        if len(prices) > 1:
            product_item['price_unit'] = prices[1]
        else:
            product_item['price_unit'] = None

        product_item['details'] = ' '.join(extract_data_list(hxs.select('//div[@class="full"]/p/text()')))

        product_item['path'] = '/'.join(extract_data_list(hxs.select('//nav/a/text()')))

        return product_item