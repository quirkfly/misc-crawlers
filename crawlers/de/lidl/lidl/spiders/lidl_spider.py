# -*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector

from shopper.crawlers.lidl.lidl.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list


class BelivaSpider(CrawlSpider):
    name = 'lidl'
    allowed_domains = ['www.lidl.de']
    start_urls = ['http://www.lidl.de/de']

    rules = (Rule(SgmlLinkExtractor(allow=['/.*/p\d+']), 'parse_product_item', follow=True),
            Rule(SgmlLinkExtractor(allow=['.*']), follow=True),
             )

    def parse_product_item(self, response):
        hxs = HtmlXPathSelector(response)

        product_path = '/'.join(extract_data_list(hxs.select('//div[@class="c-10"]/ul/li/a/text()')))
        # focus only on food, chemistry or weekly offers for now
        if product_path.find('Angebote im Lidl-Shop') == -1 and product_path.find('Lebensmittel & Drogerie') == -1:
            return None

        product_item = ProductItem()
        product_item['path'] = product_path
        product_item['name'] = extract_data_item(hxs.select('//div[@class="c-4"]')[0].select('//h1/text()'))
        product_item['link'] = response.url
        product_item['image_urls'] = extract_data_list(hxs.select('//div[@data-image-index="0"]/a/img/@src'), prefix='http://www.lidl.de')
        product_item['amount'] = extract_data_item(hxs.select('//small[@class="amount"]/text()'))

        return product_item