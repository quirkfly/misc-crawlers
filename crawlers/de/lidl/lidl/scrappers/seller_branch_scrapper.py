# -*- coding: utf-8 -*-

__author__ = 'peterd'

import requests
import json
import logging
import re

from shopper.models import SellerBranch, Seller

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

MAX_NUM_LOCATIONS = 5000

LIDL = Seller.get_seller_by_id(8)


def purify_location_result(location_result):
    locality = location_result['Locality']
    locality = locality.replace('?', u'Š')
    location_result['Locality'] = locality

    address_line = location_result['AddressLine']
    address_line = address_line.replace('?', u'Š')
    address_line = address_line.replace(u'Okružná č.1 4783', u'Okružná 1')
    address_line = address_line.replace(u'Kolpašská I.1902/ 1/F', u'Kolpašská 1A')
    location_result['AddressLine'] = address_line
    return location_result


session = requests.Session()
session.headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                   'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:30.0) Gecko/20100101 Firefox/30.0',
                   'Accept-Encoding': 'gzip, deflate, sdch',
                   'Accept-Language': 'en-US,en;q=0.8,de;q=0.6,sk;q=0.4'}


resp = session.get('https://spatial.virtualearth.net/REST/v1/data/018f9e1466694a03b6190cb8ccc19272/Filialdaten-SK/Filialdaten-SK?spatialFilter=nearby(48.169620513916016,17.1295108795166,400)&$filter=Adresstyp%20Eq%201&$select=EntityID,Latitude,Longitude,AddressLine,PostalCode,Locality,Bilanzgesellschaft,OpeningTimes,INFOICON1,INFOICON2,INFOICON3,INFOICON4,INFOICON5,AR,NF,Adresstyp,OverlayOperatorData,ShownPostalCode,ShownLocality,ShownAddressLine,ShownStoreName&$top=' + str(MAX_NUM_LOCATIONS) + '&$format=json&$skip=0&key=AqN50YiXhDtZtWqXZcb7nWvF-4Xc9rg9IXd6YWepqk4WnlmbvD-NV3KHA3A0dOtw&Jsonp=displayResultStores')


if resp.reason == 'OK':
    m = re.match(r'^displayResultStores\((.*?)\)$', resp.content)
    if m:
        for location_result in json.loads(m.group(1))['d']['results']:
            location_result['State'] = 'Slovensko'
            location_result = purify_location_result(location_result)
            seller_branch, created = SellerBranch.get_or_create_from_virtualearth_location(LIDL, location_result)
            if created:
                logging.info('Created seller branch: "%s"' % seller_branch)
            else:
                logging.info('Seller branch "%s" already exists' % seller_branch)
    else:
        logging.error('Failed to fetch LIDL branch locations, reason: missing displayResultStores context.')
else:
    logging.error('Failed to fetch LIDL branch locations, reason:' % resp.reason)



