# -*- coding: utf-8 -*-

import argparse
import logging
import os
import re
from moneyed import EUR, Money

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import django
import simplejson
import time

from shopper.crawlers.discount import ImageLink, ImageDoesNotExist
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.crawlers.kaufland.sk.offers.discount import KauflandDiscount
from shopper.models.product import Seller, SellerProductDiscountTrash, SellerProductDiscount, SellerProduct, \
    CommonProductCategory

django.setup()

__author__ = 'peterd'

parser = argparse.ArgumentParser(description='kaufland offers scrapper.')
parser.add_argument('--store-id', dest='store_id', type=int, default=2220,
                    help='Store id to fetch offers for. If not specified 2220 (Trnavska cesta, Ba) will be used.')
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--dont-send-mail', dest="dont_send_email", action='store_true',
                    help='Do not send a scrapping email report. The report is sent by default.')
parser.add_argument('--logfile', '-f', help='File where the logging will be written. If not specified logging will go to console.')
args = parser.parse_args()

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

logger = logging.getLogger(__name__)

OTHER_HOUSEHOLD_CATEGORIES = [u'Elektro, kancelária, médiá', u'Dom, domácnosť',u'Oblečenie, auto, voľný čas, hry']
base_other_household_category = CommonProductCategory.objects.get(name=u'Všetko pre domov')
detailed_other_household_category = CommonProductCategory.objects.get(name=u'Všetko pre domov/Ostatné')

class KauflandOffersScrapper(DiscountScrapper):
    BASE_URL = 'https://app.kaufland.net'
    USER_ID = 'KIS-KLAPP'
    PASSWORD = 'xxxxxxxxxxx'

    RE_UNIT = re.compile(r'(?P<unit>g|kg|ml|l|m|kus(y|ov)*)')

    def __init__(self, logger, seller, store_id):
        super(KauflandOffersScrapper, self).__init__(seller, logger, sleep_int=0)

        self.store_id = store_id


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        session = self.session
        session.headers = {'Accept': 'application/json', 'Accept-Encoding': 'gzip', 'User-Agent': 'okhttp/2.6.0'}

        response = session.get('%s/data/api/v2/offers/SK%d/all' % (KauflandOffersScrapper.BASE_URL, self.store_id),
                                    auth=(KauflandOffersScrapper.USER_ID, KauflandOffersScrapper.PASSWORD))
        for category_offers in simplejson.loads(response.content):

            for offer in category_offers['offers']:
                try:
                    name = offer['title'] if offer['title'] is not None else offer['detailTitle']

                    if name is None:
                        logger.warning('skipping offer: %s as missing title.' % offer)
                        continue

                    # if name != u'Bravčová krkovička':
                    #     continue

                    unit = None
                    if offer['detailTitle'] is not None:
                        for word in offer['detailTitle'].split(' '):
                            if name.find(word) == -1:
                               name = '%s %s' % (name, word)
                    if offer['unit'] is not None:
                        name = '%s %s' % (name, offer['unit'])
                        m = KauflandOffersScrapper.RE_UNIT.search(offer['unit'])
                        if m:
                            unit = m.groupdict()['unit']

                    name = name.replace('\r\n', '')

                    abs_product_link = '%s/%s/%s' % (KauflandOffersScrapper.BASE_URL,
                                                     offer['offerId'],
                                                     os.path.basename(offer['listImage']))

                    description = None
                    if offer['subtitle'] is not None:
                        description = offer['subtitle']
                    if offer['detailDescription'] is not None:
                        description = '%s %s' % (description, offer['detailDescription'])

                    old_price = float(offer['oldPrice']) if offer.has_key('oldPrice') else None
                    new_price = float(offer['price'])
                    if old_price == new_price:
                        old_price = 0
                    discount = offer['discount'] if offer.has_key('discount') else None
                    if old_price is None and discount is not None:
                        old_price = round(new_price / float((100 - discount) * 0.01), 2)

                    discount = KauflandDiscount(abs_product_link, None, name,
                                                offer['dateFrom'], offer['dateTo'],
                                                self.seller, description,
                                                new_price, old_price, date_format='%Y-%m-%d',
                                                unit=unit)

                    # skip the product if trashed
                    if SellerProductDiscountTrash.discount_exists(discount):
                        logger.info('skipping "%s" as has been trashed' % discount.name)
                        continue

                    # skip the product if scrapped already
                    if SellerProductDiscount.discount_exists(discount, match_by_url=True) and \
                        category_offers['displayName'] not in OTHER_HOUSEHOLD_CATEGORIES:
                        logger.info('skipping "%s" as already scrapped' % discount.name)
                        continue

                    seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                            discount.name,
                                                            discount.url,
                                                            self.seller.id)
                    if seller_product_discount and seller_product_discount.seller_product_id is not None:
                        seller_product_discount.on_discount_valid(discount)

                        seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                        seller_product.price_100g_with_vat_old = Money(0.0, EUR)
                        seller_product.price_1kg_with_vat_old = Money(0.0, EUR)
                        seller_product.on_discount_valid(seller_product_discount)
                    else:
                        image_link = ImageLink(dict(src=offer['listImage']))

                        num_tries = 3
                        i = 0
                        while num_tries > 0:
                            try:
                                image_link.fetch(session)
                                break
                            except ImageDoesNotExist:
                                break
                            except RuntimeError, e:
                                num_tries -= 1
                                i += 1
                                logger.error('failed to fetch product image, reason: %s, num_tries left: %d' % (e,
                                                                                                                num_tries))
                                if num_tries > 0:
                                    time.sleep(5 * i)

                        discount.image_file = image_link.rel_image_path

                        if category_offers['displayName'] in OTHER_HOUSEHOLD_CATEGORIES:
                            discount.save_to_common_product_category(
                                                base_common_product_category=base_other_household_category,
                                                detailed_common_product_category=detailed_other_household_category,
                                                logger=self.logger)
                        else:
                            seller_product = discount.save(logger=self.logger)

                            if seller_product is None:
                                self.add(discount)
                            else:
                                if seller_product.must_recategorize:
                                    self.on_must_recategorize(seller_product)

                    self.logger.debug(u'scrapped discount: %s' % discount)
                except Exception, e:
                    logger.exception('failed to process offer %s, reason: %s' % (offer, e))

        return self.discount_map

try:
    kaufland_scrapper = KauflandOffersScrapper(logger, Seller.get_seller_by_name('kaufland'), args.store_id)
    kaufland_scrapper.scrap()
    if not args.dont_send_email:
        kaufland_scrapper.send_mail()
except Exception, e:
    logger.exception('Failed to scrap kaufland offers, reason: %s' % e)

