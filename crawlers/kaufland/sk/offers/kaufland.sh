#!/bin/bash -u

# main script that orchestrates the scrapping process from kaufland
#

LOG_FILE=
TEASER_ONLY=0
DONT_SEND_MAIL=0
VERBOSITY=0
OPTIONS=

function usage {
	echo "kaufland scrapper wrapper"
	echo "`basename $0` [-v] [-l <log file>] <store id>"
    echo "v - be verbose about actions performed"
	echo "<log file> - if not specify log to console"
	echo "t - scrap only first page"
	echo "store id - id of the store to scrap offers for"
	exit 1
}

while getopts l:stv FLAG; do
	case $FLAG in
    l)
		LOG_FILE=$OPTARG;
     	;;
    s)
		DONT_SEND_MAIL=1;
      	;;
	v)
		VERBOSITY=1;
		;;
    ?)
     	usage;
      	;;
  esac
done

shift $(( OPTIND - 1 ));

test ! $# -eq 1 && usage

cd $ITRASH_HOME_DIR

if [ $VERBOSITY -eq 1 ]
then
	OPTIONS="--verbosity"
fi

if [ ! -z "$LOG_FILE" ]
then
	OPTIONS=" $OPTIONS --logfile $LOG_FILE"
fi

if [ $DONT_SEND_MAIL -eq 1 ]
then
	OPTIONS=" $OPTIONS --dont-send-mail"
fi

STORE_ID=$1

PYTHONPATH=`pwd`:.. DJANGO_SETTINGS_MODULE=itrash.settings python shopper/crawlers/kaufland/sk/offers/discount_scrapper.py $OPTIONS --store-id $STORE_ID

if [ ! -z "$LOG_FILE" ]
then
    #
    # check scrapper log for errors
    #

    NUM_WRITE_ERRORS=`grep -i error $LOG_FILE | wc -l`
    if [ $NUM_WRITE_ERRORS -gt 0 ]
    then
        grep -i -A100 error $LOG_FILE > /tmp/discount_scrapper.err
        python2.7 utils/mailer.py --from_addr discount.scrapper@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to scrap $NUM_WRITE_ERRORS store-$STORE_ID discounts." /tmp/discount_scrapper.err
        rm /tmp/discount_scrapper.err
    fi
fi