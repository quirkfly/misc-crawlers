import argparse
import logging
import re
from decimal import Decimal
import sys

import django
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from moneyed import EUR, Money
from django.utils.translation import ugettext as _

django.setup()

from infra.lexical import CharMapper
from shopper.crawlers.discount import ImageLink
from shopper.models.product import SellerProduct, Seller, SellerBranch, Brand, BrandProductCategory, ProductUnit, \
    SellerProductCategory, SellerToCommonCategoryMap, BrandProduct, SellerProductPrice

__author__ = 'peterd'

import requests
from bs4 import BeautifulSoup

parser = argparse.ArgumentParser(description='kaufland exquisit product scrapper.')
parser.add_argument('--logfile', '-f', help='File where the logging will be written. If not specified logging will go to console.')
args = parser.parse_args()

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

logger = logging.getLogger(__name__)

BRAND = 'Exquisit'
BASE_URL = 'http://www.kaufland.sk'
PRODUCT_CATEGORY_LINK = '/Home/02_Sortiment/010_Exquisit/02_Sortiment/%s/index.jsp'
RE_PRODUCT_CATEGORY_LINK = re.compile(r'/Home/02_Sortiment/010_Exquisit/02_Sortiment/(\w+)/index.jsp')
RE_PRICE_PER_L = re.compile(r'.*?=\s+1\s+l\s+([0-9,.]+)')
RE_PRICE_PER_KG = re.compile(r'.*?=\s+1\s+kg\s+([0-9,.]+)')

# get seller instance
seller = None
try:
    seller = Seller.objects.get(name='Kaufland')
except ObjectDoesNotExist:
    logging.error('Failed to get seller object (did you provision it?)')
    sys.exit(1)

# get seller branch
seller_branch = SellerBranch.get_default_seller_branch(seller.id)
seller_branch_currency = seller_branch.location.state.currency

# get default brand
brand = None
try:
    brand = Brand.objects.get(name=BRAND)
except ObjectDoesNotExist:
    logging.error('Failed to get Exquisit brand  (did you provision it?)')
    sys.exit(1)

# get default brand product category
brand_product_category = None
try:
    brand_product_category = BrandProductCategory.objects.get(name='Unknown')
except ObjectDoesNotExist:
    logging.error('Failed to get default brand product category (did you provision it?)')
    sys.exit(1)

product_unit = ProductUnit.objects.get(unit_sk='kg')

num_new_products = 0

product_categories = []

def parse_product_page(product_page_url):
    num_new_products = 0

    response = requests.get(product_page_url)
    product_preview = BeautifulSoup(response.content)

    num_products = len(product_preview.findAll('div', {'id': 'posItemDescription'}))

    for i in range(num_products):
        product_details_link = product_preview.findAll('div', {'id': 'productpicture'})[i].find('a')['href']

        response = requests.get(product_details_link)
        product_details_soup = BeautifulSoup(response.content)

        product_image_link = ImageLink(product_details_soup.find('div', {'id': 'klein'}).find('a').find('img'))
        product_image_link.fetch(requests.Session())
        logger.debug(product_image_link)

        product_name = product_details_soup.find('div', {'id': 'product_name'}).text.replace('\t', '').replace('\r', '').replace('\n', '').strip().replace(BRAND, BRAND + ' ')
        logger.debug(product_name)

        price_1kg_with_vat = None
        price_unit_with_vat = None
        product_unit = ProductUnit.objects.get(unit_sk='kg')

        product_desc = product_details_soup.find('div', {'id': 'product_txt'}).text.replace('\t', '').replace('\r', '').replace('\n', '').strip().replace(BRAND, BRAND + ' ')
        m = re.search(RE_PRICE_PER_KG, product_desc)
        if m:
            price_1kg_with_vat = Money(Decimal(m.group(1).replace(',', '.')), EUR)
            price_unit_with_vat = price_1kg_with_vat
        else:
            m = re.search(RE_PRICE_PER_L, product_desc)
            if m:
                product_unit = ProductUnit.objects.get(unit_sk='l')
                price_unit_with_vat = Money(Decimal(m.group(1).replace(',', '.')), EUR)

        logger.debug(product_desc)

        product_amount = product_details_soup.find('div', {'class': 'MengenangabeDetail'}).text
        logger.debug(product_amount)

        product_price = Money(Decimal(product_details_soup.find('div', {'class': 'VerkaufspreisDetail'}).text.replace(',', '.')), EUR)
        logger.debug(product_price)

        product_name = u'%s %s' % (product_name, product_amount)
        product_ascii_name = CharMapper.cp1250_to_ascii(product_name).lower()

        try:
            seller_product = SellerProduct.objects.get(name=product_name, seller_branch=seller_branch)

            seller_product.brand_product.update_image(product_image_link.image_file, product_image_link.file_name)
            seller_product.brand_product.save()

            seller_product.update(price_pkg_with_vat=product_price,
                                  price_1kg_with_vat=price_1kg_with_vat,
                                  price_unit_with_vat=price_unit_with_vat)

            logging.info('Updated product "%s"' % seller_product.name)

        except ObjectDoesNotExist:
            a_seller_product_category = SellerProductCategory.objects.filter(seller_id=seller.id)[0]
            a_common_product_category_id = SellerToCommonCategoryMap.get_common_product_category_id(
                a_seller_product_category.id)

            brand_product = BrandProduct(brand=brand, brand_product_category=brand_product_category,
                                         common_product_category_id=a_common_product_category_id,
                                         ascii_name=product_ascii_name, name=product_name,
                                         unit_id=product_unit.id,
                                         amount_input_type='pc', amount_input_base=1,
                                         is_clonnable=False)
            brand_product.update_image(product_image_link.image_file, product_image_link.file_name)
            brand_product.save()

            # than create seller product
            seller_product = SellerProduct(ascii_name=brand_product.ascii_name,
                                           name=brand_product.name, brand_product=brand_product,
                                           seller_product_category_id=a_seller_product_category.id,
                                           common_product_category_id=a_common_product_category_id,
                                           seller_id=seller.id,
                                           seller_branch=seller_branch,
                                           verified=False)
            seller_product.save()

            # create seller product price
            SellerProductPrice.get_or_create_seller_product_price(
                                           seller_product,
                                           seller_branch.id,
                                           price_pkg_with_vat=product_price,
                                           price_1kg_with_vat=price_1kg_with_vat,
                                           price_unit_with_vat=price_unit_with_vat)

            logging.info('Created product "%s"' % seller_product.name)

            num_new_products += 1

    scroll_right_link = product_preview.find('div', {'id': 'scrollBoxright'}).find('a', {'class': 'buttonright'})
    if scroll_right_link:
        num_new_products += parse_product_page(scroll_right_link['href'])

    return num_new_products

try:
    response = requests.get('%s/Home/02_Sortiment/010_Exquisit/02_Sortiment/index.jsp' % BASE_URL)
    soup = BeautifulSoup(response.content)

    for link in soup.findAll('a'):
        if link.has_attr('href'):
            m = re.search(RE_PRODUCT_CATEGORY_LINK, link['href'])
            if m:
                product_category = m.group(1)
                if product_category not in product_categories:

                    logger.debug('scrapping "%s"' % product_category)

                    product_categories.append(product_category)

                    product_category_link = PRODUCT_CATEGORY_LINK % product_category

                    num_new_products += parse_product_page('%s/%s' % (BASE_URL, product_category_link))

    if num_new_products > 0:
        recipient_list = [CEO_EMAIl]

        subject = _('%(num_new_products)d Kaufland Exquisit products needs to be recategorized') % ({'num_new_products':
                                                                                                    num_new_products})
        send_mail(subject, None, 'product.scrapper@20deka.com', recipient_list)
except Exception, e:
    logger.exception('Failed to scrap Kaufland Exquisit products, reason: %s' % e)