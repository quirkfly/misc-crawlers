# -*- coding: utf-8 -*-
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector

from shopper.crawlers.kaufland.sk.exquisit import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list


class KauflandSpider(CrawlSpider):
    name = 'kaufland'
    allowed_domains = ['www.kaufland.sk']
    #start_urls = ['http://www.kaufland.sk/Home/02_Sortiment/010_Exquisit/02_Sortiment/index.jsp']
    start_urls = ['http://www.kaufland.sk/Home/02_Sortiment/010_Exquisit/02_Sortiment/004_Udeniny_syry/index.jsp']

    rules = (Rule(SgmlLinkExtractor(allow=['.*']), 'parse_product_item', follow=True),
             )

    def parse_product_item(self, response):
        hxs = HtmlXPathSelector(response)

        if response.url.find('Exquisit') != -1 and len(hxs.select('//div[@id="posItemDescription"]')) > 0:

            for i in range(len(hxs.select('//div[@id="posItemDescription"]'))):
                product_item = ProductItem()
                product_item['path'] = extract_data_item(hxs.select('//h1[@class="content1"]/text()'))
                product_item['image_urls'] = extract_data_list(hxs.select('//div[@id="productpicture"]')[i].select('a/img/@src'))
                product_item['name'] = ' '.join(hxs.select('//div[@id="posItemDescription"]')[i].select('text()').extract()).replace('\t', '').replace('\r', '').replace('\n', '').strip()
                product_item['amount'] = extract_data_item(hxs.select('//div[@class="Mengenangabe"]')[i].select('text()'))
                product_item['price'] = extract_data_item(hxs.select('//div[@class="Verkaufspreis"]')[i].select('text()'))

                yield product_item

            try:
                next_page = hxs.select('//div[@id="scrollBoxright"]/a/@href')[0].extract()
                yield Request(next_page, callback=self.parse_product_item)
            except IndexError:
                pass

