# -*- coding: utf-8 -*-

from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector
from scrapy.spider import BaseSpider
#from myproject.items importer MyItem
from shopper.crawlers.kaufland.sk.core import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

local_to_product_item_name_map = {u'Hmotnost:': 'weight',
                                  u'Čárový kód EAN:': 'bc'}

last_page = 22

class KauflandSpider(BaseSpider):
    name = 'kaufland'
    allowed_domains = ['www.zdravapotravina.cz']

    def start_requests(self):
        for page in range(1, last_page + 1):
            yield Request('http://www.zdravapotravina.cz/detail-firmy/23328?page=%d' % page, self.parse_product_list)

    def parse_product_list(self, response):
        hxs = HtmlXPathSelector(response)

        for url in hxs.select('//table[@class="product-list"]//a/@href').extract():
            if url != '/detail-firmy/23328':
                yield Request('http://www.zdravapotravina.cz/%s' % url, callback=self.parse_product_details)


    def parse_product_details(self, response):
        hxs = HtmlXPathSelector(response)

        product_item = ProductItem()
        product_item['name'] = extract_data_item(hxs.select('//h2[@class="product-headline"]/text()'))
        for product_info_row in hxs.select('//table[@class="product-detail-info__table"]//tr'):
            product_info_name = extract_data_item(product_info_row.select('th/text()'))
            product_info_val = extract_data_item(product_info_row.select('td/text()'))
            if local_to_product_item_name_map.has_key(product_info_name):
                product_item[local_to_product_item_name_map[product_info_name]] = product_info_val

        product_item['image_urls'] = extract_data_list(hxs.select('//img[@class="product-detail-img__main"]/@src'))

        return product_item