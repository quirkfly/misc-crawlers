# -*- coding: utf-8 -*-

__author__ = 'peterd'

import requests
import json
import logging

from shopper.models import SellerBranch, Seller

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

KAUFLAND = Seller.get_seller_by_id(10)

def purify_location_result(location_result):
    city = location_result['city']
    city = city.replace(u'Dubnica n/Váhom', u'Dubnica nad Váhom')
    location_result['city'] = city

    street = location_result['street']
    street = street.replace(u'Trnavská ulica 4455/ 12A', u'Trnavska ulica 12A')
    location_result['street'] = street

    return location_result


session = requests.Session()
session.headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                   'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:30.0) Gecko/20100101 Firefox/30.0',
                   'Accept-Encoding': 'gzip, deflate, sdch',
                   'Accept-Language': 'en-US,en;q=0.8,de;q=0.6,sk;q=0.4'}


resp = session.get('http://www.kaufland.sk/Home/index.jsp')

headers = session.headers
headers['X-Requested-With'] = 'XMLHttpRequest'
headers['Content-Type'] = 'application/x-www-form-urlencoded'

resp = session.post('http://www.kaufland.sk/Storefinder/finder', headers=headers, data='filtersettings=%7B%22filtersettings%22%3A%7B%22area%22%3A%2250.83114564387932%2C32.308521875%2C47.306294449664186%2C12.093678124999997%22%7D%2C%22location%22%3A%7B%22latitude%22%3A49.1%2C%22longitude%22%3A22.201099999999997%7D%2C%22clienttime%22%3A%2220150811160615%22%7D&loadStores=true&locale=SK')

if resp.reason == 'OK':
    for location_result in json.loads(resp.content):
        # skip locations outside Slovakia for now
        if location_result['country'] == 'SK':
            location_result = purify_location_result(location_result)
            seller_branch, created = SellerBranch.get_or_create_from_storefinder_location(KAUFLAND, location_result)
            if created:
                logging.info('Created seller branch: "%s"' % seller_branch)
            else:
                logging.info('Seller branch "%s" already exists' % seller_branch)

else:
    logging.error('Failed to fetch Kaufland branch locations, reason:' % resp.reason)