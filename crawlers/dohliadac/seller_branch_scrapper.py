# -*- coding: utf-8 -*-

#!/usr/bin/env python


__author__ = 'peterd'

import sys
import argparse
import logging
from bs4 import BeautifulSoup

import django
from django.core.exceptions import ObjectDoesNotExist
django.setup()

from shopper.models.product import Seller, Location, SellerBranch
from shopper.crawlers.discount_scrapper import DiscountScrapper

seller_to_seller_branch_url_map = {
    'Billa': 'https://dohliadac.sk/obchody/billa/pobocky/2',
    '101drogerie': 'https://dohliadac.sk/obchody/101-drogerie/pobocky/24',
    'Carrefour': 'https://dohliadac.sk/obchody/carrefour/pobocky/5',
    'CBA Slovakia': 'https://dohliadac.sk/obchody/cba-slovakia/pobocky/17',
    'COOP': 'https://dohliadac.sk/obchody/coop-jednota/pobocky/13',
    'DM': 'https://dohliadac.sk/obchody/dm-drogerie-markt/pobocky/23',
    'Hypernova': 'https://dohliadac.sk/obchody/hypernova/pobocky/14',
    'Kaufland': 'https://dohliadac.sk/obchody/kaufland/pobocky/27',
    #u'Labaš': 'https://dohliadac.sk/obchody/labas/pobocky/29',
    'Lidl': 'https://dohliadac.sk/obchody/lidl/pobocky/3',
    u'Moja samoška': 'https://dohliadac.sk/obchody/moja-samoska/pobocky/16',
    u'Tempo': 'https://dohliadac.sk/obchody/tempo/pobocky/11',
    u'Terno': 'https://dohliadac.sk/obchody/terno/pobocky/15',
    u'Tesco': 'https://dohliadac.sk/obchody/tesco/pobocky/9',
    u'Teta': 'https://dohliadac.sk/obchody/teta/pobocky/19',
    u'1. day': 'https://dohliadac.sk/obchody/1-day/pobocky/25',
    u'MILK-AGRO': 'https://dohliadac.sk/obchody/milkagro/pobocky/21',
    u'Sama': 'https://dohliadac.sk/obchody/sama/pobocky/22',
    u'Fresh': 'https://dohliadac.sk/obchody/fresh/pobocky/26',
}

class DohliadacSellerBranchScrapper(DiscountScrapper):
    def __init__(self, seller, logger):
        super(DohliadacSellerBranchScrapper, self).__init__(seller, logger=logger)


    def scrap_branches(self):
        if seller_to_seller_branch_url_map.has_key(self.seller.name):
            seller_branch_url = seller_to_seller_branch_url_map[self.seller.name]
            response = self.session.get(seller_branch_url)
            try:
                soup = BeautifulSoup(response.content)
                for region in ['BB', 'BA', 'KE', 'NT', 'PO', 'TN', 'TT', 'ZA']:
                    try:
                        for city in soup.find('div', {'id': 'region-%s' % region}).findChildren('div'):
                            city_name = city.find('h3').text
                            for address_line in city.find('p').text.split('\n'):
                                address_line = address_line.strip('\n\t ')
                                if len(address_line) > 0:
                                    location, location_created = Location.get_or_create_from_locality_address_line(
                                        city_name, address_line)
                                    seller_branch, seller_branch_created = SellerBranch.get_or_create_from_location(
                                        self.seller.id, location)
                                    if seller_branch_created:
                                        self.logger.info('Created seller branch: %s' % seller_branch)
                    except Exception, e:
                        self.logger.error('Failed to retrieve branches from region %s, reason: %s' % (region, e))
            except Exception, e:
                self.logger('Failed to retrieve region reason: %s' % e)
        else:
            self.logger.warning('Do not know how to scrap %s branches.' % seller.name)



parser = argparse.ArgumentParser(description='dohliadac seller branch scrapper utility')
parser.add_argument('--seller', '-s', dest='seller',
                    help='Name of seller')
parser.add_argument('--active-sellers', '-a', dest="active_sellers", action='store_true',
                    help='Scraps branches for all active sellers. Has precedens over --seller option.')
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')
args = parser.parse_args()

sellers = []
if args.active_sellers:
    sellers = Seller.get_active_sellers()[0]
else:
    try:
        sellers.append(Seller.get_seller_by_name(args.seller))
    except ObjectDoesNotExist:
        parser.print_usage()
        sys.exit(1)

if len(sellers) == 0:
    parser.print_usage()
    sys.exit(1)

log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)


for seller in sellers:
    logging.info('Scrapping %s branches...' % seller.name)

    seller_branch_scrapper = DohliadacSellerBranchScrapper(seller, logger=logging)
    seller_branch_scrapper.scrap_branches()