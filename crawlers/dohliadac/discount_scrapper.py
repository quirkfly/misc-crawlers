# -*- coding: utf-8 -*-

#!/usr/bin/env python

import sys
import argparse
import re
from bs4 import BeautifulSoup
import logging

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import django
django.setup()

from shopper.crawlers.discount import ImageLink
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.crawlers.dohliadac.discount import DohliadacDiscount
from shopper.exception import SellerDoesNotExist
from shopper.models.product import Seller, SellerProductDiscount, SellerProduct, SellerProductDiscountTrash

__author__ = 'peterd'

seller_map = {'101drogerie': dict(seller_name='101drogerie', url='/obchody/101-drogerie/24'),
              'Billa': dict(seller_name='Billa', url='/obchody/billa/2'),
              'Carrefour': dict(seller_name='Carrefour', url='/obchody/carrefour/5'),
              'CBA-Slovakia': dict(seller_name='CBA Slovakia', url='/obchody/cba-slovakia/17'),
              'COOP': dict(seller_name='COOP', url='/obchody/coop-jednota/13'),
              'DM': dict(seller_name='DM', url='/obchody/dm-drogerie-markt/23'),
              'Hypernova': dict(seller_name='Hypernova', url='/obchody/hypernova/14'),
              'Fresh': dict(seller_name='Fresh', url='/obchody/fresh/26'),
              'Kaufland': dict(seller_name='Kaufland', url='/obchody/kaufland/27'),
              #'Labas': dict(seller_name='Labaš', url='/obchody/labas/letak/29'),
              'Milk-Agro': dict(seller_name='MILK-AGRO', url='/obchody/milkagro/21'),
              'Lidl': dict(seller_name='Lidl', url='/obchody/lidl/3'),
              'Moja-samoska': dict(seller_name=u'Moja samoška', url='/obchody/moja-samoska/16'),
              'Sama': dict(seller_name=u'Sama', url='/obchody/sama/22'),
              'Tempo': dict(seller_name=u'Tempo', url='/obchody/tempo/11'),
              'Terno': dict(seller_name=u'Terno', url='/obchody/terno/15'),
              'Teta': dict(seller_name=u'Teta', url='/obchody/teta/19'),
              '1.day': dict(seller_name=u'1. day', url='/obchody/1-day/25',)
}


class DohliadacScrapper(DiscountScrapper):
    BASE_URL = 'https://dohliadac.sk'

    RE_NEW_PAGE = re.compile(r'newPage=(\d+)')
    RE_DISCOUNT_PERIOD = re.compile(r'.*?(\d{2}.\d{2}.\d{4}.*?)')

    def __init__(self, logger, seller, url, sleep_int=None):
        super(DohliadacScrapper, self).__init__(seller, logger, sleep_int)

        self.url = url


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        num_pages = self.do_scrap(1)
        if num_pages > 1 and not teaser_only:
            for page in range(2, num_pages + 1):
                self.do_scrap(page)

        return self.discount_map


    def do_scrap(self, page):
        try:
            response = self.session.get('%s/%s?newPage=%d&do=updatePage' % (DohliadacScrapper.BASE_URL, self.url, page))

            soup = BeautifulSoup(response.content)

            num_pages = 0
            for link in soup.findAll('a'):
                try:
                    m = re.search(DohliadacScrapper.RE_NEW_PAGE, link['href'])
                    if m:
                        num_pages = int(m.group(1))
                except KeyError:
                    pass
            if num_pages == 0:
                num_pages = 1

            has_discounts = soup.find('div', {'id': 'snippet--discounts'})
            if not has_discounts:
                return

            for cell in soup.find('div', {'id': 'snippet--discounts'}).find(
                    'div', {'class': 'row'}).findAll('div', {'class': 'col-md-4'}):

                item = None
                try:
                    item = cell.find('div', {'class': ['panel', 'panel-default', 'item']})
                    item_heading = item.find('div', {'class': 'panel-heading'}).find('a')
                    abs_product_link = '%s%s' % (DohliadacScrapper.BASE_URL, item_heading['href'])

                    product_name = item_heading['title']
                    product_amount = item_heading.find('span', {'class': 'product-amount'}).text
                    name = '%s %s' % (product_name, product_amount)
                    item_body = item.find('div', {'class': 'panel-body'}).find('a')
                    item_discount = item.find('div', {'class': 'discount'})
                    discount_amount = None
                    if item_discount is not None:
                        item_discount_amount = item_discount.find('div', {'class': 'discount-amount'})
                        if item_discount_amount:
                            discount_amount = int(item_discount_amount.text.strip('-%'))
                    item_price = item.find('div', {'class': ['panel-footer', 'price']})

                    new_price = float(item_price.find('span').text.strip(u'\n\t €').replace(',', '.'))
                    item_expiration = item.find('div', {'class': ['expiration']}).text.strip('\n\t ')
                    m = re.findall(DohliadacScrapper.RE_DISCOUNT_PERIOD, item_expiration)
                    if m and len(m) == 2:
                        valid_from = m[0]
                        valid_until = m[1]

                        if discount_amount:
                            old_price = round(new_price / float((100 - discount_amount) * 0.01), 2)
                        else:
                            old_price = None

                        discount = DohliadacDiscount(abs_product_link, None, name, valid_from, valid_until,
                                            self.seller, None, new_price, old_price, date_format='%d.%m.%Y')

                        # skip the product if trashed
                        if SellerProductDiscountTrash.discount_exists(discount):
                            logger.info('skipping "%s" as has been trashed' % discount.name)
                            continue

                        # skip the product if scrapped already
                        if SellerProductDiscount.discount_exists(discount):
                            logger.info('skipping "%s" as already scrapped' % discount.name)
                            continue

                        seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                                discount.name,
                                                                discount.url,
                                                                self.seller.id)
                        if seller_product_discount and seller_product_discount.seller_product_id is not None:
                            seller_product_discount.on_discount_valid(discount)

                            seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                            seller_product.on_discount_valid(seller_product_discount)
                        else:
                            image_link = ImageLink(item_body.find('img'), base_url=DohliadacScrapper.BASE_URL)
                            image_link.fetch(self.session)

                            discount.image_file = image_link.rel_image_path
                            seller_product = discount.save(logger=self.logger)

                            if seller_product is None:
                                self.add(discount)
                            else:
                                if seller_product.must_recategorize:
                                    self.on_must_recategorize(seller_product)

                        self.logger.debug(u'scrapped discount: %s' % discount)
                    else:
                        self.logger.exception(u'failed to fetch valid from and valid until for product %s' % product_name)
                except Exception, e:
                    self.logger.exception(u'Failed to scrap a discount, item: "%s", reason: %s' % (item,
                                                                                        e.message.decode('utf-8')))
        except Exception, e:
            self.logger.exception(u'Failed to get discount cells, reason: %s' % e.message.decode('utf-8'))

        return num_pages



parser = argparse.ArgumentParser(description='dohliadac discount scrapper utility')
parser.add_argument('--seller', '-s', dest="seller",
                    help='Seller name, whose products are to be scrapped. Supported sellers: %s' % seller_map.keys())
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--teaser-only', '-t', dest="teaser_only", action='store_true',
                    help='Scraps only few discounts.')
parser.add_argument('--dont-send-mail', dest="dont_send_email", action='store_true',
                    help='Do not send a scrapping email report. The report is sent by default.')

args = parser.parse_args()


if args.seller is None or not seller_map.has_key(args.seller):
    parser.print_help()
    sys.exit(1)

try:
    seller = Seller.get_seller_by_name(seller_map[args.seller]['seller_name'])
except SellerDoesNotExist:
    parser.print_help()
    sys.exit(1)

log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)

logger.info('Scrapping discounts...')

discount_scrapper = DohliadacScrapper(logger=logger, seller=seller,
                                      url=seller_map[args.seller]['url'], sleep_int=None)
discount_scrapper.scrap(teaser_only=args.teaser_only)
if not args.dont_send_email:
    discount_scrapper.send_mail()