#!/bin/bash -u

# main script that orchestrates the scrapping process from hofer.at
#

VERBOSITY=0
LOG_FILE=
TEASER_ONLY=0
DONT_TORIFY=0
OPTIONS=

function usage {
	echo "lidl sk scrapper wrapper"
	echo "`basename $0` [-v] [-l <log file] [-t]"
    echo "v - be verbose about actions performed"
	echo "<log file> - if not specify log to console"
	echo "t - scrap only first page"
	exit 1
}


while getopts vl:tr FLAG; do
	case $FLAG in
	v)
		VERBOSITY=1;
		;;
    l)
		LOG_FILE=$OPTARG;
     	;; 
    t)
		TEASER_ONLY=1;
      	;;
    r)
		DONT_TORIFY=1;
      	;;
    ?)
     	usage;
      	;;
  esac
done

cd $ITRASH_HOME_DIR

if [ $VERBOSITY -eq 1 ]
then
	OPTIONS="--loglevel DEBUG"
fi

if [ ! -z "$LOG_FILE" ]
then
	OPTIONS=" $OPTIONS --logfile $LOG_FILE"
fi

if [ $TEASER_ONLY -eq 1 ]
then
	OPTIONS=" $OPTIONS --teaser-only"
fi

if [ $DONT_TORIFY -eq 1 ]
then
	OPTIONS=" $OPTIONS --dont-torify"
fi


PYTHONPATH=`pwd`:.. DJANGO_SETTINGS_MODULE=itrash.settings python shopper/crawlers/lidl/discount_scrapper.py $OPTIONS

if [ ! -z "$LOG_FILE" ]
then
    #
    # check scrapper log for errors
    #

    NUM_WRITE_ERRORS=`grep -i error $LOG_FILE | wc -l`
    if [ $NUM_WRITE_ERRORS -gt 0 ]
    then
        grep -i -A100 error $LOG_FILE > /tmp/discount_scrapper.err
        python2.7 utils/mailer.py --from_addr discount.scrapper@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to scrap $NUM_WRITE_ERRORS lidl sk discounts." /tmp/discount_scrapper.err
        rm /tmp/discount_scrapper.err
    fi
fi