# -*- coding: utf-8 -*-

import argparse
import logging
from random import randint
import re
from moneyed import EUR, Money

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import simplejson
from datetime import datetime, time

import django
django.setup()

from shopper.crawlers.discount import ImageLink, ImageDoesNotExist
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.models.product import Seller, SellerProductDiscountTrash, SellerProductDiscount, SellerProduct
from shopper.crawlers.lidl.discount import LidlDiscount

__author__ = 'peterd'

parser = argparse.ArgumentParser(description='lidl discount scrapper')
parser.add_argument('-l', '--loglevel',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set the logging level')
parser.add_argument('-f', '--logfile',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--dont-send-mail', dest="dont_send_email", action='store_true',
                    help='Do not send a scrapping email report. The report is sent by default.')

args = parser.parse_args()

if args.loglevel:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=args.loglevel)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=args.loglevel)
else:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

class LidlDiscountScrapper(DiscountScrapper):
    BASE_URL = 'https://mobile.lidl.de/Mobile-Server/service/20'
    USER_ID = 'prodapiuser'
    PASSWORD = 'xxxxxxxxxxx'

    x_devices = ['LENOVO Lenovo K53a48', 'LGE Nexus 4', 'LG Volt 4G', 'HTC One', 'Samsung Galaxy A5',
                 'Samsung Galaxy S8', 'Samsung Galaxy A3', 'Huawei P9 Plus', 'Huawei P10']
    x_oses = ['Android 4.4.4', 'Android 4.2.1', 'Android 5.1.0', 'Android 5.1.1', 'Android 6.0.1',
              'Android 7.0.2']

    def __init__(self, logger, seller):
        super(LidlDiscountScrapper, self).__init__(seller, logger, sleep_int=0)


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        session = self.session
        session.headers = {'Accept': 'application/json,text/html,application/xhtml+xml,'
                                     'application/xml;q=0.9,image/webp,*/*;q=0.8',
           'Accept-Encoding': 'gzip',
           'User-Agent': 'Lidl/3.0.0(575) Android',
           'X-Device': LidlDiscountScrapper.x_devices[randint(0, len(LidlDiscountScrapper.x_devices) - 1)],
           'X-OS': LidlDiscountScrapper.x_oses[randint(0, len(LidlDiscountScrapper.x_oses) - 1)]}

        response = session.get('%s/containerService/SK/root/sk' % LidlDiscountScrapper.BASE_URL,
                               auth=(LidlDiscountScrapper.USER_ID, LidlDiscountScrapper.PASSWORD))
        root = simplejson.loads(response.content)

        for container_item in root['RootContainer']['ContainerItems']:
            for grouped_container_item in container_item['GroupedContainer']['ContainerItems']:
                campaign = grouped_container_item['Campaign']
                start_date = datetime.fromtimestamp(campaign['starttime'] / 1000)
                end_date = datetime.fromtimestamp(campaign['endtime'] / 1000)

                response = session.get('%s/%s' % (LidlDiscountScrapper.BASE_URL, campaign['dataPath']),
                                       auth=(LidlDiscountScrapper.USER_ID, LidlDiscountScrapper.PASSWORD))
                campain_data = simplejson.loads(response.content)
                for container_item in campain_data['Campaign']['ContainerItems']:
                    try:
                        product = container_item['Product']
                        price = product['price']
                        name = '%s %s' % (product['productLanguageSet']['ProductLanguageSet']['title'],
                            product['productLanguageSet']['ProductLanguageSet']['packagingText'].split('=')[0].strip())
                        discount_valid_from = '%s%s' % (
                            product['productLanguageSet']['ProductLanguageSet']['specialDescription'].split('-')[0].strip(),
                            start_date.year
                        )
                        discount_valid_until = '%s%s' % (
                            product['productLanguageSet']['ProductLanguageSet']['specialDescription'].split('-')[1].strip(),
                            end_date.year
                        )

                        abs_product_link = '%s/%s' % (LidlDiscountScrapper.BASE_URL, product['dataPath'])

                        discount = LidlDiscount(abs_product_link, None, name,
                                                discount_valid_from, discount_valid_until,
                                                self.seller, price)

                        # skip the product if trashed
                        if SellerProductDiscountTrash.discount_exists(discount):
                            logger.info('skipping "%s" as has been trashed' % discount.name)
                            continue

                        # skip the product if scrapped already
                        if SellerProductDiscount.discount_exists(discount, match_by_url=True):
                            logger.info('skipping "%s" as already scrapped' % discount.name)
                            continue

                        seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                                discount.name,
                                                                discount.url,
                                                                self.seller.id)
                        if seller_product_discount and seller_product_discount.seller_product_id is not None:
                            seller_product_discount.on_discount_valid(discount)

                            seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                            seller_product.price_100g_with_vat_old = Money(0.0, EUR)
                            seller_product.price_1kg_with_vat_old = Money(0.0, EUR)
                            seller_product.on_discount_valid(seller_product_discount)
                        else:
                            image_link = ImageLink(dict(src=product['mainImage']['Image']['largeUrl']))

                            num_tries = 3
                            i = 0
                            while num_tries > 0:
                                try:
                                    image_link.fetch(session)
                                    break
                                except ImageDoesNotExist:
                                    break
                                except RuntimeError, e:
                                    num_tries -= 1
                                    i += 1
                                    logger.error('failed to fetch product image, reason: %s, num_tries left: %d' % (e,
                                                                                                                    num_tries))
                                    if num_tries > 0:
                                        time.sleep(5 * i)

                            discount.image_file = image_link.rel_image_path

                            seller_product = discount.save(logger=self.logger)

                            if seller_product is None:
                                self.add(discount)
                            else:
                                if seller_product.must_recategorize:
                                    self.on_must_recategorize(seller_product)

                        self.logger.info(u'scrapped discount: %s' % discount)
                    except Exception, e:
                        logger.exception('failed to process container item "%s", reason: %s' % (container_item, e))

try:
    lidl_discount_scrapper = LidlDiscountScrapper(logger, Seller.get_seller_by_name('lidl'))
    lidl_discount_scrapper.scrap()
    if not args.dont_send_email:
        lidl_discount_scrapper.send_mail()
except Exception, e:
    logger.exception('Failed to scrap lidl discounts, reason: %s' % e)
