__author__ = 'peterd'

from shopper.crawlers.discount import Discount

from moneyed import EUR


class LidlDiscount(Discount):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller,
                 new_price, currency=EUR, date_format='%d.%m.%Y', unit=None):
        super(LidlDiscount, self).__init__(url, image_file, name, valid_from, valid_until, seller, None,
                                           new_price, None, currency, date_format, unit)