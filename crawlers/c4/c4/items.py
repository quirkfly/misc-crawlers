# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class ProductItem(Item):
	id = Field()
	bc = Field()
	name = Field()
	link = Field()
	path = Field()
	price = Field()
	price_unit = Field()
	image_urls = Field()
	images = Field()
	details = Field()
