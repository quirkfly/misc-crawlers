import re
import sys

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.exceptions import CloseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy import log

from shopper.crawlers.c4.c4.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

class C4Spider(CrawlSpider):
	name = "martinus"
	allowed_domains = ["carrefourdrive.sk"]
	start_urls = [
        "http://www.carrefourdrive.sk/c4d/ba-dan/"        
    ]
	rules = (Rule(SgmlLinkExtractor(allow=['\d+-.*']), 'parse_product_item', follow=True),)
	
	def parse_product_item(self, response):
		product_barcode = re.search('.*?-(\d{6,13})\.html', response.url)
		if not product_barcode:
			return None

		product_barcode = product_barcode.group(1)

		hxs = HtmlXPathSelector(response)

		product_item = ProductItem()
		product_item['name'] = extract_data_item(hxs.select('//div[@id="primary_block"]/h1/text()'))
		product_item['link'] = response.url 
		product_item['path'] =  hxs.select('//div[@id="breadcrumb_c"]/a/text()').extract()
		
		product_item['price'] = extract_data_item(hxs.select('//span[@id="our_price_display"]/text()'))
		product_item['price_unit'] = {'price': extract_data_item(hxs.select('//p[@class="unit-price"]/span/text()')),
					                  'unit': extract_data_item(hxs.select('//p[@class="unit-price"]/text()'))}
		product_item['id'] = extract_data_item(hxs.select('//p[@id="product_reference"]/span/text()'))
		product_item['image_urls'] = extract_data_list(hxs.select('//img[@id="bigpic"]/@src'))
		product_item['details'] = extract_data_item(hxs.select('//div[@id="idTab1"]/text()')) 
		product_item['bc'] = product_barcode

		return product_item
