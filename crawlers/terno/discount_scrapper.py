# -*- coding: utf-8 -*-

#
# terno product scrapper
#

import argparse
import json
import logging
import os
import re
from datetime import date
import sys
from bs4 import BeautifulSoup

import requests

import django
django.setup()

from itrash.itrash_settings import ITRASH_DATA_ROOT
from shopper.crawlers.discount import ImageLink
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.crawlers.terno.discount import TernoDiscount
from shopper.exception import SellerDoesNotExist
from shopper.models.product import SellerProduct, Seller, SellerProductDiscountTrash, SellerProductDiscount


seller_map = {'terno': u'Terno',
              'terno-plus': u'Terno',
              'hypernova': u'Hypernova',
              'moja-samoska': u'Moja samoška'
}


class TernoScrapper(DiscountScrapper):
    BASE_URL = 'https://www.terno.sk'

    RE_DISCOUNT_VALID_FROM_TO = re.compile(r'Produkty v akcii od (\d+)\.(\d+)\. do (\d+)\.(\d+)\.')
    RE_IMAGE_URL = re.compile(r"background-image: url\('(.*?)'\)")
    RE_PRICE = re.compile(r'(?P<euros>\d+)\s+(?P<cents>\d+)')
    RE_AMOUNT = re.compile(r'(?P<amount>\d*\s*x*\s*\d*,*\d+\s*(g|kg|ml|l|m))')
    RE_PRICE_UNIT = re.compile(r'1 (?P<unit>kg|l|dávka|m) = (?P<price>\d+,*\d*)')

    def __init__(self, logger, seller, seller_tag, price_list_id):
        super(TernoScrapper, self).__init__(seller, logger)

        self.seller_tag = seller_tag
        self.price_list_id = price_list_id


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        session = requests.Session()
        session.headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko)' \
                                ' Chrome/48.0.2564.82 Safari/537.36'

        response = session.get('https://www.terno.sk/akciova-ponuka')
        soup = BeautifulSoup(response.content)
        token = soup.find('input', {'name': '_token'})['value']

        has_discounts = soup.find('li', {'role': 'presentation'})
        if has_discounts:
            m = TernoScrapper.RE_DISCOUNT_VALID_FROM_TO.search(soup.find('li', {'role': 'presentation'}).text)
            if m:
                valid_from = '%s.%s.%s' % (m.group(1), m.group(2), date.today().year)
                valid_until = '%s.%s.%s' % (m.group(3), m.group(4), date.today().year)

                got_sale_items = True
                num_sale_items = 0

                while got_sale_items:
                    try:
                        response = json.loads(session.post('%s/products/category' % TernoScrapper.BASE_URL,
                                                           data=dict(_token=token,
                                                                     category_id=0,
                                                                     count=num_sale_items,
                                                                     id=self.price_list_id,
                                                                     stores=self.seller_tag)).content)

                        got_sale_items = len(response['html']) > 0

                        if got_sale_items:
                            soup = BeautifulSoup(response['html'])
                            sale_items = soup.findAll('div', {'class': 'sale-item'})
                            num_sale_items += len(sale_items)
                            for sale_item in sale_items:
                                try:
                                    image_url = TernoScrapper.RE_IMAGE_URL.search(sale_item.find('div', {'class': 'image'})['style']).group(1)
                                    image_link = ImageLink(dict(src=image_url))
                                    image_link.fetch(session)

                                    name = sale_item.find('div', {'class': 'name'}).text.strip()
                                    desc = sale_item.find('div', {'class': 'size'}).text.strip()

                                    price_tags = sale_item.find('div', {'class': 'price-cont'})
                                    new_price_tag = TernoScrapper.RE_PRICE.search(price_tags.find('div',
                                                        {'class': 'price'}).text.strip().replace('\r', '').replace('\n', '')).groupdict()
                                    new_price = float('%s.%s' % (new_price_tag['euros'], new_price_tag['cents']))

                                    old_price_tag = price_tags.find('div', {'class': 'old-value'})
                                    if old_price_tag:
                                        old_price_tag = TernoScrapper.RE_PRICE.search(old_price_tag.find('div',
                                                            {'class': 'stroke'}).text.strip().replace('\r', '').replace('\n', '')).groupdict()
                                        old_price = float('%s.%s' % (old_price_tag['euros'], old_price_tag['cents']))
                                    else:
                                        old_price = None
                                    discount_tag = price_tags.find('div', {'class': 'sale'})
                                    if discount_tag:
                                        discount_amount = int(discount_tag.text.replace('\r', '').replace('\n', '').replace('-', '').replace('%', '').strip())
                                    else:
                                        discount_amount = 0

                                    m = TernoScrapper.RE_AMOUNT.search(desc)
                                    if m:
                                        name = u'%s %s' % (name, m.groupdict()['amount'])

                                    m = TernoScrapper.RE_PRICE_UNIT.search(desc)
                                    if m:
                                        unit = m.groupdict()['unit']
                                        price_unit = float(m.groupdict()['price'].replace(',', '.'))
                                    else:
                                        unit = None
                                        price_unit = None

                                    discount = TernoDiscount(url=image_url, image_file=image_link.rel_image_path,
                                                             name=name, valid_from=valid_from, valid_until=valid_until,
                                                             seller=seller, description=desc,
                                                             new_price=new_price, old_price=old_price,
                                                             discount=discount_amount,
                                                             unit=unit, price_unit=price_unit)

                                    # skip the product if trashed
                                    if SellerProductDiscountTrash.discount_exists(discount):
                                        logger.info('skipping "%s" as has been trashed' % discount.name)
                                        continue

                                    # skip the product if scrapped already
                                    if SellerProductDiscount.discount_exists(discount):
                                        logger.info('skipping "%s" as already scrapped' % discount.name)
                                        continue

                                    seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                                            discount.name,
                                                                            discount.url,
                                                                            seller.id)
                                    if seller_product_discount and seller_product_discount.seller_product_id is not None:
                                        seller_product_discount.on_discount_valid(discount)

                                        seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                                        seller_product.on_discount_valid(seller_product_discount)
                                    else:
                                        seller_product = discount.save(logger=logger)

                                        if seller_product is None:
                                            self.add(discount)
                                        else:
                                            if seller_product.must_recategorize:
                                                self.on_must_recategorize(seller_product)

                                    logger.debug(u'scrapped discount: %s' % discount)
                                except Exception, e:
                                    logger.exception('failed to process sale item %s, reason: %s' % (sale_item, e))
                    except Exception, e:
                        logger.exception('failed to fetch sale items (num_sale_items = %d), reason: %s' % (
                            num_sale_items, e))
            else:
                logger.error('failed to get %s discounts (missing discount period' % args.seller)
        else:
            logger.error('failed to get %s discounts (missing presentation role)' % args.seller)


parser = argparse.ArgumentParser(description='terno discount scrapper utility')
parser.add_argument('--seller', '-s', dest="seller",
                    help='Seller name, whose products are to be scrapped. Supported sellers: %s' % seller_map.keys())
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--teaser-only', '-t', dest="teaser_only", action='store_true',
                    help='Scraps only few discounts.')
parser.add_argument('--dont-send-mail', dest="dont_send_email", action='store_true',
                    help='Do not send a scrapping email report. The report is sent by default.')
parser.add_argument('--price-list-id', dest="price_list_id",
                    help='Price list id identifing the discount period to be passed to seller.')

args = parser.parse_args()


if args.seller is None or not seller_map.has_key(args.seller):
    parser.print_help()
    sys.exit(1)

try:
    seller = Seller.get_seller_by_name(seller_map[args.seller])
except SellerDoesNotExist:
    parser.print_help()
    sys.exit(1)

log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)

logger.info('Scrapping discounts...')

price_list_id_file = os.path.join(ITRASH_DATA_ROOT, 'sellers', args.seller, 'prices', 'price_list.id')

if args.price_list_id:
    price_list_id = int(args.price_list_id)
elif os.path.exists(price_list_id_file):
    with open(price_list_id_file, 'r') as f:
        price_list_id = int(f.read())
else:
    logger.error('Missing price list id, bailing out')
    sys.exit(1)

discount_scrapper = TernoScrapper(logger=logger, seller=seller, seller_tag=args.seller, price_list_id=price_list_id)
discount_scrapper.scrap(teaser_only=args.teaser_only)
if not args.dont_send_email:
    discount_scrapper.send_mail()

if discount_scrapper.get_num_scrapped_discounts() > 0:
    with open(price_list_id_file, 'w') as f:
        f.write(str(price_list_id + 1))