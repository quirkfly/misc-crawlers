__author__ = 'peterd'

from shopper.crawlers.discount import Discount

from moneyed import EUR


class TernoDiscount(Discount):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller, description,
                 new_price, old_price, discount, currency=EUR, date_format='%d.%m.%Y', unit=None, price_unit=None):
        super(TernoDiscount, self).__init__(url, image_file, name, valid_from, valid_until, seller, description,
                                            new_price, old_price, currency, date_format, unit, price_unit,
                                            discount)
