# -*- coding: utf-8 -*-

#!/usr/bin/env python

import argparse
import logging
import os
from django.core.mail import mail_admins
import re
import requests
from bs4 import BeautifulSoup
import sys

import django
from infra.lexical import CharMapper

django.setup()

from shopper.crawlers.discount import ImageLink
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.crawlers.firstday.discount import FirstDayDiscount
from infra.pdf.utils import pdf_to_str
from itrash.settings import MEDIA_ROOT
from shopper.models.product import Seller, SellerProductDiscount, SellerProductDiscountTrash, SellerProduct


class FirstDayScrapper(DiscountScrapper):
    BASE_URL = 'https://www.1day.sk'

    RE_LEAFLET_PERIOD = re.compile('AKCIA TRVÁ OD (\d{2}\.\d{2}\.\d{4}) DO (\d{2}\.\d{2}\.\d{4})')
    RE_EAN_PC = re.compile('<td>(\d{8,13})</td>')

    def __init__(self, logger, seller, url, sleep_int=None, dont_torify=False):
        super(FirstDayScrapper, self).__init__(seller, logger, sleep_int=sleep_int, dont_torify=dont_torify)

        self.url = url

        response = requests.get('%s/letakove-akcie/' % (FirstDayScrapper.BASE_URL))
        soup = BeautifulSoup(response.content)
        current_leaflet_link = soup.find('div', {'class': 'documentDownloadItem'}).find('a')['href']
        response = requests.get(current_leaflet_link)
        current_leaflet_name = os.path.basename(soup.find('div', {'class': 'documentDownloadItem'}).find('a')['href'])

        leaflet_dir = os.path.join(MEDIA_ROOT, 'product', 'seller', '%d' % seller.id, 'leaflet')
        if not os.path.exists(leaflet_dir):
            os.makedirs(leaflet_dir)

        current_leaflet_file = os.path.join(leaflet_dir, current_leaflet_name)

        if os.path.exists(current_leaflet_file):
            logger.info('"%s" leaflet has been processed already' % current_leaflet_name)
            sys.exit(0)

        with open(current_leaflet_file, 'wb') as f:
            for chunk in response:
                f.write(chunk)

        self.valid_from = None
        self.valid_until = None

        m = FirstDayScrapper.RE_LEAFLET_PERIOD.search(pdf_to_str(current_leaflet_file))
        if m:
            self.valid_from = m.group(1)
            self.valid_until = m.group(2)

            logger.info('leaflet is valid from %s to %s' % (self.valid_from, self.valid_until))

        if self.valid_from is None or self.valid_until is None:
            raise RuntimeError('failed to get discount period from file "%s"' % current_leaflet_file)


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        page = 0

        has_more_pages = self.do_scrap(page)
        while has_more_pages:
            page += 1
            has_more_pages = self.do_scrap(page)

        logger.info('scrapped %d discounts' % len(self.discount_map))

        return self.discount_map


    def do_scrap(self, page):
        try:
            logger.info('scrapping page %d...' % page)

            response = self.session.get('%s/%s/?&offset=%d&limit=24' % (FirstDayScrapper.BASE_URL, self.url,
                                                                        page * 24))
            soup = BeautifulSoup(response.content)
            for product in soup.findAll('div', {'class': 'list-product'}):
                name = product.find('a')['title']
                try:
                    abs_product_link = '%s%s' % (FirstDayScrapper.BASE_URL, product.find('a')['href'])
                    new_price = float(product.find('span', {'class': 'price'}).find('strong').text.strip(u'\n\t €').replace(',', '.'))

                    m = FirstDayScrapper.RE_EAN_PC.search(self.session.get(abs_product_link).content)
                    if m:
                        ean_pc = m.group(1)
                    else:
                        ean_pc = None

                    discount = FirstDayDiscount(abs_product_link, None, name, self.valid_from, self.valid_until,
                                                seller, None, new_price, None, ean_pc=ean_pc)

                    logger.debug('processing %s...' % discount)

                    # skip the product if trashed
                    if SellerProductDiscountTrash.discount_exists(discount):
                        logger.info('skipping "%s" as has been trashed' % discount.name)
                        continue


                    # skip the product if scrapped already
                    if SellerProductDiscount.discount_exists(discount):
                        logger.info('skipping "%s" as already scrapped' % discount.name)
                        continue

                    # skip the product if scrapped already
                    if SellerProductDiscount.discount_exists(discount):
                        logger.info('skipping "%s" as already scrapped' % discount.name)
                        continue

                    seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                            discount.name,
                                                            discount.url,
                                                            seller.id)
                    if seller_product_discount and seller_product_discount.seller_product_id is not None:
                        seller_product_discount.on_discount_valid(discount)

                        seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                        seller_product.on_discount_valid(seller_product_discount)
                    else:
                        image_link = ImageLink(product.find('img'))
                        image_link.fetch(self.session)

                        discount.image_file = image_link.rel_image_path
                        seller_product = discount.save(logger=logger)

                        if seller_product is None:
                            self.add(discount)
                        else:
                            if seller_product.must_recategorize:
                                self.on_must_recategorize(seller_product)

                    logger.debug(u'scrapped discount: %s' % discount)
                except Exception, e:
                    err_msg = u'Failed to scrap "%s" reason: %s' % (name, e)
                    logger.exception(u'Failed to scrap "%s" reason: %s' % (name, e))
                    self.err_msgs.append(err_msg)

            btn = soup.find('button', {'class': ['button', 'next-offset']})

            return btn is not None and CharMapper.cp1250_to_ascii(btn.text).lower() == 'dalsie'

        except Exception, e:
            self.logger.exception(u'Failed to scrap page %d, reason: %s' % (page, e.message.decode('utf-8')))
            raise



parser = argparse.ArgumentParser(description='1. day discount scrapper')
parser.add_argument('--teaser-only', '-t', dest="teaser_only", action='store_true',
                    help='Scraps only few discounts.')
parser.add_argument('--dont-send-mail', dest="dont_send_email", action='store_true',
                    help='Do not send a scrapping email report. The report is sent by default.')
parser.add_argument('-l', '--loglevel',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set the logging level')
parser.add_argument('-f', '--logfile',
                    help='File where the logging will be written. If not specified logging will go to console.')

args = parser.parse_args()

if args.loglevel:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=args.loglevel)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=args.loglevel)
else:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

seller = Seller.objects.get(name='1. day')

try:
    first_day_scrapper = FirstDayScrapper(logger, seller, 'catalog/browse/letak', dont_torify=True)
    first_day_scrapper.scrap()
    if not args.dont_send_email:
        first_day_scrapper.send_mail()

    num_errs = len(first_day_scrapper.err_msgs)
    if num_errs > 0:
        errs = u' '.join(first_day_scrapper.err_msgs)
        mail_admins('[%s discount scrapper] Failed to scrap %d discounts' % (seller.name, num_errs),
                    'failed to scrap %d discounts reason: %s' % (num_errs, errs))

except Exception, e:
    logger.exception('failed to scrap 1. day discounts, reason: %s' % e)

    mail_admins('[%s discount scrapper] Failed to scrap discounts',
                'failed to scrap discounts reason: %s' % (seller.name, e))