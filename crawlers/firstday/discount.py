__author__ = 'peterd'

from shopper.crawlers.discount import Discount

from moneyed import EUR


class FirstDayDiscount(Discount):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller, description,
                 new_price, old_price, currency=EUR, date_format='%d.%m.%Y', ean_pc=None):
        super(FirstDayDiscount, self).__init__(url, image_file, name, valid_from, valid_until, seller, description,
                                                new_price, old_price, currency, date_format, ean_pc=ean_pc)