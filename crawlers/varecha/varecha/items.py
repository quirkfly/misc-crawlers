# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class RecipeItem(Item):
    path = Field()
    leaf_path_title = Field()
    name = Field()
    url = Field()
    image_thumb = Field()
    image_full = Field()
    teaser = Field()
    ingredients = Field()
    serves = Field()
    preparation_time = Field()
    cooking_time = Field()