# varecha recipe preview scrapper

import re
import requests


from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector

from shopper.crawlers.varecha.varecha.items import RecipeItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

VARECHA_URL = 'varecha.pravda.sk'
RECIPES_URL = 'http://varecha.pravda.sk/recepty/'

class VarechaSpider(CrawlSpider):
    name = 'varecha'
    allowed_domains = [VARECHA_URL]
    start_urls = [RECIPES_URL]

    rules = (Rule(LinkExtractor(deny=['suroviny', 'recepty/.*?/\d+-recept.html', 'recepty/pridaj-recept',
                                      'recepty$', 'recepty/dennik'],
                                allow=['recepty/.*?/']),
                  callback='parse_recipe_category', follow=True),)


    def parse_recipe_category(self, response):
        if response.url == RECIPES_URL.rstrip('/'):
            return

        hxs = Selector(response)

        url = response.url.replace(RECIPES_URL, '')
        url = re.sub('\?.*', '', url)
        url = url.rstrip('/')

        recipe_category_leaf_title = extract_data_item(hxs.xpath('//h2[@class="module-ingredients__title"]/span/text()'))

        for recipe_preview_selector in hxs.xpath('//ul[@class="list list-reset list--recipes"]/li'):
            recipe = Recipe(url, recipe_category_leaf_title, recipe_preview_selector)
            yield recipe.to_item()


class Recipe(object):
    def __init__(self, path, leaf_path_title, selector):
        self.path = path
        self.leaf_path_title = leaf_path_title
        self.name = extract_data_item(selector.xpath('div/h3[@class="list__item-title"]/a/text()'))
        self.url = extract_data_item(selector.xpath('div/h3[@class="list__item-title"]/a/@href'))
        self.image_thumb = extract_data_item(selector.xpath('a[@class="list__item-img media__img"]/img/@src'))
        self.teaser = extract_data_item(selector.xpath('div[@class="list__item-main media__main"]/p[@class="list__item-subtitle"]/text()'))

        r = requests.get('http://%s%s' % (VARECHA_URL, self.url))
        hxs = Selector(text=r.content)

        self.image_full = extract_data_item(hxs.xpath('//div[@class="recipe-photo"]/img/@src'))

        self.ingredients = []
        for ingredient_selector in hxs.xpath('//tbody/tr[@class="recipe-ingredients__row"]'):
            name = extract_data_item(ingredient_selector.xpath('td[@class="recipe-ingredients__ingredient"]/div/label/span/a/text()'))
            amount = extract_data_item(ingredient_selector.xpath('td[@class="recipe-ingredients__amount"]/text()'))
            self.ingredients.append({'name': name, 'amount': amount})

        try:
            self.serves = extract_data_item(hxs.xpath('//div[@class="recipe-info__item"]')[0].xpath('div/div/div/text()'))
        except Exception, e:
            self.serves = None

        try:
            self.preparation_time = extract_data_item(hxs.xpath('//div[@class="recipe-info__item"]')[1].xpath('div/div/div/text()'))
        except Exception, e:
            self.preparation_time = None

        self.cooking_time = extract_data_item(hxs.xpath('//div[@class="recipe-info__item recipe-info__item--cooking"]').xpath('div/div/div/text()'))


    def to_item(self):
        recipe_item = RecipeItem()
        recipe_item['path'] = self.path
        recipe_item['leaf_path_title'] = self.leaf_path_title
        recipe_item['name'] = self.name
        recipe_item['url'] = self.url
        recipe_item['image_thumb'] = self.image_thumb
        recipe_item['image_full'] = self.image_full
        recipe_item['teaser'] = self.teaser
        recipe_item['ingredients'] = self.ingredients
        recipe_item['serves'] = self.serves
        recipe_item['preparation_time'] = self.preparation_time
        recipe_item['cooking_time'] = self.cooking_time

        return recipe_item


    def __unicode__(self):
        return self.name
