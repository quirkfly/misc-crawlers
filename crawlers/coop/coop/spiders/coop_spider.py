import os 
import re

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log

from shopper.crawlers.coop.coop.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

PRODUCT_HOME_PAGE_URL='http://www.coop.sk/sk/zakaznik/vlastne-znacky'

class COOPSpider(CrawlSpider):
	name = "coop"
	allowed_domains = ['www.coop.sk']
	start_urls = [PRODUCT_HOME_PAGE_URL,
				 ]

	rules = (Rule(SgmlLinkExtractor(allow='/vlastne-znacky/.*'), callback='parse_product_item', follow=True), )

	def parse_product_item(self, response):

		log.msg("from parse_product_item()", log.DEBUG)

		hxs = HtmlXPathSelector(response)

		# is this product detail page
		product_image = extract_data_item(hxs.select('//img/@src'))
		if product_image is None or product_image.find('product_images/detail') == -1:
			return None

		product_item = ProductItem()

		product_item['name'] = extract_data_item(hxs.select('//title/text()'))
		product_item['brand'] = extract_data_item(hxs.select('//p/span/text()'))
		product_item['link'] = response.url

		log.msg(product_item, level=log.DEBUG)

		img_file = extract_data_item(hxs.select('//img/@src')).split('/')[3]
		product_item['bc'] = os.path.splitext(img_file)[0] 

		path = response.url.replace(PRODUCT_HOME_PAGE_URL, '').split('/')
		del path[-1]
		product_item['path'] = path

		product_item['price'] = None
		product_item['price_unit'] = {'price': None,
				                      'unit': None}
			
		product_item['image_urls'] = extract_data_list(hxs.select('//img/@src'), prefix = 'http://www.coop.sk')		 
				
		product_item['details'] = None
			
		return product_item
