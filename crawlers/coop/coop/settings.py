# Scrapy settings for t3 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'coop'


SPIDER_MODULES = ['coop.spiders']
NEWSPIDER_MODULE = 'coop.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1'

RETRY_HTTP_CODES = [500, 502, 503, 504, 400, 403, 408, 111]
RETRY_TIMES = 1

DOWNLOADER_MIDDLEWARES = {                          
    'crawler.hmss.downloadmiddleware.retry.ProxyRetryMiddleware': 500,
    'scrapy.contrib.downloadermiddleware.retry.RetryMiddleware': None,
    'crawler.hmss.downloadmiddleware.httpproxy.LiveHttpProxyMiddleware': 750,
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': None,    
}

# TODO: consider enabling these once get familiar with their function
#DOWNLOAD_DELAY = 0.25
#DUPEFILTER=True
#COOKIES_ENABLED=False
#RANDOMIZE_DOWNLOAD_DELAY=True
#SCHEDULER_ORDER='BFO'


ITEM_PIPELINES = ['scrapy.contrib.pipeline.images.ImagesPipeline']

IMAGES_STORE = '/var/data/itrash/media/images'

IMAGES_THUMBS = {
    'small': (60, 60),
    'big': (135, 135),
}

