from decimal import Decimal
import sys

import django
from django.core.exceptions import ObjectDoesNotExist
from shopper.exception import ProductUnitDoesNotExist

django.setup()

from moneyed import Money
from moneyed import EUR

from infra.lexical import CharMapper
from shopper.connectors.seller.tesco.common import TESCO
from shopper.models.product import BrandProduct, Brand, BrandProductCategory, CommonProductCategory, SellerProduct, \
    Seller, ProductUnit, get_product_amount_and_unit

__author__ = 'peterd'

import argparse
import os
import requests
from datetime import date
import subprocess
import logging

from itrash.itrash_settings import ITRASH_DATA_ROOT

parser = argparse.ArgumentParser(description='BILLA Clever price scrapper utility')
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')

args = parser.parse_args()


log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)

logger.info('Scrapping product prices...')

price_list_id = 8
price_list_id_file = os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'prices', 'price_list.id')
if os.path.exists(price_list_id_file):
    with open(price_list_id_file, 'r') as f:
        price_list_id = int(f.read())


price_list_url = 'https://www.clevervyrobky.sk/files/aktualny-cennik-porovnavanych-vyrobkov-%d.pdf' % price_list_id

try:
    r = requests.get(price_list_url, stream=True)

    if r.status_code == 200:
        if r.content.find('html') != -1:
            logger.info('Price list with id = %d is not read yet, bailing out' % price_list_id)
            sys.exit(0)

        if not os.path.exists(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'prices')):
            os.makedirs(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'prices'))
        price_list_src_file = os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'prices',
                                  os.path.basename(price_list_url))

        with open(price_list_src_file, 'wb') as f:
            for chunk in r:
                f.write(chunk)

        price_list_dst_file = os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'prices',
                                  'price-list-%s.pdf' % date.today().strftime('%m%d%Y'))
        os.rename(price_list_src_file, price_list_dst_file)

        subprocess.call(['pdftotext', price_list_dst_file])
        price_list_txt_file = price_list_dst_file.replace('pdf', 'txt')

        product_to_price_map = dict()

        with open(price_list_txt_file, 'r') as f:
            product_name = None

            ln_num = 1
            for ln in f:
                try:
                    if ln.find('CLEVER') != -1 or ln.find('CV') != -1:
                        product_name = ln.replace('\n', '').strip()
                    elif product_name is not None and len(ln) > 0 and ln[0].isdigit():
                        price = Money(Decimal(ln.replace('\n', '').replace(',', '.').replace('*', '').strip()),
                                                                           EUR)
                        product_to_price_map[product_name.decode('utf-8')] = price
                        product_name = None
                except Exception, e:
                    logger.exception('failed to process clever price list line %d, content: %s, reason: %s' %
                                     (ln_num, ln, e))
                ln_num += 1

        for product_name, price in product_to_price_map.iteritems():
            try:
                brand_product = BrandProduct.objects.get(name=product_name)
            except ObjectDoesNotExist:
                clever = Brand.objects.get(name='Clever')
                brand_product_category_id = BrandProductCategory.objects.get(name='Unknown').id
                common_product_category_id = CommonProductCategory.objects.get(ascii_name='ostatne').id
                product_unit_id = ProductUnit.objects.all()[0].id

                brand_product = BrandProduct(brand=clever,
                                             brand_product_category_id=brand_product_category_id,
                                             common_product_category_id=common_product_category_id,
                                             ascii_name=CharMapper.cp1250_to_ascii(product_name).lower(),
                                             name=product_name,
                                             ean_pc='',         # can't be None
                                             ean_pc_int=None,
                                             ean_crt=None,
                                             ean_crt_int=None,
                                             unit_id=product_unit_id,
                                             amount_input_type='pc',
                                             amount_input_base=1)
                brand_product.save()

            # update product unit
            product_amount, product_unit_sym = get_product_amount_and_unit(brand_product.ascii_name)

            try:
                product_unit = ProductUnit.get_product_unit_by_name(product_unit_sym)
            except ProductUnitDoesNotExist:
                logger.warning('Failed to get product unit for product "%s", '
                               'using product unit sym "%s", will use kg as unit' % (brand_product.ascii_name,
                                                                                     product_unit_sym))
                product_unit = ProductUnit.get_product_unit_by_name('kg')

            if brand_product.unit_id != product_unit.id:
                brand_product.unit_id = product_unit.id
                brand_product.save(update_fields=['unit_id'])

            try:
                seller_product = SellerProduct.objects.get(brand_product_id=brand_product.id)
                seller_product.update(product_name=brand_product.name,
                                      product_ascii_name=brand_product.ascii_name,
                                      price_pkg_with_vat=price)
            except ObjectDoesNotExist:
                seller = Seller.objects.get(name='Billa')
                seller_product = seller.create_seller_product_using_brand_product_id_and_ref_seller_id(brand_product.id,
                                        TESCO.id,
                                        price_pkg_with_vat=price)

            logger.info('price of "%s" is now %s' % (seller_product.name, price))

        os.remove(price_list_dst_file)

        with open(price_list_id_file, 'w') as f:
            f.write(str(price_list_id + 1))

except Exception, e:
    logger.exception('failed to process price list %s, reason: %s' % (price_list_url, e))