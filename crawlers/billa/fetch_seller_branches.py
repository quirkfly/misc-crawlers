# -*- coding: utf-8 -*-

import argparse
import logging
import re
import simplejson
import requests

import django
from shopper.models.location import Location
from shopper.models.product import SellerBranch, Seller

django.setup()

from django.core.exceptions import ObjectDoesNotExist
from shopper.exception import ProductUnitDoesNotExist

RE_BRANCHES = re.compile(u'\((.*?)\);')
RE_BRANCH_INFO = re.compile(u'p:(?P<phone>.*?)(\|t:(?P<opening_hours>.*?))*$')
RE_OPENING_HOURS_MON_SAT_SUN = re.compile(u'Po - So:\s*(?P<mon_sat>.*?)<br>(Ne:\s*)*(?P<sun>.*?)$')
RE_OPENING_HOURS_MON_FRI_SAT_SUN = re.compile(u'Po - Pi: (?P<mon_fri>.*?)(,\s*|<br>)So: (?P<sat>.*?)<br>Ne:\s*(?P<sun>.*?)$')

parser = argparse.ArgumentParser(description='GVP price reader')
parser.add_argument('-l', '--loglevel',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set the logging level')
parser.add_argument('-f', '--logfile',
                    help='File where the logging will be written. If not specified logging will go to console.')

args = parser.parse_args()

if args.loglevel:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=args.loglevel)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=args.loglevel)
else:
    if args.logfile is not None:
        logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                            level=logging.INFO)
    else:
        logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

try:
    billa = Seller.objects.get(name='Billa')

    response = requests.get('https://service.sk.rewe.co.at/filialservice/filialjson.asmx/getfilialenjson?shopcd=%22SK'
                            '%22&client=gme-rewedigitalgmbh&channelriag.at_sk-billa')
    for json_branch in simplejson.loads(RE_BRANCHES.search(response.content).group(1))['d'].itervalues():

        logger.info('parsing %s' % json_branch)

        seller_branch = None

        opening_hours_weekdays = None
        opening_hours_saturday = None
        opening_hours_sunday = None

        for branch_info in json_branch['jsonPayload']:
            if type(branch_info) in (str, unicode):
                m = RE_BRANCH_INFO.search(branch_info)
                if m:
                    phone = m.groupdict()['phone']
                    if m.groupdict().has_key('opening_hours') and m.groupdict()['opening_hours']:
                        opening_hours = m.groupdict()['opening_hours']

                        m = RE_OPENING_HOURS_MON_SAT_SUN.search(opening_hours)
                        if m:
                            opening_hours_mon_sat = m.groupdict()['mon_sat'].replace('<br>', '')
                            opening_hours_sun = m.groupdict()['sun'].replace('<br>', '')

                            opening_hours_weekdays = opening_hours_mon_sat
                            opening_hours_saturday = opening_hours_mon_sat
                            opening_hours_sunday = opening_hours_sun
                        else:
                            m = RE_OPENING_HOURS_MON_FRI_SAT_SUN.search(opening_hours)
                            if m:
                                opening_hours_mon_fri = m.groupdict()['mon_fri'].replace('<br>', '')
                                opening_hours_sat = m.groupdict()['sat'].replace('<br>', '')
                                opening_hours_sun = m.groupdict()['sun'].replace('<br>', '')

                                opening_hours_weekdays = opening_hours_mon_fri
                                opening_hours_saturday = opening_hours_sat
                                opening_hours_sunday = opening_hours_sun

                    if phone or opening_hours_weekdays and opening_hours_saturday and opening_hours_sunday:
                        seller_branch_address = u'%s, %s' % (json_branch['address1'], json_branch['city'])
                        location = Location.get_or_create_location_from_string(seller_branch_address)
                        location.lat = json_branch['latitude']
                        location.lng = json_branch['longitude']
                        location.save(update_fields=['lat', 'lng'])

                        seller_branch, created = SellerBranch.get_or_create_from_location(billa.id, location)
                        seller_branch_update_fields = []
                        if seller_branch.opening_hours_weekdays != opening_hours_weekdays:
                            seller_branch.opening_hours_weekdays = opening_hours_weekdays
                            seller_branch_update_fields.append('opening_hours_weekdays')
                        if seller_branch.opening_hours_saturday != opening_hours_saturday:
                            seller_branch.opening_hours_saturday = opening_hours_saturday
                            seller_branch_update_fields.append('opening_hours_saturday')
                        if seller_branch.opening_hours_sunday != opening_hours_sunday:
                            seller_branch.opening_hours_sunday = opening_hours_sunday
                            seller_branch_update_fields.append('opening_hours_sunday')
                        if seller_branch.phone != phone:
                            seller_branch.phone = phone
                            seller_branch_update_fields.append('phone')
                        if len(seller_branch_update_fields) > 0:
                            seller_branch.save(update_fields=seller_branch_update_fields)

                            logger.info('Updated seller branch %s' % seller_branch)

        if seller_branch is None:
            logger.warn('missing seller branch for %s' % json_branch)

except Exception, e:
    logger.exception('failed to fetch billa branches, reason: %s' % e)


