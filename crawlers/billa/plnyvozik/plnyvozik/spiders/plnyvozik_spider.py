import logging
import re
from scrapy import Selector

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shopper.crawlers.billa.plnyvozik.plnyvozik.items import ProductItem

RE_PRODUCT_ID = re.compile('(\d+)')

class PlnyVozikSpider(CrawlSpider):
    name = 'plnyvozik'
    base_url = 'https://www.plnyvozik.sk'
    allowed_domains = ['www.plnyvozik.sk']
    start_urls = [
        'https://www.plnyvozik.sk'
    ]


    rules = (Rule(LinkExtractor(allow=(r'/?p=sortiment.*'),),
                                callback='parse_product',
                                follow=True),
             )


    def parse_product(self, response):
        """Returns ProductItem instance."""

        hxs = Selector(response)

        try:
            for product_box in hxs.xpath('//div[@class="produktBox"]'):
                price_int = product_box.xpath('span[@class="priceB color0"]/text()').extract()[0] if \
                    product_box.xpath('span[@class="priceB color0"]/text()') else \
                    product_box.xpath('span[@class="priceB color0 sml"]/text()').extract()[0]
                price_decimal = product_box.xpath('span[@class="priceS color0"]/text()').extract()[0] if \
                    product_box.xpath('span[@class="priceS color0"]/text()') else \
                    product_box.xpath('span[@class="priceS color0 sml"]/text()').extract()[0]
                price = '%s.%s' % (price_int, price_decimal)
                product_item = ProductItem()
                product_item['id'] = re.search(RE_PRODUCT_ID, product_box.xpath('img/@src').extract()[0]).group(0)
                product_item['price'] = price

                yield product_item

        except Exception, e:
            logging.exception('Failed to parse product from url %s, reason: %s' % (response.url, e))