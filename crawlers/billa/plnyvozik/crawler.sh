#!/bin/bash -x

# main script that orchestrates the crawling process
#

SKIP_CRAWLING=0
USE_HTTP_PROXY=1
LOG_DIR=/var/log/itrash
DATA_DIR=/var/data/itrash

MAX_NUM_LOG_FILES=6
MAX_NUM_WRITER_FILES=3
MAX_NUM_DATA_FILES=3
MIN_NUM_SCRAPED_ITEMS=100

function usage {
	echo "common seller crawler and db writer"
	echo "`basename $0` [-n] [-l <log dir>] [-d <data dir>] <seller> <seller_branch_id> [<crawler arg>] [<num crawler tries>] [<crawler retry sleep>]"
    echo "n - if supplied a http proxy will not be used (default is to use it)"
    echo "s - skip crawling phase (default is not to skip it)"
	echo "log dir - defaults to /var/log/hmss"
	echo "data dir - defaults to /var/data/hmss"
	echo "num crawler tries - number of times crawler is retried with different proxy when failed (defaults to 20)"
	echo "crawler retry sleep - number of seconds to wait between crawler retries (defaults to 5)"
	exit 1
}

function rotate_files {
	FILE_DIR=$1
	FILE_PREFIX=$2
	MAX_NUM_FILES=$3

	NUM_FILES=`ls $FILE_DIR/$FILE_PREFIX* | wc -l`
	if [ $NUM_FILES -gt $MAX_NUM_FILES ]
	then 
		NUM_FILES=`expr $NUM_FILES - 1`
		rm $FILE_DIR/$FILE_PREFIX.$NUM_FILES
	fi

	while [ $NUM_FILES -gt 0 ]
	do
		CUR_FILE_SUFFIX=`expr $NUM_FILES - 1`
		NEXT_FILE_SUFFIX=`expr $CUR_FILE_SUFFIX + 1`
		if [ $CUR_FILE_SUFFIX -gt 0 ]
		then
			mv $FILE_DIR/$FILE_PREFIX.$CUR_FILE_SUFFIX $FILE_DIR/$FILE_PREFIX.$NEXT_FILE_SUFFIX
		else
			mv $FILE_DIR/$FILE_PREFIX $FILE_DIR/$FILE_PREFIX.1
		fi
		NUM_FILES=`expr $NUM_FILES - 1`
	done
}

while getopts snl:d: FLAG; do
	case $FLAG in
	s)
		SKIP_CRAWLING=1;
		;;
	n)
		USE_HTTP_PROXY=0;
		;;
    l)
		LOG_DIR=$OPTARG;
     	;; 
    d)
		DATA_DIR=$OPTARG;
      	;;
    ?)
     	usage;
      	;;
  esac
done

shift $(( OPTIND - 1 ));

test $# -lt 1 && usage 

SELLER=$1
SELLER_BRANCH_ID=$2
CRAWLER_ARGS=$3
NUM_CRAWLER_TRIES=$4
test "x$NUM_CRAWLER_TRIES" == "x" && NUM_CRAWLER_TRIES=20
CRAWLER_RETRY_SLEEP=$5
test "x$CRAWLER_RETRY_SLEEP" == "x" && CRAWLER_RETRY_SLEEP=5

cd $ITRASH_HOME_DIR/shopper/crawlers/billa/plnyvozik

if [ "x$SELLER_BRANCH_ID" == "x" ]
then
	SELLER_BRANCH_ID=""
else
	SELLER_BRANCH_ID="-$SELLER_BRANCH_ID"
fi


#
# rotate files
#

# rotate crawler files
test -f $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log && rotate_files $LOG_DIR ${SELLER}${SELLER_BRANCH_ID}_crawler.log $MAX_NUM_LOG_FILES

# rotate writer files
test -f $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log && rotate_files $LOG_DIR ${SELLER}${SELLER_BRANCH_ID}_writer.log $MAX_NUM_WRITER_FILES

# rotate data files
test -f $DATA_DIR/${SELLER}${SELLER_BRANCH_ID}_products.jsonlines && rotate_files $DATA_DIR ${SELLER}${SELLER_BRANCH_ID}_products.jsonlines $MAX_NUM_DATA_FILES

#
# launch crawler
#

while [ $NUM_CRAWLER_TRIES -gt 0 -a $SKIP_CRAWLING -eq 0 ]
do

	if [ $USE_HTTP_PROXY -eq 1 ]
	then
		HTTP_PROXY=`DJANGO_SETTINGS_MODULE=hmss_settings PYTHONPATH=$HMSS_HOME_DIR python $HMSS_HOME_DIR/ippool/host_mgr.py`

		echo "http_proxy=$HTTP_PROXY"
	else
		HTTP_PROXY=	
	fi

	if [ "x$CRAWLER_ARGS" == "x" ]
	then
		http_proxy=$HTTP_PROXY DJANGO_SETTINGS_MODULE=hmss_settings PYTHONPATH=$PYTHONPATH:$ITRASH_HOME_DIR:$HMSS_HOME_DIR scrapy crawl $SELLER -o $DATA_DIR/${SELLER}${SELLER_BRANCH_ID}_products.jsonlines -t jsonlines --logfile $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log 2>$LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log
	else
		http_proxy=$HTTP_PROXY DJANGO_SETTINGS_MODULE=hmss_settings PYTHONPATH=$PYTHONPATH:$ITRASH_HOME_DIR:$HMSS_HOME_DIR scrapy crawl $SELLER $CRAWLER_ARGS -o $DATA_DIR/${SELLER}${SELLER_BRANCH_ID}_products.jsonlines -t jsonlines --logfile $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log 2>$LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log
	fi

	NUM_SCRAPED_ITEMS=`grep item_scraped_count $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log | cut -f2 -d: | tr -d ' ,'`
	test "x$NUM_SCRAPED_ITEMS" == "x" && NUM_SCRAPED_ITEMS=0

	if [ $USE_HTTP_PROXY -eq 1 -a $NUM_SCRAPED_ITEMS -lt $MIN_NUM_SCRAPED_ITEMS ]
	then
        # mark proxy as dead
        DJANGO_SETTINGS_MODULE=hmss_settings PYTHONPATH=$HMSS_HOME_DIR python $HMSS_HOME_DIR/ippool/mark_host_dead.py $HTTP_PROXY

		NUM_CRAWLER_TRIES=`expr $NUM_CRAWLER_TRIES - 1`

		echo "$HTTP_PROXY marked dead, wil try to choose another one in $CRAWLER_RETRY_SLEEP seconds (num retries left = $NUM_CRAWLER_TRIES) ..."

		sleep $CRAWLER_RETRY_SLEEP
	else
		break
	fi

done

if [ $SKIP_CRAWLING -eq 0 ]
then
    if [ $NUM_SCRAPED_ITEMS -lt $MIN_NUM_SCRAPED_ITEMS ]
    then
        cd $ITRASH_HOME_DIR
        grep -i -A100 "dumping scrapy stats" $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.log > $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.stats
        python utils/mailer.py --from_addr ${SELLER}_crawler@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to crawl $SELLER (seller_branch_id=$SELLER_BRANCH_ID)." $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.stats
        rm $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_crawler.stats
        exit 1
    fi
fi

#
# write scrapped items do db
#

cd $ITRASH_HOME_DIR

DJANGO_SETTINGS_MODULE=itrash.settings PYTHONPATH=`pwd`:.. python shopper/writers/billa/plnyvozik/${SELLER}_writer.py --logfile $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log $DATA_DIR/${SELLER}${SELLER_BRANCH_ID}_products.jsonlines 2>$LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log
RV=$?

if [ ! $RV -eq 0 ]
then
	tail -100 $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log > $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
	python utils/mailer.py --from_addr ${SELLER}_writer@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to write $SELLER (seller_branch_id=$SELLER_BRANCH_ID) products to database." $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
	rm $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
	exit 2
fi

#
# check writer log for errors
#

NUM_WRITE_ERRORS=`grep ERROR $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log | wc -l`
if [ $NUM_WRITE_ERRORS -gt 0 ]
then
	grep ERROR $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.log > $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
	python utils/mailer.py --from_addr ${SELLER}_writer@20deka.com --to_addr peter.dermek@20deka.com --subject "Failed to write $NUM_WRITE_ERRORS $SELLER (seller_branch_id=$SELLER_BRANCH_ID)  products to database." $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
	rm $LOG_DIR/${SELLER}${SELLER_BRANCH_ID}_writer.err
	exit 2
fi
