from bs4 import BeautifulSoup
import os

import requests

from itrash.itrash_settings import ITRASH_DATA_ROOT

__author__ = 'peterd'


for product_category in ('mliecne-vyrobky', 'ovocie-a-zelenina', 'zakladne-potraviny'):
    response = requests.get('https://www.clevervyrobky.sk/sortiment-detail/%s' % product_category)
    soup = BeautifulSoup(response.content)

    for div_product in soup.findAll('div', {'class': 'produkt'}):
        img_src = div_product.find('img')['src']

        r = requests.get(img_src, stream=True)
        if r.status_code == 200:
            if not os.path.exists(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa',
                                               'images', 'full', product_category)):
                os.makedirs(os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'images', 'full', product_category))
            image_file = os.path.join(ITRASH_DATA_ROOT, 'sellers', 'billa', 'images', 'full', product_category,
                                      os.path.basename(img_src))

            with open(image_file, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
