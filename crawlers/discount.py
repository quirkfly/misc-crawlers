import hashlib
from uuid import uuid4
from django.core.exceptions import ObjectDoesNotExist
from djmoney.models.fields import MoneyPatched

from shopper.exception import SellerProductDoesNotExist
from shopper.utils.product.discount_linker import link_seller_product_discount, do_link_seller_product_discount, \
    dump_brand_product_candidates

__author__ = 'peterd'

import os
from datetime import datetime, date
from moneyed import Money
import requests

from itrash.settings import DATA_ROOT, MEDIA_ROOT
from infra.lexical import CharMapper
from shopper.models.product import SellerProductDiscount, SellerProduct, seller_product_discount_get_full_image_path, \
    disect_product_name, Brand, BrandProduct, BrandProductCategory, ProductUnit, SellerProductCategory


class ImageDoesNotExist(RuntimeError):
    pass

class ImageLink(object):
    def __init__(self, img, base_url=None, dst_path=None):
        if base_url is not None:
            self.src = '%s%s' % (base_url, img['src'])
        else:
            self.src = img['src']
        try:
            self.height = int(img['height'])
        except KeyError:
            self.height = 300
        try:
            self.width = int(img['width'])
        except KeyError:
            self.width = 150

        self.image_file = None

        self.dst_path = dst_path
        if self.dst_path is None:
            if not os.path.exists(os.path.join(DATA_ROOT, 'media', 'images', 'full')):
                os.makedirs(os.path.join(DATA_ROOT, 'media', 'images', 'full'))
            self.dst_path = os.path.join(DATA_ROOT, 'media', 'images', 'full')


    @property
    def rel_image_path(self):
        return os.path.join('images', 'full', os.path.basename(self.image_file)) if self.image_file else None


    @property
    def file_name(self):
        return os.path.basename(self.image_file) if self.image_file else None


    def fetch(self, session=None):
        """Fetches image from a remote url."""

        if session:
            r = session.get(self.src, stream=True)
        else:
            r = requests.get(self.src, stream=True)

        if r.status_code == 200:

            file_name, file_ext = os.path.splitext(self.src)
            if file_ext.find('?') != -1:
                file_ext = file_ext.split('?')[0]
            self.image_file = os.path.join(self.dst_path, '%s%s' % (hashlib.md5(self.src).hexdigest(), file_ext)).replace('ashx', 'jpg')

            with open(self.image_file, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        elif r.status_code == 404:
            raise ImageDoesNotExist
        else:
            raise RuntimeError('Failed to fetch image %s, status code: %d' % (self.src, r.status_code))

    def __str__(self):
        return unicode(self).encode('utf-8')


    def __unicode__(self):
        return u'ImageLink = { src = %s, height = %d, width = %d }' % (self.src, self.height, self.width)


    __repr__ = __str__



class InvalidDiscount(Exception):
    """Raised when an invalid  value has been encountered, mostly from parameters."""

    def __init__(self, message):
        super(InvalidDiscount, self).__init__(message)


class DiscountAlreadyExpired(Exception):
    """Raised when an invalid  value has been encountered, mostly from parameters."""

    def __init__(self, message):
        super(DiscountAlreadyExpired, self).__init__(message)



class Discount(object):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller, description,
                 new_price, old_price, currency, date_format, unit=None, price_unit=None, discount=0,
                 multipack=None, ean_pc=None):
        self.image_file = image_file

        if url in [None, '']:
            raise InvalidDiscount('[%s] Missing product url' % url)
        self.url = url

        if name in [None, '']:
            raise InvalidDiscount('[%s] Missing product name' % self.url)
        self.name = sanitize_data(name)
        self.ascii_name = CharMapper.cp1250_to_ascii(name).lower()

        self.brand = disect_product_name(self.name)[6]
        if self.brand is None:
            self.brand = Brand.get_default_brand()

        if type(valid_from) == date:
            self.valid_from = valid_from
        else:
            try:
                self.valid_from = datetime.strptime(valid_from, date_format).date()
            except Exception, e:
                raise InvalidDiscount('[%s (%s)]: Valid from is invalid, expected a date string in '
                                      'format "%s", got "%s"' % (self.url, self.name, date_format.replace('%', '%%'),
                                                                 valid_from))

        if type(valid_until) == date:
            self.valid_until = valid_until
        else:
            try:
                self.valid_until = datetime.strptime(valid_until, date_format).date()
            except Exception, e:
                raise InvalidDiscount('[%s (%s)]: Valid until is invalid, expected a date string in '
                                      'format "%s", got "%s"' % (self.url, self.name, date_format.replace('%', '%%'),
                                                                 valid_until))

        if self.valid_until < date.today():
            raise DiscountAlreadyExpired('[%s (%s)]: Valid until is invalid, expected a date greater than today'
                                  ', got "%s"' % (self.url, self.name, valid_until))


        if self.valid_until < self.valid_from:
            # correct some level of misspelling
            if self.valid_from.year > self.valid_until.year:
                self.valid_from = date(self.valid_until.year, self.valid_from.month, self.valid_from.day)
            if self.valid_until < self.valid_from:
                raise InvalidDiscount('[%s (%s)]: Valid until "%s" is invalid, must be greater than valid from "%s"'
                                      ', got "%s"' % (self.url, self.name, valid_until, valid_from, valid_until))


        self.seller = seller
        self.description = description

        if new_price in [None, '']:
            raise InvalidDiscount('[%s (%s)] Missing new product price' % (self.url, self.name))

        if type(new_price) == MoneyPatched:
            self.new_price = new_price
        else:
            try:
                self.new_price = Money(new_price, currency)
            except Exception, e:
                raise InvalidDiscount('[%s (%s)] New product price is invalid, expected a decimal value, '
                                      'got "%s" (error: %s)' % (self.url, self.name, new_price, e))

        # old price might be None
        if type(old_price) == MoneyPatched:
            self.old_price = old_price
        else:
            try:
                self.old_price = Money(old_price, currency)
            except Exception:
                self.old_price = Money(0.0, currency)

        self.discount = discount

        self.unit = unit

        # price unit might be None
        try:
            self.price_unit = Money(price_unit, currency)
        except Exception:
            self.price_unit = Money(0.0, currency)

        self.currency = currency

        self.has_candidates = False

        self.multipack = multipack

        self.ean_pc = ean_pc


    def save(self, create_seller_product=False, logger=None, use_fuzzy_linker=True):
        """Saves this discount into database and returns linked seller product if any, None otherwise."""

        seller_product_discount = SellerProductDiscount(
            image_full=seller_product_discount_get_full_image_path(self, self.image_file) if self.image_file else None,
            url=self.url,
            name=self.name,
            ascii_name=self.ascii_name,
            ean_pc=self.ean_pc,
            brand=self.brand,
            discount_valid_from=self.valid_from,
            discount_valid_until=self.valid_until,
            seller_id=self.seller.id,
            description=self.description,
            price_with_vat=self.new_price,
            price_with_vat_old=self.old_price,
            discount_amount=self.discount,
            unit=self.unit,
            price_unit_with_vat=self.price_unit,
            is_expired=False,
            multipack=self.multipack)
        seller_product_discount.save()

        seller_product = None

        linked_discounts = SellerProductDiscount.objects.filter(
            name=self.name).exclude(seller_product_id=None).exclude(seller_id=self.seller.id)
        if linked_discounts.exists():
            for linked_discount in linked_discounts:
                try:
                    a_seller_product = SellerProduct.get_seller_product_by_id(linked_discount.seller_product_id)
                    brand_product = a_seller_product.brand_product
                    seller_product = do_link_seller_product_discount(brand_product, seller_product_discount,
                                                                     skip_amount_check=True)
                    break
                except SellerProductDoesNotExist:
                    if use_fuzzy_linker:
                        seller_product = link_seller_product_discount(seller_product_discount, logger=logger)

                        self.has_candidates = seller_product_discount.has_candidates

                        if seller_product:
                            break

            if not seller_product:
                dump_brand_product_candidates(seller_product_discount, logger=logger)

        elif use_fuzzy_linker:
            seller_product = link_seller_product_discount(seller_product_discount, logger=logger)

            self.has_candidates = seller_product_discount.has_candidates
        else:
            dump_brand_product_candidates(seller_product_discount, logger=logger)

        return seller_product


    def save_to_common_product_category(self, base_common_product_category, detailed_common_product_category,
                                        logger):
        if self.image_file is not None:
            image_full_rel_path = seller_product_discount_get_full_image_path(self, self.image_file)
            image_full_abs_path = os.path.join(MEDIA_ROOT, image_full_rel_path)
        else:
            image_full_rel_path = None
            image_full_abs_path = None

        if not BrandProduct.objects.filter(name=self.name).exists():
            brand = Brand.get_default_brand()
            brand_product_category = BrandProductCategory.objects.get(name='Unknown')
            product_unit = ProductUnit.get_product_unit_by_name('ks')
            unknown_seller_product_category = SellerProductCategory.objects.get(seller_id=self.seller.id,
                                                                                name='Unknown')

            brand_product = BrandProduct.create_brand_product(brand, brand_product_category,
                                                              base_common_product_category.id,
                                                              detailed_common_product_category.id,
                                                              self.name,
                                                              self.ascii_name,
                                                              'x' * 13, 'x' * 13, 'x' * 13, 'x' * 13,
                                                              product_unit.id,
                                                              image_full_abs_path,
                                                              'pc', 1,
                                                              is_clonnable=False)
        else:
            brand_product = BrandProduct.objects.filter(name=self.name)[0]

        try:
            seller_product = SellerProduct.objects.get(brand_product_id=brand_product.id, seller_id=self.seller.id)
            seller_product.discount_valid_from = self.valid_from
            seller_product.discount_valid_until = self.valid_until
            seller_product.price_pkg_with_vat_old = self.old_price
            seller_product.price_pkg_with_vat = self.new_price
            seller_product.save(update_fields=['discount_valid_from', 'discount_valid_until', 'price_pkg_with_vat_old',
                                               'price_pkg_with_vat'])
        except ObjectDoesNotExist:
            seller_product = SellerProduct.create_seller_product(product_name=self.name,
                                                                 product_ascii_name=self.ascii_name,
                                                                 ean_pc='x' * 13,
                                                                 ean_pc_int='x' * 13,
                                                                 ean_crt='x' * 13,
                                                                 ean_crt_int='x' * 13,
                                                                 product_unit_id=product_unit.id,
                                                                 brand_product_id=brand_product.id,
                                                                 seller_id=self.seller.id,
                                                                 seller_product_category_id=unknown_seller_product_category.id,
                                                                 discount_valid_from=self.valid_from,
                                                                 discount_valid_until=self.valid_until,
                                                                 price_pkg_with_vat_old=self.old_price,
                                                                 price_pkg_with_vat=self.new_price)

        if not SellerProductDiscount.discount_exists(self, match_by_url=True):
            seller_product_discount = SellerProductDiscount(
                image_full=image_full_rel_path,
                url=self.url,
                name=self.name,
                ascii_name=self.ascii_name,
                brand=self.brand,
                discount_valid_from=self.valid_from,
                discount_valid_until=self.valid_until,
                seller_id=self.seller.id,
                seller_product=seller_product,
                description=self.description,
                price_with_vat=self.new_price,
                price_with_vat_old=self.old_price,
                discount_amount=self.discount,
                unit=self.unit,
                price_unit_with_vat=self.price_unit,
                is_expired=False,
                multipack=self.multipack)
            seller_product_discount.save()
        else:
            seller_product_discount = SellerProductDiscount.objects.filter(seller=self.seller,
                                                        url=self.url,
                                                        discount_valid_from=self.valid_from,
                                                        discount_valid_until=self.valid_until,
                                                        price_with_vat=self.new_price,
                                                        price_with_vat_old=self.old_price)[0]
            seller_product_discount.seller_product_id = seller_product.id
            seller_product_discount.save(update_fields = ['seller_product_id'])

        return seller_product


    def __str__(self):
        return unicode(self).encode('utf-8')


    def __unicode__(self):
        return u'Discount = { url = %s, name = %s, valid_from = %s, valid_until = %s, ' \
               u'seller = %s, description = %s, new_price = %s %s, old_price = %s %s}' %\
               (self.url, self.name, self.valid_from, self.valid_until, self.seller.name, self.description,
                self.new_price.amount, self.currency, self.old_price.amount, self.currency)

    __repr__ = __str__


def sanitize_data(data):
    data = data.replace(u'\u0301', '')

    return data