# -*- coding: utf-8 -*-

import logging
from bs4 import BeautifulSoup

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from shopper.crawlers.drmax.drmax.items import ProductItem

DRMAX_URL = 'https://www.drmax.sk'

class DrMaxSpider(CrawlSpider):
    name = 'drmax'
    allowed_domains = ['www.drmax.sk']
    start_urls = [DRMAX_URL]

    rules = (Rule(LinkExtractor(allow=['.*?']), callback='parse_items', follow=True),)


    def parse_items(self, response):
        soup = BeautifulSoup(response.body)

        product_div_list = soup.find('div', {'class': 'productList'})

        if product_div_list is not None:
            for product_div in product_div_list.findAll('div', {'class': 'product'}):
                try:
                    product = Product(soup, product_div)
                    product_item = ProductItem()
                    product.to_item(product_item)

                    yield product_item

                except Exception, e:
                    logging.exception('Failed to parse product li "%s", reason: %s' % (product_div, e))



class Product(object):
    def __init__(self, soup, product_div):
        self.name = u'%s %s' % (product_div.find('a', {'class': 'ecommerce'})['title'],
                                product_div.find('span', {'class': 'fs-small'}).text)
        self.image_urls = ['%s%s' % (DRMAX_URL, product_div.find('img', {'class': 'image'})['src'])]
        self.url = product_div.find('a', {'class': 'ecommerce'})['href']
        self.path = soup.find('div', {'itemscope': 'itemscope'}).text.strip('\n').replace(u'Nachádzate sa: ', '')

        self.price_old = None
        try:
            self.price_old = product_div.find('div', {'class': 'price'}).find('span', {'class': 'old'}).text
        except Exception, e:
            logging.warning('Failed to parse old price, reason: %s', e)

        self.price_new = None
        try:
            self.price_new = product_div.find('div', {'class': 'price'}).find('span', {'class': 'bold'}).text
        except Exception, e:
            logging.warning('Failed to parse new price, reason: %s', e)

        self.availability = None
        try:
            self.availability = product_div.find('strong', {'class': 'green'}).text
        except Exception, e:
            logging.warning('Failed to parse availability, reason: %s', e)



    def to_item(self, product_item):
        product_item['name'] = self.name
        product_item['image_urls'] = self.image_urls
        product_item['url'] = self.url
        product_item['path'] = self.path
        product_item['price_old'] = self.price_old
        product_item['price_new'] = self.price_new
        product_item['availability'] = self.availability

        return product_item


    def __unicode__(self):
        return self.name