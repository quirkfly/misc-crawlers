# -*- coding: utf-8 -*-

import re
from decimal import Decimal
import logging

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector

from shopper.crawlers.metro.metro.items import ProductItem
from shopper.crawlers.utils.extractor import extract_data_item, extract_data_list

RE_PRICE = re.compile(r'(\d+,\d+)')
RE_PRODUCT_EAN = re.compile(u'^EAN:\s*(\d+)')
RE_PRODUCT_EAN_INTERNAL = re.compile(u'^EAN\s\(.*\):\s*(\d+)')
RE_PRODUCT_CODE = re.compile(u'^K\xf3d tovaru:\s(.*)')
RE_COUNTRY_OF_ORIGIN = re.compile(u'^Krajina p\xf4vodu:\s(.*)')
RE_NUM_PIECES_PACKAGE = re.compile(u'^Po\u010det ks v balen\xed:\s*(\d+)')

STORE_ID_MAP = {
    'nitra': 'AE219983-6BA4-490D-BFF2-35E51E6BBBDB',
    'devinska-nova-ves': '889E6DE7-C58F-4E00-B557-0A28CF32EDB5',
    'ivanka-pri-dunaji': 'AE04A54D-1433-493B-A809-27AC5B3740A4',
    'kosice': '2C494D9A-3CC1-45CC-9B19-5E3D47FF4B87',
    'zvolen': '286466FE-9930-4815-8D5D-29B161CD349D',
    'zilina': '9340E466-17AD-4270-AC02-0F32D1ED4947'
}

STORE_ID_NUM_MAP = {
    'nitra': 19,
    'devinska-nova-ves': 22,
    'ivanka-pri-dunaji': 18,
    'kosice': 21,
    'zvolen': 20,
    'zilina': 23
}


class METROSpider(CrawlSpider):
    name = 'metro'
    allowed_domains = ['sortiment.metro.sk']
    start_urls = ['https://sortiment.metro.sk/sk/']

    rules = (Rule(LinkExtractor(deny=['tel', 'pricefilter', 'lst', 'inactionforce'], allow=['.*/\d+c']), follow=True),
             Rule(LinkExtractor(allow=['.*/\d+p']), callback='parse_product_item'))

    def __init__(self, store_id='ivanka-pri-dunaji', *args, **kwargs):
        super(METROSpider, self).__init__(*args, **kwargs)
        self.store_id = store_id

    def start_requests(self):
        return [FormRequest('http://www.metro.sk',
                            formdata={'__EVENTTARGET': 'pageph_0$UIStoreInfo',
                                      '__EVENTARGUMENT': '{%s}' % STORE_ID_MAP[self.store_id]},
                            callback=self.store_selected)]

    def store_selected(self, response):
        logging.debug('cookies: %s' % response.headers.getlist('Set-Cookie'))

        return Request('https://sortiment.metro.sk/sk/')

    def parse_product_item(self, response):
        hxs = Selector(response)

        errors = []

        try:
            # product path
            product_path = hxs.xpath('//ul[@class="breadcrumb clearfix"]/li/a/text()').extract()
            if product_path is not None and isinstance(product_path, list) and len(product_path) > 1:
                product_path = product_path[1:]
                logging.debug("got product path: %s" % product_path)
            else:
                err = 'failed to get product path'
                errors.append(err)
                logging.error(err)

            #product name
            product_name = extract_data_item(hxs.xpath('//h1[@class="m-t-0 m-b"]/text()'))
            if product_name is not None:
                logging.debug("got product name: %s" % product_name)
            else:
                err = 'failed to get product name'
                errors.append(err)
                logging.error(err)

            try:
                product_code = extract_data_item(hxs.xpath('//span[@class="product-id color-gray-darker"]/span[@class="value"]/text()'))
            except Exception, e:
                err = 'failed to get product code, reason: %s' % e
                errors.append(err)
                logging.error(err)

            is_available = True if len(hxs.xpath('//span[@class="icon-neskladem"]')) == 0 else False

            is_on_promotion = len(hxs.xpath('//span[@class="mic mic-action-sk is-32"]')) > 0

            # promotion_valid_until (optional)
            promotion_valid_until = extract_data_item(hxs.xpath('//div[@class="action-l"]/text()'))
            if promotion_valid_until is not None:
                promotion_valid_until = promotion_valid_until.replace(u'Platnosť akcie do ', '')
                logging.debug("got promotion valid until: %s" % promotion_valid_until)

            is_chilled = len(hxs.xpath('//i[@class="mi mi-sortiment-chilled is-24 color-blue"]')) > 0

            is_frozen = len(hxs.xpath('//i[@class="mi mi-sortiment-frozen is-24 color-primary"]')) > 0

            sold_by_weight = len(hxs.xpath('//i[@class="mi mi-weight-kg is-24"]')) > 0

            # # inclusion_own_brand (optional)
            # incl_own_brand = extract_data_item(hxs.xpath('//div[@class="action-v"]/text()'))
            # if incl_own_brand is not None:
            #     logging.debug("got inclusion own brand", level=log.DEBUG)

            # image urls
            image_urls = extract_data_list(hxs.xpath('//a[@class="thumb-main m-b-md clearfix"]/img/@src'))
            if image_urls is not None and isinstance(image_urls, list) and len(product_path) > 0:
                logging.debug("got image urls: %s" % image_urls)
            else:
                err = 'failed to get image urls'
                errors.append(err)
                logging.error(err)

            # product info
            num_pieces_package = extract_data_item(hxs.xpath('//p[@class="product-units"]/span[@class="value pull-right"]/text()'))
            if num_pieces_package is None:
                err = 'failed to get num pieces / package'
                errors.append(err)
                logging.error(err)
            else:
                logging.debug("got num pieces / package: %s" % num_pieces_package)


            product_ean = extract_data_item(hxs.xpath('//p[@class="product-ean color-gray-dark"]/span[@class="value pull-right"]/text()'))
            if product_ean is None:
                err = 'failed to get EAN'
                errors.append(err)
                logging.error(err)
            else:
                logging.debug("got EAN: %s" % product_ean)

            price_piece = None
            price_piece_with_vat = None
            price_package = None
            price_package_with_vat = None
            price_unit = None
            price_unit_with_vat = None
            price_1kg = None
            price_1kg_with_vat = None

            if sold_by_weight:
                price_1kg = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-unit"]/th/text()'))[0]).group(1).replace(',', '.'))
                if price_1kg:
                    logging.debug("got price 1 kg: %s" % price_1kg)
                else:
                    err = 'failed to get price 1 kg'
                    errors.append(err)
                    logging.error(err)

                price_1kg_with_vat = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-unit"]/th/text()'))[1]).group(1).replace(',', '.'))
                if price_1kg_with_vat:
                    logging.debug("got price 1 kg with vat: %s" % price_1kg_with_vat)
                else:
                    err = 'failed to get price 1 kg with vat'
                    errors.append(err)
                    logging.error(err)
            else:
                price_piece = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-unit"]/th/text()'))[0]).group(1).replace(',', '.'))
                if price_piece:
                    logging.debug("got price piece: %s" % price_piece)
                else:
                    err = 'failed to get price piece'
                    errors.append(err)
                    logging.error(err)

                price_piece_with_vat = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-unit"]/th/text()'))[1]).group(1).replace(',', '.'))
                if price_piece_with_vat:
                    logging.debug("got price piece with vat: %s" % price_piece_with_vat)
                else:
                    err = 'failed to get price piece with vat'
                    errors.append(err)
                    logging.error(err)

                price_package = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-package"]/td/text()'))[1]).group(1).replace(',', '.'))
                if price_package:
                    logging.debug("got price package: %s" % price_package)
                else:
                    err = 'failed to get price package'
                    errors.append(err)
                    logging.error(err)

                price_package_with_vat = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-package"]/td/text()'))[2]).group(1).replace(',', '.'))
                if price_package_with_vat:
                    logging.debug("got price package with vat: %s" % price_package_with_vat)
                else:
                    err = 'failed to get price package with vat'
                    errors.append(err)
                    logging.error(err)

                price_unit = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-volume"]/td/text()'))[1]).group(1).replace(',', '.'))
                if price_unit:
                    logging.debug("got price unit: %s" % price_unit)
                else:
                    err = 'failed to get price unit'
                    errors.append(err)
                    logging.error(err)

                price_unit_with_vat = Decimal(RE_PRICE.search(extract_data_list(hxs.xpath('//tr[@class="price-volume"]/td/text()'))[2]).group(1).replace(',', '.'))
                if price_unit_with_vat:
                    logging.debug("got price unit with vat: %s" % price_unit_with_vat)
                else:
                    err = 'failed to get price unit with vat'
                    errors.append(err)
                    logging.error(err)

            product_info = extract_data_list(hxs.xpath('//div[@class="panel-body"]/div[@class="dontsplit"]'))

            product_item = None

            if len(errors) == 0:
                product_item = ProductItem()
                product_item['id'] = product_code
                product_item['bc'] = product_ean
                product_item['name'] = product_name
                product_item['link'] = response.url
                product_item['path'] = product_path
                product_item['promotion'] = is_on_promotion
                if promotion_valid_until is not None:
                    product_item['promotion_valid_until'] = promotion_valid_until
                else:
                    product_item['promotion_valid_until'] = None
                # if incl_own_brand is not None:
                #     product_item['own_brand'] = True
                # else:
                product_item['own_brand'] = False
                product_item['chilled'] = is_chilled
                product_item['frozen'] = is_frozen
                product_item['image_urls'] = image_urls
                # if country_of_origin is not None:
                #     product_item['country_of_origin'] = country_of_origin
                product_item['num_pieces_package'] = num_pieces_package
                product_item['price_package'] = price_package
                product_item['price_package_with_vat'] = price_package_with_vat
                product_item['price_1kg'] = price_1kg
                product_item['price_1kg_with_vat'] = price_1kg_with_vat
                product_item['price_unit'] = price_unit
                product_item['price_unit_with_vat'] = price_unit_with_vat
                product_item['price_piece'] = price_piece
                product_item['price_piece_with_vat'] = price_piece_with_vat
                product_item['product_info'] = product_info
                product_item['url'] = response.url
                product_item['availability'] = is_available

            return product_item

        except Exception, e:
            logging.exception('failed to parse product item, reason: %s' % e)

            return None