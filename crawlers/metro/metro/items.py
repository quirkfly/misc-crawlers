# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class ProductItem(Item):
    id = Field()
    store_id = Field()
    bc = Field()
    bc_external = Field()
    name = Field()
    link = Field()
    path = Field()
    promotion = Field()
    promotion_valid_until = Field()
    own_brand = Field()
    chilled = Field()
    frozen = Field()
    availability = Field()
    image_urls = Field()
    images = Field()
    country_of_origin = Field()
    num_pieces_package = Field()
    price_package = Field()
    price_package_with_vat = Field()
    price_1kg = Field()
    price_1kg_with_vat = Field()
    price_unit = Field()
    price_unit_with_vat = Field()
    price_piece = Field()
    price_piece_with_vat = Field()
    product_info = Field()
    url = Field()

