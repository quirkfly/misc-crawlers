# Scrapy settings for metro project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'metro'

SPIDER_MODULES = ['metro.spiders']
NEWSPIDER_MODULE = 'metro.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1'

USER_AGENT_LIST = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0) Gecko/16.0 Firefox/16.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
]

HTTP_PROXY = 'http://127.0.0.1:8123'

RETRY_HTTP_CODES = [500, 502, 503, 504, 400, 401, 403, 404, 407, 408, 111]
RETRY_TIMES = 1

DOWNLOADER_MIDDLEWARES = {                          
    'shopper.crawlers.metro.metro.middlewares.RandomUserAgentMiddleware': 400,
    'shopper.crawlers.metro.metro.middlewares.ProxyMiddleware': 410,
    'scrapy.downloadermiddleware.retry.RetryMiddleware': None,
    #'scrapy.downloadermiddleware.httpproxy.HttpProxyMiddleware': None,
}

# delay between 0.5 to 1.5 secs
DOWNLOAD_DELAY = 1


ITEM_PIPELINES = {'scrapy.pipelines.images.ImagesPipeline': 300}

IMAGES_STORE = '/var/data/itrash/media/images'

IMAGES_THUMBS = {
    'small': (60, 60),
    'big': (135, 135),
}