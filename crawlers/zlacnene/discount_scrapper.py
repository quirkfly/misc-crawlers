# -*- coding: utf-8 -*-

#!/usr/bin/env python

from datetime import date
from random import randint

import argparse
import re
from bs4 import BeautifulSoup
import logging
import sys
from decimal import Decimal
import time

import django
from django.utils.translation import ugettext as _

django.setup()

from shopper.crawlers.discount import ImageLink
from shopper.crawlers.zlacnene.discount import ZlacneneDiscount
from shopper.crawlers.discount_scrapper import DiscountScrapper
from shopper.exception import SellerDoesNotExist
from shopper.models.product import Seller, SellerProductDiscount, SellerProduct, SellerProductDiscountTrash

__author__ = 'peterd'

class ZlacneneScrapper(DiscountScrapper):
    BASE_URL = 'http://www.zlacnene.sk'
    RE_NUM_DISCOUNTS = re.compile(r'.*?(\d+).*?')
    RE_PRICE = re.compile(r'(?P<euros>\d+),(?P<cents>\d+)')
    RE_AMOUNT = re.compile(u'(?P<amount>\d*\s*x*\s*\d*,*\d+\s*(g|kg|ml|l|m))')
    RE_PRICE_UNIT = re.compile(u'(?P<amount>\d*\s*x*\s*\d*,*\d+\s*(g|kg|ml|l|m)).*?jednotková cena\s+(?P<price>\d+,*\d*)\s+EUR/(?P<unit>(kg|l|m))')
    RE_PRICE_100 = re.compile(u'(?P<amount>\d*\s*x*\s*\d*,*\d+\s*(g|kg|ml|l|m)).*?(?P<price>\d+,*\d*)\s+[EUR|€]\s+za\s+100\s+(?P<unit>(ml|g)).*?(?P<multipack>.*?ZADARMO)*')
    RE_PRICE_PC = re.compile(u'(?P<amount>\d*\s*x*\s*\d*,*\d+\s*ks).*?(?P<price>\d+,*\d*)\s+[EUR|€]\s+za\s+1\s+(?P<unit>ks)')
    RE_PRICE_DOSE = re.compile(u'(?P<amount>\d*\s*x*\s*\d*,*\d+\s*(g|kg|ml|l|m)).*?(?P<price>\d+,*\d*)\s+[EUR|€]\s+za\s+1\s+(?P<unit>praciu dávku)')

    RE_DISCOUNT_PERIOD = re.compile(r'(?P<valid_from>\d+\.\d+.)\s+-\s+(?P<valid_until>\d+\.\d+.\d+)')
    MAX_DESCRIPTION_SIZE = 256
    MAX_NUM_RETRIES = 10

    def __init__(self, seller, seller_slug, logger, sleep_int=None, dont_torify=False):
        super(ZlacneneScrapper, self).__init__(seller, logger, sleep_int, dont_torify)

        self.seller_slug = seller_slug


    def scrap(self, teaser_only=False):
        """Returns scrapped discount object map. If teaser_only is True scraps only first page."""

        response = self.session.get('%s/akciovy-tovar/%s/' % (ZlacneneScrapper.BASE_URL, self.seller_slug))
        soup = BeautifulSoup(response.content)

        num_pages = int(soup.find('div', {'class': 'strankovani'}).findChildren('a',
                                                                                {'class': 'pngfix'})[-2].text.strip())

        if response.content.decode('utf-8').find(u'Nebylo nalezeno žádné zboží.') != -1:
            self.logger.info('No discounts found, bailing out')
            sys.exit(0)
        else:
            self.logger.info('%s got %d pages of discounts' % (self.seller_name, num_pages))

        self.do_scrap(response)

        if not teaser_only:
            for page in range(2, num_pages):
                try:
                    discount_url = '%s/akciovy-tovar/%s/strana-%d/' % (ZlacneneScrapper.BASE_URL,
                                                                       self.seller_slug,
                                                                       page)

                    self.logger.info('scrapping %s...' % discount_url)

                    response = self.session.get(discount_url)
                    self.do_scrap(response)
                except Exception, e:
                    self.logger.error('Failed to fetch page %d, reason: %s' % (page, e))
        else:
            self.logger.info('scrapped discounts only from first page as a teaser')

        return self.discount_map


    def do_scrap(self, response):
        """Scraps discounts from response."""

        soup = BeautifulSoup(response.content)
        content = None

        for product in soup.findAll('div', attrs={'class': 'zboziVypis'}):
            rel_product_link = None

            num_retries = ZlacneneScrapper.MAX_NUM_RETRIES
            while num_retries > 0:
                try:
                    name = product.find('a', {'class': 'zboziImg'})['title'].strip()
                    try:
                        description = product.find('p', {'class': 'popis'}).text.strip()[:ZlacneneScrapper.MAX_DESCRIPTION_SIZE]
                        if len(description) == ZlacneneScrapper.MAX_DESCRIPTION_SIZE:
                            description = u'%s...' % description[:ZlacneneScrapper.MAX_DESCRIPTION_SIZE - 3]
                    except Exception:
                        description = None

                    try:
                        discount_amount = int(product.find('a', {'class': 'slevaStar'}).text.strip().replace('-', '').replace('%', ''))
                    except Exception:
                        discount_amount = 0

                    new_price = ZlacneneScrapper.RE_PRICE.search(product.find('p', {'class': 'cena'}).text).groupdict()
                    new_price = Decimal('%s.%s' % (new_price['euros'], new_price['cents']))

                    old_price = None

                    amount = None
                    price_unit = None
                    unit = None
                    multipack = None
                    if description:
                        for re_price_unit in [ZlacneneScrapper.RE_PRICE_UNIT,
                                              ZlacneneScrapper.RE_PRICE_100,
                                              ZlacneneScrapper.RE_PRICE_PC,
                                              ZlacneneScrapper.RE_PRICE_DOSE,
                                              ZlacneneScrapper.RE_AMOUNT]:
                            m = re_price_unit.search(description)
                            if m:
                                amount_and_price_unit = m.groupdict()
                                amount = amount_and_price_unit['amount'].strip()
                                if amount_and_price_unit.has_key('price'):
                                    price_unit = Decimal(amount_and_price_unit['price'].replace(',', '.'))
                                if amount_and_price_unit.has_key('unit'):
                                    unit = amount_and_price_unit['unit']
                                if amount_and_price_unit.has_key('multipack'):
                                    multipack = amount_and_price_unit['multipack']

                                break

                    if amount:
                        name = u'%s %s' % (name, amount)

                    if unit and unit == u'praciu dávku':
                        unit = u'dávka'

                    if multipack and multipack.find('2+1 ZADARMO'):
                        multipack = _('multipack_2_plus_one_free')

                    discount_period = ZlacneneScrapper.RE_DISCOUNT_PERIOD.search(product.find('dl', {'class': 'platnostInfo'}).text.replace('\n', '').strip()).groupdict()
                    valid_from = '%s%s' % (discount_period['valid_from'], date.today().year)
                    valid_until = discount_period['valid_until']

                    rel_product_link = product.find('a', {'class': 'zboziImg'})
                    abs_product_link = '%s%s' % (ZlacneneScrapper.BASE_URL, rel_product_link['href'])

                    discount = ZlacneneDiscount(url=abs_product_link, image_file=None,
                                                name=name, valid_from=valid_from, valid_until=valid_until,
                                                seller=seller, description=description,
                                                new_price=new_price, old_price=old_price, discount=discount_amount,
                                                unit=unit, price_unit=price_unit,
                                                multipack=multipack)

                    # skip the product if trashed
                    if SellerProductDiscountTrash.discount_exists(discount):
                        logger.info('skipping "%s" as has been trashed' % discount.name)
                        break

                    # skip the product if scrapped already
                    if SellerProductDiscount.discount_exists(discount,
                                                             adjust_discount_period=self.seller_name == 'lidl'):
                        logger.info('skipping "%s" as already scrapped' % discount.name)
                        break

                    seller_product_discount = SellerProductDiscount.get_seller_product_discount_by_url_and_seller(
                                                            discount.name,
                                                            discount.url,
                                                            self.seller.id)
                    if seller_product_discount and seller_product_discount.seller_product_id is not None:
                        seller_product_discount.on_discount_valid(discount)

                        seller_product = SellerProduct.get_seller_product_by_id(seller_product_discount.seller_product_id)
                        seller_product.on_discount_valid(seller_product_discount)
                    else:
                        image_link = ImageLink(product.find('img'))
                        image_link.fetch(self.session)

                        discount.image_file = image_link.rel_image_path
                        seller_product = discount.save()

                        if seller_product is None:
                            self.add(discount)
                        else:
                            if seller_product.must_recategorize:
                                self.on_must_recategorize(seller_product)

                    self.logger.debug('scrapped discount: %s' % discount)

                    break

                except Exception, e:
                    num_retries -= 1
                    if num_retries > 0:
                        sleep_int = randint(5, 10)
                        # self.logger.warning('Failed to scrap a discount, reason: %s, will retry in %d secs ('
                        #                     'num retries left: %d)' % (e.message.decode('utf-8'),
                        #                                                sleep_int, num_retries))
                        self.logger.exception('Failed to scrap a discount, reason: %s, will retry in %d secs ('
                                            'num retries left: %d)' % (e.message.decode('utf-8'),
                                                                       sleep_int, num_retries))
                        time.sleep(sleep_int)
                    else:
                        self.logger.exception(u'Failed to scrap a discount, seller = "%s", rel_product_link = "%s", '
                                              'content: "%s", reason: %s' % (
                            self.seller.name, rel_product_link, content.decode('utf-8'), e.message.decode('utf-8')))



sellers = ['Billa', 'Kaufland', '101-drogerie', 'DM', 'coop-jednota-slovensko']

parser = argparse.ArgumentParser(description='zlacnene discount scrapper utility')
parser.add_argument('--seller', '-s', dest="seller",
                    help='Seller name, whose products are to be scrapped. Supported sellers: %s' % sellers)
parser.add_argument('--verbosity', '-v', action='count', default=1,
                    help='Be verbose about actions performed (-v info, -vv - debug)')
parser.add_argument('--logfile', '-f',
                    help='File where the logging will be written. If not specified logging will go to console.')
parser.add_argument('--teaser-only', '-t', dest="teaser_only", action='store_true',
                    help='Scraps only first page.')
parser.add_argument('--dont-torify', dest="dont_torify", action='store_true',
                    help='Do not use TOR when scrapping. Used by default.')
parser.add_argument('--dont-send-mail', dest="dont_send_email", action='store_true',
                    help='Do not send a scrapping email report. The report is sent by default.')

seller_map = {'coop-jednota-slovensko': 'COOP',
              '101-drogerie': '101drogerie'}


args = parser.parse_args()

if args.seller is None or args.seller not in sellers:
    parser.print_help()
    sys.exit(1)

if seller_map.has_key(args.seller):
    seller_name = seller_map[args.seller]
else:
    seller_name = args.seller

try:
    seller = Seller.get_seller_by_name(seller_name)
except SellerDoesNotExist:
    parser.print_help()
    sys.exit(1)

log_level = logging.INFO
if args.verbosity > 1:
    log_level = logging.DEBUG

if args.logfile is not None:
    logging.basicConfig(filename=args.logfile, filemode='w', format='%(asctime)s %(levelname)s %(message)s',
                        level=log_level)
else:
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=log_level)

logger = logging.getLogger(__name__)

logger.info('Scrapping %s discounts...' % seller.name)

discount_scrapper = ZlacneneScrapper(seller, args.seller, logger=logger, sleep_int=None,
                                     dont_torify=args.dont_torify)
discount_scrapper.scrap(teaser_only=args.teaser_only)
if not args.dont_send_email:
    discount_scrapper.send_mail()