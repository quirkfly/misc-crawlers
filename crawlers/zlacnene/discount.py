__author__ = 'peterd'

from moneyed import EUR, Money

from shopper.crawlers.discount import Discount

class ZlacneneDiscount(Discount):
    """Represents a discount product."""

    def __init__(self, url, image_file, name, valid_from, valid_until, seller, description,
                 new_price, old_price, discount, currency=EUR, date_format='%d.%m.%Y', unit=None, price_unit=None,
                 multipack=None):
        super(ZlacneneDiscount, self).__init__(url, image_file, name, valid_from, valid_until, seller, description,
                                               new_price, old_price, currency, date_format, unit, price_unit, discount,
                                               multipack=None)