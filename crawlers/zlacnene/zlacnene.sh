#!/bin/bash -u

# main script that orchestrates the scrapping process from zlacnene.sk
#

VERBOSITY=0
LOG_FILE=
TEASER_ONLY=0
DONT_SEND_MAIL=0
DONT_TORIFY=0
OPTIONS=

function usage {
	echo "zlacnene scrapper wrapper"
	echo "`basename $0` [-v] [-l <log file] [-t] <seller> <seller> <seller> ..."
    echo "v - be verbose about actions performed"
	echo "<log file> - if not specify log to console"
	echo "t - scrap only first page"
	echo "seller - name of seller, Supported sellers: ['Billa', 'Kaufland', 'Carrefour',
                        'Lidl', '101-drogerie', 'DM']"
	exit 1
}


while getopts vl:trs FLAG; do
	case $FLAG in
	v)
		VERBOSITY=1;
		;;
    l)
		LOG_FILE=$OPTARG;
     	;; 
    t)
		TEASER_ONLY=1;
      	;;
    r)
		DONT_TORIFY=1;
      	;;
    s)
		DONT_SEND_MAIL=0
      	;;
    ?)
     	usage;
      	;;
  esac
done

shift $(( OPTIND - 1 ));

test $# -lt 1 && usage

cd $ITRASH_HOME_DIR

if [ $VERBOSITY -eq 1 ]
then
	OPTIONS="--verbosity"
fi

if [ ! -z "$LOG_FILE" ]
then
	OPTIONS=" $OPTIONS --logfile $LOG_FILE"
fi

if [ $TEASER_ONLY -eq 1 ]
then
	OPTIONS=" $OPTIONS --teaser-only"
fi

if [ $DONT_TORIFY -eq 1 ]
then
	OPTIONS=" $OPTIONS --dont-torify"
fi

if [ $DONT_SEND_MAIL -eq 1 ]
then
	OPTIONS=" $OPTIONS --dont-send-mail"
fi

for SELLER in $@
do
    # first scrap products from official site if seller is lidl
    if [ $SELLER == "Lidl" ]
    then
        DONT_TORIFY_SHELL_OPT=""
        if [ $DONT_TORIFY -eq 1 ]
        then
            DONT_TORIFY_SHELL_OPT="-r"
        fi
        ITRASH_HOME_DIR=$ITRASH_HOME_DIR shopper/crawlers/lidl/lidl.sh -v $DONT_TORIFY_SHELL_OPT -l /var/log/itrash/lidl_sk_scrapper.log
    fi

	PYTHONPATH=`pwd`:.. DJANGO_SETTINGS_MODULE=itrash.settings python shopper/crawlers/zlacnene/discount_scrapper.py $OPTIONS --seller $SELLER

	if [ ! -z "$LOG_FILE" ]
	then
		#
		# check scrapper log for errors
		#

		NUM_WRITE_ERRORS=`grep -i error $LOG_FILE | wc -l`
		if [ $NUM_WRITE_ERRORS -gt 0 ]
		then
			grep -i -A100 error $LOG_FILE > /tmp/discount_scrapper.err
			python2.7 utils/mailer.py --from_addr discount.scrapper@20deka.com --to_addr peter.dermek@20deka.com --subject "[zlacnene] Failed to scrap $NUM_WRITE_ERRORS discounts." /tmp/discount_scrapper.err
			rm /tmp/discount_scrapper.err
		fi
	fi

done